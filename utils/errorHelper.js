var logger = console;
var _ = require('underscore');

exports.log = function(modelName, data) {
    logger.info('Error saving %s', modelName);
    logger.info('========== INPUT =========================================');
    _.each(data, function(d) {
        logger.info(d);
    });
    logger.info('==========================================================');
};

exports.createErrorMessages = function(errors) {
  var messages = [];

  _.each(errors, function(validationError) {
    messages.push({
      message: validationError.message,
      path: validationError.path,
      type: validationError.type
    });
  });

  return messages;
}
