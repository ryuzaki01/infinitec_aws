/**
 * Created by ihsan on 4/13/15.
 */

var handlebar = require('./handlebarSingleton.js');
var fs = require('fs');
var cwd = process.cwd();
var template = fs.readFileSync(cwd + '/templates/error/error-page.hbs', 'utf8');

/**
 * This is our custom http 404 and 500 error handler page.
 *
 * @type {Function}
 */
exports = module.exports = function customHttpError(title, errorMessage) {
  var html = handlebar.compile(template);
  var status = 500;
  if ('Sorry, no page could be found at this address (404)' == title) {
    status = 404;
  }

  return html({
    status: status,
    errorMessage: errorMessage
  });
}
