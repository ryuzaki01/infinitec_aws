/**
 * Created by ihsan on 4/13/15.
 */

// This is the singleton instance that we will return to anyone who needs it.
var handlebar = require('handlebars');

//this handlebar helpers library is a utility to register all helpers to handlebars
var handlebarHelper = require('express-handlebars-helpers');

var fs = require('fs');
var cwd = process.cwd();

//dump all the default handlebar helpers defined in node_modules/express-handlebars-helpers/lib/helpers.js to allTheHelpers object
var allTheHelpers = handlebarHelper.registerToHelpers({});

//dump our own handlebar helper library to allTheHelpers object
allTheHelpers = new require(cwd + '/templates/views/helpers')(allTheHelpers);

handlebarHelper.register(handlebar, undefined, undefined, allTheHelpers);

//this is an extra library that we want to use because it gives us the power to inherit template (handlebar doesn't support inheritance by default)
var handlebarLayout = require('handlebars-layouts');
//register this library to handlebar
handlebarLayout.register(handlebar);

module.exports = handlebar;

