/**
 * Configures and setup winston for logging. This logging is only used for
 * application level logging. If there is an error with the server (like uncaught exception)
 * it will be printed into console.log. The reason why this is necessary is because node.js
 * current architecture has a weakness. If there's an uncaught exception, the process will die.
 * Sometimes when we redirect console.log to winston, we can have a situation where the
 * uncaught exception kill the process before the log message got printed out by winston.
 * So sometimes we can lose very important log message (the reason why the process dies). Hence
 * we should separate the console.log and winston. Winston only specialize in app specific log
 * because the nice thing about winston is, it can tell you the file name, it is async, it
 * can have many transport types (store data to mongo, email, sms, etc).
 *
 * Note: Winston can have many loggers. Each logger can have many channel to write
 * the log to. In our project we simply use one logger for everything and
 * for this logger we will log everything to file (rotating file).
 *
 * In the future if we want to be more serious, we can create something like this:
 * logger A -> for module A
 * logger B -> for module B
 *
 * for each logger create:
 * 1.) a transport that handles only handles fatal error and send
 * email or sms to the team/person responsible for that logger.
 * 2.) another transport that handles normal logging to a file
 *
 * @author Ihsan Fauzi
 */

"use strict";
var winston = require("winston");
var path = require("path");
var mkdirp = require("mkdirp");
/*
 loads the environment variable defined in .env
 The .env that's loaded is always from where the caller is located so make sure
 if you import logger, the .env must be located correctly.
 */
require('dotenv').load();

var logPath = null;

if (!process.env.LOG_PATH) {
  //by default create log directory at where the caller is located if it doesn't exist
  var logDirectory = process.cwd() + '/logs';
  mkdirp(logDirectory, function (err) {
    console.error("Cannot create directory " + logDirectory +
    ". Most likely it's because the directory already exist.");
  });
  logPath = logDirectory + "/app.log";
}
else {
  logPath = process.env.LOG_PATH;
}

var level = process.env.LOG_LEVEL || 'info';

//create logger that prepend the moduleName to its message
module.exports = function Logger(moduleName) {
  var newLogger = new winston.Logger({
    transports: [
      new winston.transports.Console(),
      new winston.transports.DailyRotateFile({
        level: level,
        filename: logPath,
        handleExceptions: false, //we'll let keystone handle exception by printing it to console
        json: false,
        colorize: false //file cannot be colorized
      })
    ],
    exitOnError: false
  });
  var funcHolder = newLogger.log;
  //prepend the moduleName to winston's logger
  newLogger.log = function (level) {
    var args = arguments;
    if (args[1]) {
      args[1] = moduleName + " - " + args[1];
    }
    return funcHolder.apply(this, args);
  }
  return newLogger;
};

module.exports.stream = {
  //we create stream in case in the future we want to use morgan to log into the same log file
  write: function (message, encoding) {
    logger.info(message);
  }
};
