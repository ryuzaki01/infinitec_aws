require('dotenv').load({path: '.env'});
// Initialise New Relic if an app name and license key exists

if (process.env.NODE_ENV === 'development' && process.env.NEW_RELIC_APP_NAME && process.env.NEW_RELIC_LICENSE_KEY) {
  console.log('----------------------------------------');
  console.log('Starting NewRelic Service');
  console.log('----------------------------------------');
  require('newrelic');
}

var path = require('path');

/**
 * Keystone is a thin wrapper over express to provide CMS ability, and some other
 * boiler plate code written. For example: emailing, user sign in, etc.
 */
var keystone = require('keystone');
/**
 * Socket.IO enables real-time bidirectional event-based communication.
 */
var sock = require('socket.io');
/**
 * handlebars is a templating language, Express supports jade and handlebars.
 * We use handlebars so we use this library.
 */
var handlebars = require('express-handlebars');
/**
 * i18n is a library for translating languages.
 */
var i18n = require('i18n');
/**
 * This is a collection of plugins and our own custom helpers for handlebars
 */
var hbsHelpers = require('express-handlebars-helpers');
var handlebarsHelpers = hbsHelpers.registerToHelpers({});
handlebarsHelpers = new require('./templates/views/helpers')(handlebarsHelpers);
if (process.env.COOKIE_MAX_AGE_MILLISECONDS){
  sessionOptions.cookie = {
    maxAge : parseInt(process.env.COOKIE_MAX_AGE_MILLISECONDS, 10)
  };
}
keystone.init({

  'name': 'Infinitec AWS',
  'port': process.env.PORT || 3000,
  'host': process.env.IP || '127.0.0.1',

  'favicon': 'public/favicon.ico',
  'less': 'public',
  'static': ['public'],

  'views': 'templates/views',
  'view engine': 'hbs',

  'custom engine': handlebars.create({
    layoutsDir: 'templates/views/layouts',
    partialsDir: 'templates/views/partials',
    defaultLayout: 'default',
    helpers: handlebarsHelpers,
    extname: '.hbs'
  }).engine,

  'auto update': true,
  'session': true,
  'session store': 'mongo',
  'auth': true,
  'user model': 'User',
  'cookie secret': 'shinji"#!'

});

// Load project's Models
keystone.import('models');

// Enable mongoose logging in dev environment
if (keystone.get('env') === 'development') {
  keystone.mongoose.set('debug', true);
}

//Set absolute root application path
keystone.set('appRoot', path.resolve(__dirname));

// Configure i18n
i18n.configure({
  locales: ['id', 'en', 'jp'],

  // you may alter a site wide default locale
  defaultLocale: 'jp',

  cookie: 'locale',

  // allows object notation
  objectNotation: true,

  directory: "" + __dirname + '/locales'
});

keystone.set('routes', require('./routes'));

//must call require middleware after we initialize handlebarHelpers because we need the i18n defined
var middleware = require('./routes/middleware.js');

// setup all the common middleware logic
keystone.pre('routes', middleware.processSetLocale);
// Add-in i18n support
keystone.pre('routes', i18n.init);
// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('routes', middleware.sessionCheck);
keystone.pre('render', middleware.flashMessages);

// Duck punch html error page to show the one that we like
keystone.wrapHTMLError = require('./utils/customHttpError.js');

keystone.start({
  onHttpServerCreated: function(){
    keystone.set('io', sock.listen(keystone.httpServer));

    var io = keystone.get('io');
    var groups = [];
    var users = [];

    io.sockets.on('connection', function(socket){
      socket.emit('connected');

      socket.on('cm_auth', function(data){
        if(!groups[data.groupid]){
          groups[data.groupid] = [];
          groups[data.groupid][data.page] = [];
        } else {
          if(!groups[data.groupid][data.page]){
            groups[data.groupid][data.page] = [];
          } else {
          }
        }
        users[data.userid] = [];
        users[data.userid].username = data.username;
        socket.username = data.username;
        socket.userid = data.userid;
        socket.group = data.groupid;

        socket.join(socket.group);
        socket.emit('sm_auth', socket.id);
      });

      socket.on('cm_new', function(data) {
        socket.broadcast.to(socket.group).emit('sm_new', data);
      });

      socket.on('cm_update', function(data) {
        socket.broadcast.to(socket.group).emit('sm_update', data);
      });

      socket.on('cm_delete', function(data) {
        socket.broadcast.to(socket.group).emit('sm_delete', {
          id: data.id,
          groupid: data.groupid,
          page: data.page
        });
      });

      socket.on('cm_move', function(data) {
        socket.broadcast.to(socket.group).emit('sm_move', {
          id: data.id,
          groupid: data.groupid,
          page: data.page,
          top: data.top,
          left: data.left,
          z: data.z
        });
      });

      socket.on('cm_rotate', function(data) {
        socket.broadcast.to(socket.group).emit('sm_rotate', {
          id: data.id,
          groupid: data.groupid,
          page: data.page,
          angle: data.angle,
          z: data.z
        });
      });

      socket.on('cm_lock', function(data) {
        socket.broadcast.to(socket.group).emit('sm_lock', {
          id: data.id,
          groupid: data.group,
          page: data.page
        });
      });

      socket.on('cm_unlock', function(data) {
        socket.broadcast.to(socket.group).emit('sm_unlock', {
          id: data.id,
          groupid: data.groupid,
          page: data.page
        });
      });

      socket.on('cm_msg', function(data) {
        socket.broadcast.to(socket.group).emit('sm_msg', data);
      });

      socket.on('cm_resize', function(data) {
        socket.broadcast.to(socket.group).emit('sm_resize', {
          id: data.id,
          groupid: data.groupid,
          page: data.page,
          width: data.width,
          height: data.height,
          top: data.top,
          left: data.left
        });
      });

      socket.on('disconnect', function(){
        delete users[socket.userid];

        socket.broadcast.to(socket.group).emit('sm_status', socket.username + ' has disconnected');
        socket.leave(socket.group);
      });

    });
  }
});

exports = module.exports = keystone;
