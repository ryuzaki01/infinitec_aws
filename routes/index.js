var keystone = require('keystone'),
    middleware = require('./middleware'),
    importRoutes = keystone.importer(__dirname);

var AwsUser = keystone.list('AwsUser').model;

var cors = require('cors');
var _ = require('underscore');
// Load Routes
var routes = {
  views: importRoutes('./views'),
  api: importRoutes('./api')
};

// Bind Routes
exports = module.exports = function (app) {

  // Dummy Date
  date = new Date().getTime();

  app.get('/', routes.views.index);

  app.get('/:jp(jp)', routes.views.index);
  app.get('/:en(en)', routes.views.index);

  app.all('/mymemo/:memoId/:page', routes.views.mymemo);

  app.all('/api*', keystone.middleware.cors, keystone.middleware.api);
  app.post('/api/load-canvas', routes.api.loadCanvas);
  app.post('/api/save-canvas/:pageId', routes.api.saveCanvas);
  app.post('/api/ping', routes.api.ping);
  app.post('/api/group/:method', routes.api.group);
  app.post('/api/user/:method', routes.api.user);
  app.post('/api/upload/:method', routes.api.upload);

  app.all('/room/:id', routes.views.room);
  app.get('/rooms', routes.views.rooms);

  app.get('/usermanagement', routes.views.userManagement);

  app.all('/upload', routes.views.upload);
  app.get('/files', routes.views.files);

  app.get('/presentation', routes.views.presentation);

  app.all('/login', routes.views.login);
  app.get('/logout', function(req, res){
    AwsUser.findOne({_id : req.session.AwsUser._id}, function(err, user){
      user.lastActive = (new Date()).getTime() - (3 * 60000);
      user.save(function(err){
        req.session.AwsUser = false;
        res.redirect('/login');
      });
    });
  });

  app.use('/users', routes.views.user.router);
  app.use('/users-groups', routes.views.usergroup.router);
  app.use('/groups', routes.views.group.router);
};
