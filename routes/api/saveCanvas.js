var keystone = require('keystone');
var Element = keystone.list('Element').model;
var async = require('async');
var logger = require('../../utils/logger.js')(__filename);

exports = module.exports = function (req, res) {
  "use strict";
  var formData = req.body;
  var pageid = req.params.pageId;

    if(formData.data){
      async.forEach(formData.data, function(el, cb) {
        switch (el.persistentState) {
          case 'new':
            el.page = pageid;
            el.name = el.id;
            new Element(el).save(function(err, elem){
              if(err){
                console.log(err);
              }
              cb(err);
            });
          break;
          case 'update_required':
            Element.findOne({name: el.id}).exec(function (err, element) {
              if (!err && element) {
                element.style.lineSize = el.style.lineSize;
                element.style.stroke = el.style.stroke;
                element.style.fill = el.style.fill;
                element.size.height = el.size.height;
                element.size.width = el.size.width;
                element.position.left = el.position.left;
                element.position.top = el.position.top;
                element.angle = el.angle;
                element.data = el.data;
                element.save(function(err){
                  cb(err);
                });
              }
            });
          break;
          case 'deleted':
            Element.findOne({name: el.id}).remove(function(err){
              cb(err);
            });
          break;
        }
      }, function(err) {
        if(err){
          return res.apiResponse({success: false, error: err});
        } else {
          return res.apiResponse({success: true});
        }
      });
    } else {
      return res.apiResponse({success: true});
    }


    /** TODO : update page thumbnail **/
};
