var keystone = require('keystone');
var AwsUser = keystone.list('AwsUser').model;
var moment = require('moment');

exports = module.exports = function (req, res) {
  "use strict";
  var id = req.body.id;

  var updated = [];

  if(id) {
    current = moment();
    AwsUser.findOne({id: id}, function (err, user) {
      user.lastActive = current.format();
      user.save();
    });
  }

  return res.apiResponse({success: true});
};
