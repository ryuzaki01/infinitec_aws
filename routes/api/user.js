var keystone = require('keystone');
var AwsUser = keystone.list('AwsUser').model;
var async = require('async');
var logger = require('../../utils/logger.js')(__filename);

exports = module.exports = function (req, res) {
  "use strict";
  var formData = req.body;
  var method = req.params.method;

  if(formData) {
    switch(method){
      case 'get':
        AwsUser.findOne(formData).exec(function (err, user) {
          if (!err) {
            return res.apiResponse({success: true, user: user});
          } else {
            return res.apiResponse({error: true});
          }
        });
      break;
      case 'getall':
        AwsUser.find(formData).exec(function (err, users) {
          if (!err) {
            return res.apiResponse({success: true, users: users});
          } else {
            return res.apiResponse({error: true});
          }
        });
      break;
      case 'insert':
        new AwsUser(formData).save(function(err, user){
          if(err){
            console.log(err);
            return res.apiResponse({success: false, error: err});
          } else {
            return res.apiResponse({success: true, user: user});
          }
        });
      break;
      case 'update':
        AwsUser.findOne({id: formData.id}).exec(function (err, user) {
          if (!err) {
            user.name = formData.name;
            user.save(function(err){
              if(err) {
                console.log(err);
              }
              return res.apiResponse({success: true});
            });
          } else {
            return res.apiResponse({error: true});
          }
        });
      break;
      case 'delete':
        AwsUser.findOne({name: formData.id}).remove(function(err){
          if(err) {
            console.log(err);
          }
          return res.apiResponse({success: true});
        });
      break;
    }
  }
};