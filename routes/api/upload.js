var keystone = require('keystone');
var Background = keystone.list('Background').model;
var async = require('async');
var path = require('path');
var fs = require('fs');
var logger = require('../../utils/logger.js')(__filename);

exports = module.exports = function (req, res) {
  "use strict";
  var method = req.params.method;

    switch (method) {
      case 'background':
        var formData = req.files;
        if (formData) {
          var bgFile = formData[0];
          var newPath = "public/uploads/backgrounds/".split("/").join(path.sep) + bgFile.name;
          fs.rename(bgFile.path, newPath, function (err) {
            if (err) {
              return res.apiResponse({success: false, error: err});
            } else {
              new Background({
                name: bgFile.originalname,
                data: {
                  filename: bgFile.name,
                  path: newPath,
                  size: bgFile.size,
                  filetype: bgFile.extension
                },
                creator: req.session.AwsUser._id
              }).save(function (err, background) {
                  if (err) {
                    return res.apiResponse({success: false, error: err});
                  } else {
                    background.url = '/uploads/images/backgrounds/' + background.filename;
                    return res.apiResponse({success: true, background: background});
                  }
                });
            }
          });
        } else {
          return res.apiResponse({success: false});
        }
      break;
      case 'element':
          var formData = req.body;
          var bgFile = formData.image.replace(/^data:image\/\w+;base64,/, "");
          bgFile = bgFile.replace(/ /g, '+');
          var buff = new Buffer(bgFile, 'base64');
          var timestamp = (new Date()).getTime();
          var newPath = "public/uploads/elements/".split("/").join(path.sep) + timestamp + '.png';
          fs.writeFile(newPath, buff, 'base64', function (err) {
            if (err) {
              return res.apiResponse({success: false, error: err});
            } else {
              return res.apiResponse({success: true, data: "/uploads/elements/"+ timestamp + ".png"});
            }
          });
      break;
    }
};