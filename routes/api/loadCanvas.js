var keystone = require('keystone');
var MemoPage = keystone.list('MemoPage').model;
var Element = keystone.list('Element').model;
var async = require('async');
var logger = require('../../utils/logger.js')(__filename);

exports = module.exports = function (req, res) {
  "use strict";
  var formData = req.body;

  if(formData){
    async.waterfall([
      function(cb) {
        console.log('Loading page : '+formData.pageid);
        MemoPage.findOne({'_id' : formData.pageid})
          .exec(function (err, page) {
            if(err){
              console.log(err);
            }
            cb(err, page);
          });
      },
      function(page, cb){
        page.getElement(function (err, data) {
          if(err){
            console.log(err);
          }
            cb(null, data);
        });
      }
    ], function(err, data) {
        if (err) {
          return res.apiResponse({success: false, error: err});
        } else {
          return res.apiResponse(data);
        }
      }
    );
  } else {
    return res.apiResponse({success: false});
  }
};
