var keystone = require('keystone');
var AwsGroup = keystone.list('AwsGroup').model;
var Memo = keystone.list('Memo').model;
var MemoPage = keystone.list('MemoPage').model;
var async = require('async');
var logger = require('../../utils/logger.js')(__filename);

exports = module.exports = function (req, res) {
  "use strict";
  console.log(req.body);
  var formData = req.body;
  var method = req.params.method;

  if(formData) {
    switch(method){
      case 'get':
        AwsGroup.findOne({id: formData.id}).exec(function (err, group) {
          if (!err) {
            return res.apiResponse(user);
          } else {
            return res.apiResponse({success: false, error: err});
          }
        });
      break;
      case 'getall':
        AwsGroup.find().exec(function (err, groups) {
          if (!err) {
            return res.apiResponse(groups);
          } else {
            return res.apiResponse({success: false, error: err});
          }
        });
      break;
      case 'insert':
        async.waterfall([
          function(cb){
            new AwsGroup(formData).save(function(err, group){
              cb(err, group);
            });
          },
          function(group, cb){
            new Memo({name: 'Default File', group: group._id, page: 1, creator: req.session.AwsUser._id}).save(function(err, memo){
              cb(err, memo, group);
            });
          },
          function(memo, group, cb){
            new MemoPage({ memo: memo._id, index: 1}).save(function(err, memo){
              memo.setDefaultBackground(function(err, memo){
                cb(err, group);
              });
            });
          }
        ], function(err, group){
          if(err){
            console.log(err);
            return res.apiResponse({success: false, error: err});
          } else {
            return res.apiResponse({success: true, group: group});
          }
        });
      break;
      case 'update':
        AwsGroup.findOne({id: formData.id}).exec(function (err, group) {
          if (!err) {
            group.name = formData.name;
            group.password = formData.password;
            group.email = formData.email;
            group.level = formData.level;
            group.group = formData.group;
            group.save(function(err){
              if(err) {
                console.log(err);
              }
              return res.apiResponse({success: true});
            });
          } else {
            return res.apiResponse({error: true});
          }
        });
      break;
      case 'delete':
        AwsGroup.findOne({name: formData.id}).remove(function(err){
          if(err) {
            console.log(err);
          }
          return res.apiResponse({success: true});
        });
      break;
      case 'create_memo':
        async.waterfall([
          function(cb){
            AwsGroup.findOne({ _id : req.session.AwsUser.group}, function(err, group){
              cb(err, group);
            });
          },
          function(group, cb){
            new Memo({name: formData.name, group: group._id, page: 1, creator: req.session.AwsUser._id}).save(function(err, memo){
              cb(err, memo, group);
            });
          },
          function(memo, group, cb){
            new MemoPage({ memo: memo._id, index: 1}).save(function(err, memo){
              memo.setDefaultBackground(function(err, memo){
                cb(err, group);
              });
            });
          }
        ], function(err, group){
          if(err){
            console.log(err);
            return res.apiResponse({success: false, error: err});
          } else {
            return res.apiResponse({success: true, group: group});
          }
        });
      break;
    }
  }
};