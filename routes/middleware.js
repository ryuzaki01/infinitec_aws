var _ = require('underscore');
var keystone = require('keystone');
var bowser = require('../lib/node-bowser');
var translate = require('i18n').__;
var moment = require('moment');

/**
    Initialises the standard view locals.
    Include anything that should be initialised before route controllers are executed.
*/
exports.initLocals = function(req, res, next) {

    var locals = res.locals;

    locals.user = req.user;
    locals.AwsUser = req.session.AwsUser;

    locals.requireBundle = process.env.REQUIRE_BUNDLE;


    if(locals.AwsUser){
        locals.canAccessManagement = locals.AwsUser.level === 'teacher' || locals.AwsUser.level === 'admin';
    }

    locals.require_bundle = process.env.REQUIRE_BUNDLE;
    // Add your own local variables here

    var requestBowser = bowser.detect(req);

    locals.platform = {
        mobile: requestBowser.mobile,
        ios: requestBowser.ios,
        iphone: requestBowser.iphone,
        ipad: requestBowser.ipad,
        android: requestBowser.android
    };

    locals.initials = {platform: locals.platform};

    next();

};

/**
 Set the language cookies if needed
 */
exports.processSetLocale = function (req, res, next) {
    if (req.cookies) {
        // set the locale to japanese by default
        // TODO: use other mechanism to detect the default language
        if (!req.cookies.locale) {
            req.cookies.locale = 'jp';
        }
    }

    if (req.query && req.query.setLocale) {
        var locale = req.query.setLocale;
        if (locale === 'en' || locale === 'id' || locale === 'jp') {
            req.cookies.locale = locale;
            res.cookie('locale', locale);
        }
    }

    next();
};

/**
    Inits the error handler functions into `res`
*/
exports.initErrorHandlers = function(req, res, next) {

    res.err = function(err, title, message) {
        res.status(500).render('errors/500', {
            err: err,
            errorTitle: title,
            errorMsg: message
        });
    }

    res.notfound = function(title, message) {
        res.status(404).render('errors/404', {
            errorTitle: title,
            errorMsg: message
        });
    }

    next();

};

/**
    Fetches and clears the flashMessages before a view is rendered
*/
exports.flashMessages = function(req, res, next) {

    var flashMessages = {
        info: req.flash('info'),
        success: req.flash('success'),
        warning: req.flash('warning'),
        error: req.flash('error')
    };

    res.locals.messages = _.any(flashMessages, function(msgs) { return msgs.length }) ? flashMessages : false;

    next();

};
exports.sessionCheck = function (req, res, next) {
    // if(req.cookies.expiredSession && moment() > moment(req.cookies.expiredSession)){
        // req.session.AwsUser = false;
        // return res.redirect('/login');
    // }
    if (req.url === '/login' || /keystone/.test(req.url)) {
        return next();
    }
    if (!req.session.AwsUser) {
        req.flash('error', translate('session.pleaseLogin'));
        return res.redirect('/login');
    }
    return next();
};
