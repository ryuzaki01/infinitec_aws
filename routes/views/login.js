
var keystone = require('keystone');
var moment = require('moment');
var AwsUser = keystone.list('AwsUser').model;
//var logger = require('../../../utils/logger.js')(__filename);

exports = module.exports = function(req, res) {
  var view = new keystone.View(req, res);
  var locals = res.locals;

 // if (req.session.AwsUser) {
   // return res.redirect(req.cookies.target || '/login');
 // }

  //var view = new keystone.View(req, res);

  view.on('post', {action: 'login'}, function (next) {
    if (!req.body.username || !req.body.password) {
      req.flash('error', req.__("signin.error.missingFields"));
      return next();
    }
    var current = (new Date()).getTime();
    if(!req.body.remember){
      req.cookies.expiredSession = moment().add(1,'hours');
    }
    //we don't need to store encrypted password in our database for staging user since it's only for staging. It's not user
    //
    //data anyway. Without encrypting it, it's easier to debug and see it on the database if there's a bug.
    AwsUser.findOne({$and: [{username: req.body.username}, {password: req.body.password}] }).exec(function(err, user){
      if (err){
        req.flash("Error getting staging user data from database. See logs for more details");
        return next();
      }
      if (user){
        if(current < (parseInt(user.lastActive) + (60 * 1000 * 3))){
          req.flash('error', req.__("signin.error.logged"));
          return next();
        } else {
          //this means the user can access staging environment. Sets the key.
          req.session.AwsUser = user;
          AwsUser.findOne({_id : user._id}, function(err, user){
            user.lastActive = current;
            user.save();
          });

          if (req.body.target && !/login/.test(req.body.target)) {
            return res.redirect(req.body.target);
          }
          else {
            if(user.level === 'teacher' || user.level === 'admin'){
              return res.redirect('/usermanagement');
            } else {
              return res.redirect('/');
            }
          }
        }
      }
      else {
        req.flash('error', req.__("signin.error.wrongPassword"));
        return next();
      }
    });
  });

  view.render('login');
};
