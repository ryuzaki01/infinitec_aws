/**
 * Created by ihsan on 4/14/15.
 */

var keystone = require('keystone');

exports = module.exports = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;

  locals.showToolbar = false;
  locals.showPresentationNavbar = true;
  /** render ke halaman **/
  view.render('presentation');
};