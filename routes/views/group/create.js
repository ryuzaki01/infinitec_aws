var keystone = require('keystone');
var AwsGroup = keystone.list('AwsGroup');
var _ = require('underscore');
var translate = require('i18n').__;
var errorHelper = require('../../../utils/errorHelper');

var get = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;

  res.locals.responseMessages = res.responseMessages;

  view.render('group/create');
};

var post = function(req, res) {
  var newGroup = new AwsGroup.model();

  newGroup.name = req.body.name;

  newGroup.saveAsync().then(function(result) {
    res.responseMessages = [{message: translate('awsGroup.messages.create.success')}];

    /* for API
     * return res.send({
     *   error: false,
     *   result: result
     * });
     */

    return get(req, res);
  })
  .catch(function(err) {
    res.responseMessages = errorHelper.createErrorMessages(err.errors);
    errorHelper.log('AwsGroup', [req.body, err, res.responseMessages]);

    /* for API
     * return res.send({
     *   error: true,
     *   messages: messages
     * })
     */

    return get(req, res);
  });
};

exports = module.exports = {
    get: get,
    post: post
};
