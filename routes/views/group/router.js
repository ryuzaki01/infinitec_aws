var router = require('keystone').express.Router();
var create = require('./create');
var list = require('./list');


router.get('/', list.get);
router.get('/create', create.get);
router.post('/create', create.post);

exports = module.exports = router;
