var _ = require('underscore');
var keystone = require('keystone');
var bluebird = require('bluebird');
var AwsGroup = keystone.list('AwsGroup');
var AwsUser = keystone.list('AwsUser');
var logger = console;

var listGET = function(req, res) {
  var view = new keystone.View(req, res);

  AwsGroup.model.find().lean().execAsync()
    .then(function(groups) {
      var findUserPromises = [];

      _.forEach(groups, function(g) {
        var p = AwsUser.model.find({ group: g._id.toString() }).lean().execAsync()
                  .then(function(users) {
                    g.users = users;
                  })
                  .catch(function(err) {
                    logger.log('Error getitng awsusers:', err);
                  });

        findUserPromises.push(p);
      });

      bluebird.all(findUserPromises).then(function() {
        res.locals.groups = groups;
        view.render('group/list');
      });
    })
    .catch(function(err) {
      logger.log('Error getting awsgroups:', err);
    });
};

exports = module.exports = {
  get: listGET
};
