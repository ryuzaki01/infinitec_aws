/**
 * Created by ihsan on 4/14/15.
 */

var keystone = require('keystone');

var AwsGroup = keystone.list('AwsGroup').model;
var Memo = keystone.list('Memo').model;
var MemoPage = keystone.list('MemoPage').model;
var async = require("async");

exports = module.exports = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;

  locals.showToolbar = true;
  locals.hideFooter = true;
  locals.groupId = req.session.AwsUser.group;
  locals.memoId = req.params.memoId;
  locals.pageIndex = req.params.page;

  view.on('init', function(next){
    async.parallel({
      group: function(cb){
        AwsGroup.findOne({_id: locals.groupId}, function(err, group){
          if(err){
            console.log(err);
          }
          cb(null,group);
        });
      },
      memo: function(cb){
        Memo.findOne({_id : locals.memoId}, function(err, memo) {
          if(err){
            console.log(err);
          }
          cb(null,memo);
        });
      },
      page: function(cb){
        MemoPage.findOne({memo : locals.memoId, index: locals.pageIndex}).populate('background').exec(function(err, page) {
          if(err){
            console.log(err);
          }
          cb(null,page);
        });
      }
    },
    function(err, results){
      locals.group = results.group;
      locals.memo = results.memo;
      locals.page = results.page;
      next();
    });
  });

  /** render ke halaman **/
  view.render('memo');
};