/**
 * Created by yeniovianti on 4/27/15.
 */

var keystone = require('keystone'), File =keystone.list('File');

var _ = require('underscore');

exports = module.exports = function(req, res) {
    'use strict';
    var view = new keystone.View(req, res);
    var locals = res.locals;
    /** locals adalah variable yang bisa diakses dari .hbs **/



    /** render ke halaman **/
    view.render('upload');
};
