var keystone = require('keystone');
var Room = keystone.list('Room').model;
var AwsUser = keystone.list('AwsUser').model;
var AwsGroup = keystone.list('AwsGroup').model;
var _ = require("underscore");
exports = module.exports = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;
  /** locals adalah variable yang bisa diakses dari .hbs **/

  locals.formData = req.body || {};
  locals.rooms  = [];

  view.on('init', function(next){
    Room.find({}, function(err, rooms){
      locals.rooms = rooms;
      _.map(rooms, function(room){
          console.log(room);
          AwsGroup.find().where('id').in(room.groups).exec(function(err, groups){
            room.groups = groups;
          });

          AwsUser.find().where('id').in(room.users).exec(function(err, users){
            room.users = users;
          });

          AwsUser.findOne({ id: room.creator }).exec(function(err, user){
            room.creator = user;
            console.log(user);
          });
      });
      next();
    });
  });

  /** render ke halaman **/
  view.render('rooms');
};