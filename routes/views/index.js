var keystone = require('keystone');
var AwsGroup = keystone.list('AwsGroup').model;
var File = keystone.list('File').model;
var async = require('async');

var _ = require('underscore');
exports = module.exports = function(req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    locals.showWellcome = true;

    var isEn = req.params && req.params.en === 'en';
    var isJp = req.params && req.params.jp === 'jp';

    if (isEn) {
        locals.locale = 'en';
    }

    if (isJp) {
        locals.locale = 'jp';
    }

    view.on('init', function(next) {
        if(req.session.AwsUser.group._id){
            res.redirect('/logout');
            next();
        }
        if(!req.session.AwsUser.group){
            req.flash('error', 'Please join a group');
            return next();
        } else {
            async.waterfall([
                function(cb){
                    AwsGroup.findOne().where({ "_id" : req.session.AwsUser.group}).exec(function(err, group){
                        if(err){
                            cb(err);
                        } else {
                            cb(null, group);
                        }
                    });
                },
                function(group, cb){
                    if(group) {
                        group.getMemos(function (err, memos) {
                            if(err){
                                cb(err);
                            } else {
                                cb(null, memos);
                            }
                        });
                    } else {
                        cb(null);
                    }
                },
                function(memos,  cb){
                    if(memos){
                        _.map(memos, function(memo){
                            if(memo){
                                memo.getFirstPage(function(err, memoPage){
                                    memo.firstPage = memoPage;
                                    if(err){
                                        return cb(err);
                                    }
                                });
                            } else {
                                memo.firstPage = {
                                    background: { url: '' }
                                }
                            }
                        });
                    }
                    cb(null, memos);
                }
            ], function(err, memos){
                if(err){
                    console.log(err);
                }
                locals.memos = memos;
                next();
            });
        }
    });

    view.render('index');
};
