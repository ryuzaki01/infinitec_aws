var keystone = require('keystone');
var AwsUser = keystone.list('AwsUser');
var AwsGroup = keystone.list('AwsGroup');
var translate = require('i18n').__;
var errorHelper = require('../../../utils/errorHelper');
var logger = console;

var get = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;

  locals.roles = [
    {value: 'normal', label: 'Normal'},
    {value: 'audience', label: 'Audience'},
    {value: 'presenter', label: 'Presenter'},
    {value: 'board', label: 'Board'}
  ];

  locals.levels = [
    {value: '1', label: 'Admin'},
    {value: '2', label: 'Moderator'},
    {value: '3', label: 'User'}
  ];

  locals.languages = [
    {value: 'jp', label: 'Japan'},
    {value: 'en', label: 'English'},
    {value: 'id', label: 'Indonesia'}
  ];

  locals.responseMessages = res.responseMessages;

  AwsGroup.model.findAsync().then(function(results) {
    locals.groups = results;
    view.render('user/create');
  });
};

var post = function(req, res) {
  var newUser = new AwsUser.model();

  newUser.name = req.body.name;
  newUser.username = req.body.username;
  newUser.password = req.body.password;
  newUser.group = req.body.group;

  newUser.saveAsync().then(function(result) {
    res.responseMessages = [{message: translate('awsUser.messages.create.success')}];

    /* for API
     * return res.send({
     *   error: false,
     *   result: result
     * });
     */

    return get(req, res);
  })
  .catch(function(err) {
    res.responseMessages = errorHelper.createErrorMessages(err.errors);
    errorHelper.log('AwsUser', [req.body, err, res.responseMessages]);

    /* for API
     * return res.send({
     *   error: true,
     *   messages: messages
     * })
     */

    return get(req, res);
  });
};

exports = module.exports = {
    get: get,
    post: post
};
