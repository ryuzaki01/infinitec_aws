var keystone = require('keystone');
var AwsUser = keystone.list('AwsUser');
var logger = console;

var listGET = function(req, res) {
  var view = new keystone.View(req, res);

  AwsUser.model.find().lean().execAsync()
    .then(function(users) {
      res.locals.users = users;
      view.render('user/list');
    })
    .catch(function(err) {
      logger.log('Error getting awsusers:', err);
    });
};

exports = module.exports = {
  get: listGET
};
