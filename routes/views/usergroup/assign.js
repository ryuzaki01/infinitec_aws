var keystone = require('keystone');
var bluebird = require('bluebird');
var translate = require('i18n').__;
var AwsGroup = keystone.list('AwsGroup');
var AwsUser = keystone.list('AwsUser');
var logger = console;

var assignUserGET = function assignUserGET(req, res) {
  var view = new keystone.View(req, res);
  var locals = res.locals;

  var promises = [];
  promises.push(AwsGroup.model.findAsync());
  promises.push(AwsUser.model.findAsync());


  bluebird.all(promises)
          .then(function(results) {
            locals.groups = results[0];
            locals.users = results[1];
            locals.responseMessages = res.responseMessages || [];
            console.log(locals.responseMessages);
            view.render('user-group/assign.hbs');
          })
          .catch(function(err) {
            logger.log('Error getting AwsGroup:', err);
          });
};

var assignUserPOST = function assignUserPOST(req, res) {
  var promises = [];
  var group;
  var user;

  promises.push(AwsUser.model.findOneAsync({ _id: req.body.user }));
  promises.push(AwsGroup.model.findOneAsync({ _id: req.body.group}));

  bluebird.all(promises)
          .then(function(results) {
            // we are sure that user and group exist because
            // if they don't then this promise won't be fulfilled
            user = results[0];
            group = results[1];
            user.group = req.body.group;

            return user.saveAsync();
          })
          .then(function() {
            logger.log('Saved user:', user);
            res.responseMessages = [user.username + ' ' + translate('awsUserGroup.assign.messages.success') + ' ' + group.name];
            assignUserGET(req, res);
          })
          .catch(function(err){
            res.responseMessages = [translate('awsUserGroup.errors.savingUser'), err];
            assignUserGET(req, res);
          });
};

exports = module.exports = {
  get: assignUserGET,
  post: assignUserPOST
};
