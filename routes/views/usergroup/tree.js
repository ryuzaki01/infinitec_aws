var keystone = require('keystone');
var bluebird = require('bluebird');
var translate = require('i18n').__;
var AwsGroup = keystone.list('AwsGroup');
var AwsUser = keystone.list('AwsUser');
var Room = keystone.list('Room');
var _ = require('underscore');
var logger = console;

bluebird.promisifyAll(require('mongoose'));

var treeGET = function treeGET(req, res) {
  var queryPromises = [];
  var view = new keystone.View(req, res);

  queryPromises.push(AwsGroup.model.find().lean().execAsync());
  queryPromises.push(Room.model.findOne().lean().execAsync());

  bluebird.all(queryPromises)
    .then(function(results) {
      var groups = results[0];
      var room = results[1];
      var findUserPromises = [];

      _.forEach(groups, function(g) {
        var p = AwsUser.model.find({ group: g._id }).lean().execAsync().then(function(users) {
          g.users = users;
        });

        findUserPromises.push(p);
      });

      bluebird.all(findUserPromises).then(function() {
        res.locals.groups = groups;
        res.locals.room = room;
        view.render('partials/user-group/user-group');
      });
    })
    .catch(function(err) {
      logger.log('Error getting groups:', err);
    });
};

var treePOST = function treePOST(req, res) {
  console.log(req.body);

  Room.model.findOneAsync().then(function(room) {
    room.groups = req.body.groups;
    room.users = req.body.users;

    room.saveAsync().then(function() {
      res.locals.responseMessages = [translate('userGroup.room.save.success')];
      return treeGET(req, res);
    })
    .catch(function(err) {
      logger.log('Error saving room:', err);
    });
  });
};

exports = module.exports = {
  get: treeGET,
  post: treePOST
};
