var router = require('keystone').express.Router();
var assign = require('./assign');
var tree = require('./tree');

router.get('/assign', assign.get);
router.post('/assign', assign.post);
router.get('/tree', tree.get);
router.post('/tree', tree.post);

exports = module.exports = router;
