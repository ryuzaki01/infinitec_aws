/**
 * Created by supardi on 4/30/15.
 */

var keystone = require('keystone');
var File = keystone.list('File').model;
exports = module.exports = function(req, res) {
    'use strict';
    var view = new keystone.View(req, res);
    var locals = res.locals;
    /** locals adalah variable yang bisa diakses dari .hbs **/

    locals.formData = req.body || {};
    locals.files  = [];

    view.on('init', function(next){
        File.find({}, function(err, files){
            locals.files = files;
            next();
        });
    });

    /** render ke halaman **/
    view.render('files');
};