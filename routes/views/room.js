var keystone = require('keystone');
var fs = require('fs');
var path = require('path');
var dconvert = require('dconverter')(process.env.SOFFICE_FILE, process.env.OFFICE_ENGINE);
var Room = keystone.list('Room').model;
var AwsGroup = keystone.list('AwsGroup').model;
var AwsUser = keystone.list('AwsUser').model;
var File = keystone.list('File').model;
var _ = require('underscore');
exports = module.exports = function(req, res) {
  'use strict';
  var view = new keystone.View(req, res);
  var locals = res.locals;
  var roomId = req.params.id;
  /** locals adalah variable yang bisa diakses dari .hbs **/

  locals.formData = req.body || {};

  view.on('init', function (next) {
    if (req.files.fileToUpload) {
      var fileToUpload = req.files.fileToUpload;
      var startTime = new Date().getTime();
      fs.readFile(fileToUpload.path, function (err, data) {
        var newPath = "public/uploads/original/".split("/").join(path.sep) + fileToUpload.name;
        fs.writeFile(newPath, data, function (err) {
          var fileToConvert = keystone.get('appRoot') + path.sep + newPath;
          var output = keystone.get('appRoot') + path.sep + "public/uploads/images/".split("/").join(path.sep) + fileToUpload.originalname;
          var ext = fileToUpload.extension;
          if (ext === "doc" || ext === "docx" || ext === "xls" || ext === "xlsx" || ext === "ppt" || ext === "pptx") {
            dconvert.docToImage(fileToConvert, output, "jpg", function (pageCount) {
              var newFile = new File({
                name: fileToUpload.originalname,
                data: {
                  filename: fileToUpload.name,
                  path: newPath,
                  size: fileToUpload.size,
                  filetype: fileToUpload.extension
                },
                room : roomId,
                page: pageCount
              });

              newFile.save(function (err) {
                if(err){
                  console.log('Failed to save '+ fileToUpload.originalname)
                }
                console.log('File has been saved');
              });
              if (pageCount) {
                locals.totalPages = pageCount;
                console.log("Total Page : " + pageCount);
              }
              var endTime = new Date().getTime();
              locals.processTime = endTime - startTime;
              console.log('Execution time: ' + locals.processTime);
              req.flash('info', req.__('upload.success') + ", " + req.__('upload.processtime') + "=" + locals.processTime + ", " + req.__('upload.totalpages') + "=" + locals.totalPages);

              res.redirect('/room/'+roomId);
            }, function () {
              req.flash('info', req.__('upload.error'));
              next();
            });
          } else if (ext === "pdf") {
            dconvert.pdfToImage(fileToConvert, output, "jpg", function (pageCount) {
              var newFile = new File({
                name: fileToUpload.originalname,
                data: {
                  filename: fileToUpload.name,
                  path: newPath,
                  size: fileToUpload.size,
                  filetype: fileToUpload.extension
                },
                room : roomId,
                page: pageCount
              });

              newFile.save(function (err) {
                console.log('File has been saved');
              });
              if (pageCount) {
                locals.totalPages = pageCount;
                console.log("Total Page : " + pageCount);
              }
              var endTime = new Date().getTime();
              locals.processTime = endTime - startTime;
              console.log('Execution time: ' + locals.processTime);
              req.flash('info', req.__('upload.success') + req.__('upload.processtime') + locals.processTime + req.__('upload.totalpages') + locals.totalPages);

              res.redirect('/room/'+roomId);
            }, function () {
              req.flash('info', req.__('upload.error'));
              next();
            });
          }
        });

      });
    } else {
      next();
    }
  });

  view.on('init', function(next) {
    Room.findOne().where({ "_id" : roomId}).populate('users groups').exec(function(err, room){
      locals.room = room;
    });

    AwsUser.find({}, function(err, users){
      locals.users = users;
    });

    File.find({room: roomId}, function(err, files){
      locals.files = files;
    });

    next();
  });

  view.on('init', function(next){
    AwsGroup.find({}, function(err, groups){

      _.forEach(groups, function(g) {
        AwsUser.find({ group: g._id }).lean().execAsync().then(function(users) {
          g.users = users;
        });
      });
      
      locals.groups = groups;
      next();
    });
  });

  /** render ke halaman **/
  view.render('room');
};
