var keystone = require('keystone');
var bluebird = require('bluebird');
var translate = require('i18n').__;
var AwsGroup = keystone.list('AwsGroup').model;
var AwsUser = keystone.list('AwsUser').model;
var File = keystone.list('File').model;
var Background = keystone.list('Background').model;

exports = module.exports = function(req, res) {
  var view = new keystone.View(req, res);
  var locals = res.locals;

  var promises = [];
  promises.push(AwsGroup.findAsync());
  promises.push(AwsUser.findAsync({level: 'student'}));
  promises.push(AwsUser.findAsync({level: 'teacher'}));
  promises.push(File.findAsync());
  promises.push(Background.findAsync());


  bluebird.all(promises)
    .then(function(results) {
      locals.groups = results[0];
      locals.students = results[1];
      locals.teachers = results[2];
      locals.files = results[3];
      locals.backgrounds = results[4];
      locals.responseMessages = res.responseMessages || [];
      locals.showWellcome = true;

      view.render('userManagement');
    })
    .catch(function(err) {
      logger.log('Error getting AwsGroup:', err);
    })
};