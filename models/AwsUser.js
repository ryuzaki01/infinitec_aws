/**
 * Created by ihsan on 4/12/15.
 */

var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * AWS User is used by AWS environment to access the AWS website.
 * ==========
 */

var AwsUser = new keystone.List('AwsUser', {
  autokey: {path: 'slug', from: 'username', unique: true}
});

AwsUser.add({
  name: {type: Types.Name, initial: true, required: true},
  photo: {type: Types.Text},
  group: {type: Types.Relationship, ref: 'AwsGroup', required: true, initial: true, index: true},
  username: {type: Types.Text, initial: true, required: true, index: true, unique: true},
  password: {type: Types.Text, initial: true, required: true},
  lastActive: {type: Types.Text, Default: '0'},
  role: {type: Types.Select, options: 'normal, audience, presenter, board', default: 'normal'},
  language: {type: Types.Select, options: 'id, en, jp', required: true, default: 'jp'},
  level: {type: Types.Select, options: 'admin, teacher, student', required: true, default: 'student'}
});

AwsUser.register();
