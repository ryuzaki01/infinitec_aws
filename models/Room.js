var keystone = require('keystone'),
  Types = keystone.Field.Types;


var AwsUser = keystone.list('AwsUser').model;
var AwsGroup = keystone.list('AwsGroup').model;

var Room = new keystone.List('Room', {
  autokey: {path: 'slug', from: 'name', unique: true},
  track: true
});

Room.add({
  name: {type: Types.Text, required: true, index: true},
  files: {type: Types.Relationship, ref: 'File', many: true},
  groups: {type: Types.Relationship, ref: 'AwsGroup', many: true},
  users: {type: Types.Relationship, ref: 'AwsUser', many: true},
  creator: {type: Types.Relationship, ref: 'AwsUser'}
});

Room.register();
