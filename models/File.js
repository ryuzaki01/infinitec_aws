var keystone = require('keystone'),
  Types = keystone.Field.Types;

var File = new keystone.List('File', {
  track: true
});

File.add({
  name: {type: Types.Text, required: true, index: true},
  data: {type: Types.LocalFile, dest: '/public/uploads/originial/'},
  page: {type: Types.Number, initial: true, default: 1},
  group: {type: Types.Relationship, ref: 'AwsGroup'},
  creator: {type: Types.Relationship, ref: 'AwsUser'}
});

File.register();
