/**
 * Created by ihsan on 4/12/15.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * AWS Group is User Group List.
 * ==========
 */

var AwsGroup = new keystone.List('AwsGroup', {
  autokey: {path: 'slug', from: 'name', unique: true}
});

AwsGroup.add({
  name: {type: Types.Text, initial: true, required: true, unique: true},
  status: {type: Types.Select, options: 'enabled, disabled', default: 'enabled'}
});

AwsGroup.relationship({ path: 'users', ref: 'AwsUser', refPath: 'group' });
AwsGroup.relationship({ path: 'memo', ref: 'Memo', refPath: 'group' });

AwsGroup.schema.methods.getUsers = function(done){
  return keystone.list('AwsUser').model.find()
    .where('group', this._id )
    .exec(done);
};

AwsGroup.schema.methods.getMemos = function(done){
  return keystone.list('Memo').model.find({group: this._id}, done);
};

AwsGroup.register();
