var keystone = require('keystone'),
  Types = keystone.Field.Types;

var MemoPage = new keystone.List('MemoPage', {
  track: true
});

MemoPage.add({
  background: {type: Types.Relationship, ref: 'Background'},
  thumbnail: {type: Types.LocalFile, dest: 'public/uploads/thumbnails/'},
  index: {type: Types.Number, default: 1},
  memo: {type: Types.Relationship, ref: 'Memo'}
});

MemoPage.schema.methods.getElement = function(done){
  return keystone.list('Element').model.find({page: this._id}, done);
};

MemoPage.schema.methods.setDefaultBackground = function(done){
  var self = this;
  keystone.list('Background').model.findOne({_id: '554f55a4fcc6c8a8140bce3b'}, function(err, bg){
    self.background = bg ? bg._id : null;
    self.save(function(err, bg){
      done(err, self);
    });
  });
};



MemoPage.register();
