var keystone = require('keystone'),
  Types = keystone.Field.Types;

var Image = new keystone.List('Image', {
  track: true
});

Image.add({
  name: {type: Types.Text, required: true, index: true},
  data: {type: Types.LocalFile, dest: '/public/uploads/originial/'},
  element: {type: Types.Relationship, ref: 'Element'}
});

Image.register();
