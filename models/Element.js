/**
 * Created by ihsan on 4/12/15.
 */
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * AWS Group is User Group List.
 * ==========
 */

var Element = new keystone.List('Element', {
  autokey: {path: 'slug', from: 'name', unique: true},
  track: true
});

Element.add({
  name: {type: Types.Text, initial: true, required: true},
  type: {type: Types.Select, options: 'img, image, text, comic, table, stamp, chart', required: true, default: 'img'},
  style: {
    lineSize: {type: Types.Number, default: 1},
    stroke: {type: Types.Color, default: '#000'},
    fill: {type: Types.Color, default: '#000'}
  },
  size: {
    width: {type: Types.Number, default: 1},
    height: {type: Types.Number, default: 1}
  },
  position: {
    top: {type: Types.Number, default: 0},
    left: {type: Types.Number, default: 0}
  },
  angle: {type: Types.Text, default: 0},
  data: {type: Types.Html},
  page: {type: Types.Relationship, ref: 'MemoPage'}
});

Element.register();