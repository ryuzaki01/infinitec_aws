var keystone = require('keystone'),
  Types = keystone.Field.Types;

var AwsGroup = keystone.list('AwsGroup').model;

var Background = new keystone.List('Background', {
  track: true
});

Background.add({
  name: {type: Types.Text, required: true, index: true},
  data: {type: Types.LocalFile, dest: '/public/uploads/backgrounds/'},
  creator: {type: Types.Relationship, ref: 'AwsUser'}
});

Background.schema.virtual('url').get(function () {
  if (this.data) {
    return '/uploads/backgrounds/'+this.data.filename;
  }
  return '';
});

Background.register();
