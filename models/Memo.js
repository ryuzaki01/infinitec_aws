var keystone = require('keystone'),
  Types = keystone.Field.Types;

var Memo = new keystone.List('Memo', {
  track: true
});

Memo.add({
  name: {type: Types.Text, required: true, index: true},
  page: {type: Types.Number, default: 1},
  group: {type: Types.Relationship, ref: 'AwsGroup'},
  creator: {type: Types.Relationship, ref: 'AwsUser'}
});

Memo.relationship({ path: 'pages', ref: 'MemoPage', refPath: 'memo' });

Memo.schema.methods.getFirstPage = function(done){
  return keystone.list('MemoPage').model.findOne({'memo': this._id, index: 1}).populate('background').exec(done);
};

Memo.schema.methods.getAllPages = function(done){
  return keystone.list('MemoPage').model.findOne({'memo': this._id}).populate('background').exec(done);
};

Memo.register();
