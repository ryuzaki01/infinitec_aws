'use strict';

define([
  "jquery",
  "touchbox",
  "touchpunch",
  "contextMenu",
  "board",
  "views/login",
  "views/index",
  "views/memo",
  "views/room",
  "views/usermanagement",
  'vendor/materialize/bin/waves',
], function(
  $,
  touchbox,
  touchpunch,
  contextMenu,
  board,
  loginView,
  indexView,
  memoView,
  roomView,
  userManagementView
) {
  return {
    socket: false,
  /** todo : create requirejs packages **/
    views: {
      login: loginView,
      index: indexView,
      mymemo: memoView,
      room: roomView,
      usermanagement: userManagementView
    },

    userid: 0,

    init: function () {
      Board = board;
      App.userid = $('#userid').val();
      $('#userid').remove();
      var path = window.location.pathname.split('/');
      $(document).ready(function () {
        Waves.displayEffect();

        $('.button-collapse').sideNav({
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });


        if(path && path[1] !== 'login'){
          App.socket = io(window.location.host);
          App.initSocket();
        }

        if (path && path[1] !== "" && App.views[path[1]]) {
          App.views[path[1]].init();
        } else {
          App.views.index.init();
        }
      });
    },

    pingInterval: false,

    startPing: function(){
      if(!App.pingInterval){
        App.pingInterval = setInterval(function(){
          $.post("/api/ping", {
            data: { id: App.userid},
            dataType: 'json'
          }, function (data) {

          });
        }, 1000 * 60 * 2);
      }
    },

    initSocket: function () {


      $('#message-input').on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
          this.socket.emit('cm_msg', {
            from: $('#message-user').val() !== '' ? $('#message-user').val() : 'default',
            msg: $(this).val()
          });
          $('#message-list').append('<li><strong>ME</strong> : ' + $(this).val() + '</li>');
          $(this).val('');
        }
      });

      this.socket.on('connected', function (data) {
        console.log('Connected...');
        App.startPing();
        App.socket.emit('cm_auth', {groupid: this.groupid, page: this.page, username: 'Administrator', userid: '1'});
      });

      this.socket.on('sm_auth', function (data) {
        console.log('Authed with socket id ' + data);
      });

      this.socket.on('sm_msg', function (data) {
        var msg = $('<li class="bubblechat left"><span>' + data.from + '</span><br/>' + data.msg + '</li>');
        $('#chatlist').append(msg);
        $("#chatlist").animate({scrollTop: $('ul#chatlist  li:last').offset().top + 30});
      });

      this.socket.on('disconnected', function (data) {
        clearInterval(App.pingInterval);
        alert('disconnected from server !');
        window.location.reload();
      });
    },

    sendMsg: function () {
      var message = $('#chatmsg').val();
      var msg = $('<li class="bubblechat right"><span class="me">Me</span><br/>' + message + '</li>');
      $('#chatlist').append(msg);
      $("#chatlist").animate({scrollTop: $('ul#chatlist li:last').offset().top + 30});
      App.socket.emit('cm_msg', {
        from: Username,
        msg: message
      });
      $('#chatmsg').val('');
    },

    QueryString: function () {
      // This function is anonymous, is executed immediately and
      // the return value is assigned to QueryString!
      var query_string = {};
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
          query_string[pair[0]] = pair[1];
          // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
          var arr = [query_string[pair[0]], pair[1]];
          query_string[pair[0]] = arr;
          // If third or later entry with this name
        } else {
          query_string[pair[0]].push(pair[1]);
        }
      }
      return query_string;
    }
  }
});
