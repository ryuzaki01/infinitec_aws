define(["jquery", "materialize", "panzoom"], function ($, m) {
  return {
    memo: 'test',
    file: '',
    page: 1,
    canvasContainer: document.getElementById('canvasContainer'),
    canvas: document.getElementById('canvasFake'),
    ctx: null,
    drawContainer: $('#canvasContainer'),
    memCanvas: document.createElement('canvas'),
    imageLoader: document.getElementById('uploadimagebg'),
    canvasSaveTimer: null,
    paper: {
      w: 2128,
      h: 2128
    },
    mouse: {
      x: 0,
      y: 0
    },
    last_mouse: {
      x: 0,
      y: 0
    },

    previewMode: false,
    thumbPreview: '',

    linePoints: [],

    element: [],

    elementById: [],

    deletedElement: [],

    zIndexCounter: 0,

    fill: '#000000',
    stroke: '#000000',
    lineThick: 1,
    fontSize: 14,
    font: 'Arial, Helvetica, sans-serif',
    textAlign: 'left',

    drawMode: 'line',
    currentCtxId: 0,
    inserting: 0,
    uploading: false,
    editing: false,
    editElementId: 0,

    tempRect: {},
    tempElips: {},
    tempPoly: {},
    tempText: {},

    ordering: {},

    tempChart: {},

    stampIcon: '',

    headvisible: true,
    layervisible: true,
    init: function () {
      console.log('board inited');
      $('.main-section').css({ 'margin-bottom' : 0});

      Board.ctx = Board.canvas.getContext('2d');
      Board.memo = window.location.pathname.split('/')[2];
      Board.page = window.location.pathname.split('/')[3] || 1;
      Board.pageid = $('#pageid').val();

      if (navigator.appVersion.indexOf("MSIE") != -1 && parseFloat(navigator.appVersion.split("MSIE")[1]) < 9) {
        Board.canvas.attachEvent('ontouchstart', Board.mousedown);
        Board.canvas.attachEvent('ontouchend', Board.mouseup);
        Board.canvas.attachEvent('ontouchmove', Board.mousemove);
        Board.canvas.attachEvent('onmouseout', Board.mouseout);
        Board.canvas.attachEvent('onmouseover', Board.mouseover);
        Board.canvas.attachEvent('onmousedown', Board.mousedown);
        Board.canvas.attachEvent('onmouseup', Board.mouseup);
        Board.canvas.attachEvent('onmousemove', Board.mousemove);
      } else {
        Board.canvas.addEventListener("touchstart", Board.mousedown, false);
        Board.canvas.addEventListener("touchend", Board.mouseup, false);
        Board.canvas.addEventListener("touchmove", Board.mousemove, true);
        Board.canvas.addEventListener('mouseout', Board.mouseout, false);
        Board.canvas.addEventListener('mouseover', Board.mouseover, false);
        Board.canvas.addEventListener('mousedown', Board.mousedown, false);
        Board.canvas.addEventListener('mouseup', Board.mouseup, false);
        Board.canvas.addEventListener('mousemove', Board.mousemove, false);
      }

      Board.canvas.height = Board.paper.h;
      Board.canvas.width = Board.paper.w;
      var rightPeek = $('#right-peek');
      var minimap = $('#minimap');
      var indicator = $('#minimap-indicator');
      var scaleUpW = minimap.width() / Board.drawContainer.width();
      var scaleUpH =  minimap.height() / Board.drawContainer.height();
      $(document).ready(function(){
        scaleUpW = minimap.width() / Board.drawContainer.width();
        scaleUpH = minimap.height() / Board.drawContainer.height();

        $( "#datepicker" ).datepicker();

        rightPeek.draggable({
          scroll: false,
          handle: "#panelGrip",
          axis: "x",
          create: function(){
            $('#right-peek').css({
              'width': 280,
              'left': $(window).width()
            });
          },
          drag: function(e, ui){
            if(ui.position.left < ($(window).width() - rightPeek.width())){
              ui.position.left = $(window).width() - rightPeek.width();
            }
          },
          stop: function(e, ui){
            if(ui.position.left <= $(window).width() - rightPeek.width()){
               rightPeek.css('left', $(window).width() - rightPeek.width());
            } else if(ui.position.left >= $(window).width() - 10) {
              rightPeek.css('left', $(window).width());
            } else {
              rightPeek.css('left', $(window).width() - 100);
            }
          }
        });

        $('#panelClose').on('click', function(){
          rightPeek.css({
            'width': rightPeek.css('width'),
            'left': $(window).width()
          });
        });

        indicator.css({
          'width': ($(window).width() * scaleUpW),
          'height': ($(window).height() * scaleUpH)
        }).draggable({
          scroll: false,
          containment:'parent',
          drag: Board.canvasPan,
          stop: Board.canvasPan,
          cancel: false
        });

        Board.drawContainer.on('mousedown touchstart', function(e){
          if($(e.target).attr('id') === 'canvasContainer' || (e.touches && $(e.touches[0].target).attr('id') === 'canvasContainer')){
            Board.panning = true;
            Board.panStart = {
              x : Board.toPageX(e) - Board.drawContainer.offset().left,
              y: Board.toPageY(e) - Board.drawContainer.offset().top + $('.drawContainer').offset().top
            };
            e.preventDefault();
            e.stopPropagation();
          }
        });

        Board.drawContainer.on('mousemove touchmove', function(e){
          if(Board.panning){
            leftLoc = (Board.panStart.x - Board.toPageX(e));
            topLoc = (Board.panStart.y - Board.toPageY(e))
            if(leftLoc < 0){
              leftLoc = 0;
            }
            if(leftLoc > (Board.drawContainer.width() - $(window).width())){
              leftLoc = (Board.drawContainer.width() - $(window).width());
            }
            if(topLoc < 0){
              topLoc = 0;
            }
            if(topLoc > (Board.drawContainer.height() - $(window).height())){
              topLoc = (Board.drawContainer.height() - $(window).height());
            }

            Board.drawContainer.css({
              "transform": "translate(-" + leftLoc + "px,-" + topLoc + "px)",
              "-ms-transform": "translate(-" + leftLoc + "px,-" + topLoc + "px)",
              "-webkit-transform": "translate(-" + leftLoc + "px,-" + topLoc + "px)"
            });
            indicator.css({
              'left': leftLoc * scaleUpW,
              'top': (topLoc * scaleUpW) + 12
            });

            e.stopPropagation();
          }
        });

        Board.drawContainer.on('mouseup mouseout touchend', function(e){
          if(Board.panning){
            Board.panning = false;
            if($(e.target).attr('id') === 'canvasContainer' || (e.changedTouches && $(document.elementFromPoint(e.changedTouches[0].clientX, e.changedTouches[0].clientY)).attr('id') === 'canvasContainer')){
              e.stopPropagation();
              return true;
            }
          }
        });

      });

      $(window).on('resize', function(){
        scaleUpW = minimap.width() / Board.drawContainer.width();
        scaleUpH = minimap.height() / Board.drawContainer.height();

        rightPeek.css({
          'position':'relative',
          'bottom': '10px'
        });

        rightPeek.css({
          'position':'fixed',
          'bottom': '20px'
        });

        indicator.css({
          'width': ($(window).width() * scaleUpW),
          'height': ($(window).height() * scaleUpH)
        })
      });

      App.socket.on('sm_new', function (data) {
        Board.createObject(data);

        Board.element.push(data);
        Board.elementById[data.id] = data;
      });

      App.socket.on('sm_move', function (data) {
        $('#' + data.id).css({
          'left': data.left + 'px',
          'top': data.top + 'px',
          'z-index': data.z
        });
        Board.zIndexCounter = data.z;
      });

      App.socket.on('sm_resize', function (data) {
        $('#' + data.id).css({
          'width': data.width + 'px',
          'height': data.height + 'px',
          'left': data.left + 'px',
          'top': data.top + 'px',
          'z-index': data.z
        });
      });

      App.socket.on('sm_rotate', function (data) {
        $('#' + data.id).css({
          'z-index': data.z
        }).data('ui-rotatable').performRotation(data.angle);
      });

      App.socket.on('sm_update', function (data) {
        if ((data.type == 'text' || data.type == 'table' ) && Board.editElementId != data.id) {
          $('#' + data.id).css('width', data.width + 'px').css('height', data.height + 'px');
          $('#text-' + data.id).html(unescape(data.data));
        }

        if(data.type == 'comic'){
          $('#' + data.id).css('width', data.width + 'px').css('height', data.height + 'px');
          $('#' + data.id + ' .text-edit').removeClass('lime purple red blue green pink cyan yellow orange brown grey');
          $('#' + data.id + ' .text-edit').addClass(data.style.fill).attr('data-bg', data.style.fill);
          $('#text-' + data.id + ' .card-content').html(unescape(data.data));
        }
      });

      App.socket.on('sm_delete', function (data) {
        $('#' + data.id + ' div').remove();
        $('#' + data.id).remove();
        if (Board.elementById.indexOf(data.id))
          Board.elementById.splice(Board.elementById.indexOf(data.id), 1);

        for (var i = 0; Board.element.length; i++) {
          if (Board.element[i].id == data.id)
            Board.element.splice(i, 1);
        }
      });

      Board.drawing = 0;
      Board.imageLoader = document.getElementById('uploadimagebg');
      if (typeof G_vmlCanvasManager != "undefined") {
        Board.canvas = G_vmlCanvasManager.initElement(Board.canvas);
      }

      Board.currentCtxId = 0;

      var canvasContainer =  $('#canvasContainer');

      canvasContainer.css('height', Board.paper.h + 'px');
      canvasContainer.css('width', Board.paper.w + 'px');

      $('#canvasFakeContainer').css({
        top: canvasContainer.offset().top-50,
        left: canvasContainer.offset().left
      });

      Board.memCanvas.height =Board.paper.h;
      Board.memCanvas.width =Board.paper.w;

      Board.inserting = 0;
      Board.uploading = false;
      $('#loading').show();
      Board.canvasLoadTimer = setTimeout(function(){
        Board.loadCanvas();
      }, 2000);

      Board.taskManager(Board.saveCanvas, 60000, Board.canvasSaveTimer);
    },

    taskManager: function (func, interval, taskHolder) {
      taskHolder = window.setInterval(func, interval);
    },

    stopTaskManager: function (task) {
      window.clearTimeout(task);
    },

    loadCanvas: function () {

      $.ajax({
        method: "POST",
        url: "/api/load-canvas",
        data : {pageid: Board.pageid},
        dataType: 'json',
        success: function(data){
          if(data && data.length > 0) {
            for(d in data){
              Board.createObject(data[d], true);
            }
            setTimeout(function(){
              Board.createThumb();
            }, 5000);
          }
          $('#loading').hide();
        },
        errro: function(){
          $('#loading').hide();
        }
      });
    },

    cloneFile: function (e) {

      e.preventDefault();
      $("#save-window").data("kendoWindow").close();
      Board.saveCanvas();

      $.get("/api/clone-session", {groupId: Board.memo, pagename: $('#save-filename').val()}, function (data) {
        if (data) {
          if (data.error) {
            alert(data.error);
          } else {
            alert('File Saved with ' + data.totalpage + ' page');
          }
        }
      });
    },

    loadSession: function () {
      $.get("/api/load-file", {}, function (data) {
        if (data) {
          if (data.error) {
            alert(data.error);
          } else {
            $('#load-session-list').html('');
            for (var i = 0; i < data.file.length; i++) {
              $('#load-session-list').append('<li><a href="draw.php?memo=' + data.file[i].ses_id + '&page=1">' + data.file[i].ses_name + ' | <span class="session_lastupdate">Last Update : ' + data.file[i].last_update + '</span></li>');
            }
          }
        }
      });
    },

    createObject: function (elem, fromdb, cb) {

      var newCtxId;
      var element = {};
      if (fromdb) {
        element = {
          id: elem.name,
          type: elem.type,
          style: elem.style,
          size: elem.size,
          position: elem.position,
          angle: elem.angle,
          data: elem.data,
          persistentState: 'updated'
        };

        newCtxId = elem.name;
      } else {
        element = elem;
        newCtxId = element.id;
        Materialize.toast(elem.type + ' Created', 2000);
      }


      if (element.type == 'chart') {
        try {
          element.data = JSON.parse(element.data);
        } catch (err) {
          console.log('Element data already json..');
        }
      }

      var gridsize = parseInt($('#grid-size').val());

      var $newObject = $('<div></div>')
        .attr({
          'id': newCtxId,
          'data-type': element.type
        })
        .css({
          'width': element.size.width + 'px',
          'height': element.size.height + 'px',
          'left': Math.floor(element.position.left / gridsize) * gridsize + 'px',
          'top': Math.floor(element.position.top / gridsize) * gridsize + 'px',
          'z-index': Board.zIndexCounter,
          'position': 'absolute'
        });

      switch (element.type) {
        case 'img':
          $newObject.css('background', 'url(' + element.data + ')').addClass('obj-img');
          break;
        case 'image':
          $newObject.css('background', 'url("upload/images/' + element.data + '")').addClass('obj-image');
          break;
        case 'text':
          $newObject.html('<div class="text-edit disabled" id="text-' + newCtxId + '">' + unescape(element.data) + '</div>').addClass('obj-text');
          break;
        case 'comic':
          $newObject.html('<div class="text-edit '+(element.style.fill?element.style.fill:'lime')+' disabled" id="text-' + newCtxId + '" data-bg="'+(element.style.fill?element.style.fill:'lime')+'" ><div class="card-content">' + unescape(element.data) + '</div></div>').addClass('obj-comic');
          break;
        case 'table':
          $newObject.html('<div class="table-edit disabled" id="text-' + newCtxId + '">' + unescape(element.data) + '</div>').addClass('obj-table');
          break;
        case 'chart':
          $newObject.addClass('obj-chart');
          break;
        case 'stamp':
          $newObject.css('background', 'url(images/stamp/' + element.data + ')').addClass('obj-stamp');
          break;
      }

      if ($('#' + newCtxId).length == 0)
        $('#canvasContainer').append($newObject);

      $('#' + newCtxId).append('<div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-arrowthick-2-se-nw handle-se" style="z-index: 90;"></div>')
      .resizable({
        handles: {
          'se': '.handle-se'
        },
        containment: 'parent',
        resize: Board.resize,
        stop: Board.resize,
        minHeight: 60,
        minWidth: 150,
        autoHide: true,
        cancel: false
      });

      $('#' + newCtxId)
      .rotatable({
        angle: parseFloat(element.angle?element.angle:0),
        rotate: Board.rotate,
        stop: Board.rotate
      });

      if( !$('#' + newCtxId).data('ui-rotatable').angle()){
        $('#' + newCtxId).data('ui-rotatable').angle(0);
      }

      if (element.type == 'chart') {
        var chart = new AmCharts.AmSerialChart();
        chart.dataProvider = element.data.data;
        chart.categoryField = "label";
        chart.marginTop = 15;
        chart.marginLeft = 55;
        chart.marginRight = 15;
        chart.marginBottom = 80;
        chart.angle = 0.0;
        chart.depth3D = 15;

        var catAxis = chart.categoryAxis;
        catAxis.gridCount = element.data.data.length;
        catAxis.labelRotation = 90;

        var graph = new AmCharts.AmGraph();
        graph.title = 'Chart by Dejimo';
        graph.valueField = "value";
        graph.type = "column";
        graph.lineAlpha = 0;
        graph.fillAlphas = 0.8;
        chart.addGraph(graph);
        chart.write(newCtxId);

        element.data = JSON.stringify(element.data);
      }

      var optgrid = (gridsize > 5) ? [gridsize, gridsize] : false;

      $('#' + newCtxId).draggable({
        containment: "parent",
        scroll: false,
        grid: optgrid,
        drag: Board.move,
        stop:Board.move,
        cancel: false
      });

      $('#' + newCtxId).on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#canvasContainer > div.active').removeClass('active');
        $(this).addClass('active');
        $(this).css('z-index', Board.zIndexCounter);
        Board.zIndexCounter++;
      });

      $('#' + newCtxId).TouchBox({
        resize: true,
        callback_touches: function (touches) {
          $('#canvasContainer > div.active').removeClass('active');
          $(this).addClass('active');
        },
        callback_size_change: function (newWidth, newHeight) {
          var event = $(this).attr('id');
          var ui = {
            position: {
              top: parseInt($(this).css('top')),
              left: parseInt($(this).css('left'))
            },
            size: {
              width: newWidth,
              height: newHeight
            }
          };
          Board.resize(event, ui);
        },
        callback_position_change: function (newLeft, newTop) {
          var event = $(this).attr('id');
          var ui = {
            position: {
              top: newTop,
              left: newLeft
            },
            size: {
              width: $(this).width(),
              height: $(this).height()
            }
          };
          Board.move(event, ui);
        }
      });

      if (element.type == 'text' || element.type == 'comic' || element.type == 'table') {
        $('#' + newCtxId).on('dblclick', function (e) {
          e.preventDefault();
          Board.editTextArea(newCtxId);
        });
      }

      Board.element.push(element);
      if(typeof(cb) === 'function'){
        cb.call();
      }
      Board.elementById[element.id] = element;
      Board.zIndexCounter++;
    },

    newLayer: function () {

      if (confirm('Create a new layer ?')) {
        $.post("/api/new-layer", {memo: Board.memo}, function (data) {
          if (data && data.pageid) {
            $('#layer-slide').append('<li class="layerbox" id="layer-' + Board.memo + '-' + data.pageid + '"><a class="del-layer" onclick="this.deletePage(' + data.pageid + ')" title="Delete layer ' + data.pageid + ' ?">X</a><a href="draw.php?memo=' + Board.memo + '&page=' + data.pageid + '"><img src="images/blank.jpg" /></li>');
          } else {
            alert('problem with connection, please try again.');
          }
        });
      } else {
        alert('ok, maybe next time..');
      }
    },

    deleteSession: function () {

      if (confirm('Delete this file ?')) {
        $.post("/api/del-session", {memo: Board.memo}, function (data) {
          if (data) {
            alert('File deleted !');
            location.href = './';
          } else {
            alert('problem with connection, please try again.');
          }
        });
      } else {
        alert('ok, maybe next time..');
      }
    },

    deletePage: function (pageId) {

      if (confirm('Delete this Layer ?')) {
        $.post("/api/del-layer", {memo: Board.memo, page: pageId}, function (data) {
          if (data) {
            alert('Layer deleted !');
            $('#layer-' + Board.memo + '-' + pageId).remove();
          } else {
            alert('problem with connection, please try again.');
          }
        });
      } else {
        alert('ok, maybe next time..');
      }
    },

    toggleHandTool: function () {
      Board.drawMode = 'hand';
    },

    saveCanvas: function () {
      if(!Board.pageid){
        return;
      }
      var formData = new FormData();

      console.log('saving elements..');
      var tobesavedelement = [];
      Board.element.forEach(function(el){
        if (el.persistentState != 'updated' && el.persistentState != 'removed') {
          tobesavedelement.push(el);
        }
      });

      if(tobesavedelement.length > 0){
        $.post("/api/save-canvas/"+Board.pageid, {
          data: tobesavedelement,
          dataType: 'json'
        }, function (data) {
          if (data.success) {
            for(i in tobesavedelement){
              el = tobesavedelement[i];
              if (el.persistentState == 'update_required' || el.persistentState == 'new') {
                Board.updateElement(el.id, 'persistentState', 'updated');
              }
              if (el.persistentState == 'deleted') {
                Board.updateElement(el.id, 'persistentState', 'removed');
              }
            };
          }
        });
      }
      Board.createThumb();
    },

    wrapCanvasText: function (context, words, X, Y, maxWidth, lineHeight) {
      var word = words.split('');
      var line = '';
      var y = parseInt(Y);
      var x = parseInt(X);
      var lineH = parseInt(lineHeight);

      for (var i = 0; i < word.length; i++) {
        var testLine = line + word[i];
        var metrics = context.measureText(testLine);
        var testWidth = metrics.width;

        if (testWidth > maxWidth || word[i] == '\n') {
          context.fillText(line, x, y);
          context.strokeText(line, x, y);
          line = word[i];
          y = parseInt(y + lineH);
        } else {
          line = testLine;
        }
      }

      context.fillText(line, x, y);
      context.strokeText(line, x, y);
    },

    getUnicodeIcon: function (icon) {
      var Uicon = '';
      switch (icon) {
        case 'ico-tools' :
          Uicon = "\ue000";
          break;
        case 'ico-pencil' :
          Uicon = "\ue001";
          break;
        case 'ico-image' :
          Uicon = "\u22b7";
          break;
        case 'ico-attachment' :
          Uicon = "\ue002";
          break;
        case 'ico-handdrag' :
          Uicon = "\uf0de";
          break;
        case 'ico-eraser' :
          Uicon = "\uf1f1";
          break;
        case 'ico-presentation' :
          Uicon = "\uf0c4";
          break;
        case 'ico-file' :
          Uicon = "\uf016";
          break;
        case 'ico-hospital' :
          Uicon = "\uf0c5";
          break;
        case 'ico-chart' :
          Uicon = "\ue003";
          break;
        case 'ico-chart-2' :
          Uicon = "\ue004";
          break;
        case 'ico-chart-3' :
          Uicon = "\ue005";
          break;
        case 'ico-cancel' :
          Uicon = "\ue006";
          break;
        case 'ico-checkmark' :
          Uicon = "\ue007";
          break;
        case 'ico-circle' :
          Uicon = "\ue008";
          break;
        case 'ico-square' :
          Uicon = "\ue009";
          break;
        case 'ico-polygon' :
          Uicon = "\uf1be";
          break;
        case 'ico-polygonlasso' :
          Uicon = "\uf397";
          break;
        case 'ico-superman' :
          Uicon = "\uf33f";
          break;
        case 'ico-greenlantern' :
          Uicon = "\uf340";
          break;
        case 'ico-captainamerica' :
          Uicon = "\uf341";
          break;
        case 'ico-avengers' :
          Uicon = "\uf342";
          break;
        case 'ico-punisher' :
          Uicon = "\uf343";
          break;
        case 'ico-spawn' :
          Uicon = "\uf344";
          break;
        case 'ico-xmen' :
          Uicon = "\uf345";
          break;
        case 'ico-spider' :
          Uicon = "\uf346";
          break;
        case 'ico-spiderman' :
          Uicon = "\uf347";
          break;
        case 'ico-batman' :
          Uicon = "\uf348";
          break;
        case 'ico-ironman' :
          Uicon = "\uf349";
          break;
        case 'ico-darthvader' :
          Uicon = "\uf34a";
          break;
        case 'ico-spaceinvaders' :
          Uicon = "\uf352";
          break;
        case 'ico-pokemon' :
          Uicon = "\uf354";
          break;
        case 'ico-quake' :
          Uicon = "\uf355";
          break;
        case 'ico-robocop' :
          Uicon = "\uf357";
          break;
        case 'ico-residentevil' :
          Uicon = "\uf350";
          break;
        case 'ico-angrybirds' :
          Uicon = "\uf3c1";
          break;
        case 'ico-oneup' :
          Uicon = "\uf3b7";
          break;
        case 'ico-blankstare' :
          Uicon = "&#xf13e;";
          break;
        case 'ico-sad' :
          Uicon = "\uf13d";
          break;
        case 'ico-happy' :
          Uicon = "\uf13c";
          break;
        case 'ico-play' :
          Uicon = "\uf184";
          break;
        case 'ico-pause' :
          Uicon = "\uf186";
          break;
        case 'ico-previous' :
          Uicon = "\uf18b";
          break;
        case 'ico-next' :
          Uicon = "\uf18a";
          break;
        case 'ico-rotatecounterclockwise' :
          Uicon = "\uf203";
          break;
        case 'ico-rotateclockwise' :
          Uicon = "\uf202";
          break;
        case 'ico-undo' :
          Uicon = "\uf32a";
          break;
        case 'ico-redo' :
          Uicon = "\uf32b";
          break;
        case 'ico-disk' :
          Uicon = "\ue00a";
          break;
        case 'ico-print' :
          Uicon = "\ue00b";
          break;
        case 'ico-home' :
          Uicon = "\uf015";
          break;
        case 'ico-cog' :
          Uicon = "\ue00c";
          break;
        case 'ico-folder-open-alt' :
          Uicon = "\uf115";
          break;
        case 'ico-line' :
          Uicon = "\uf1bf";
          break;
        case 'ico-type' :
          Uicon = "\ue00d";
          break;
        case 'ico-alignleft' :
          Uicon = "\uf1d7";
          break;
        case 'ico-aligncenter' :
          Uicon = "\uf1d9";
          break;
        case 'ico-alignright' :
          Uicon = "\uf1d8";
          break;
        case 'ico-number' :
          Uicon = "\ue00e";
          break;
        case 'ico-number-2' :
          Uicon = "\ue00f";
          break;
        case 'ico-number-3' :
          Uicon = "\ue010";
          break;
        case 'ico-number-4' :
          Uicon = "\ue011";
          break;
        case 'ico-number-5' :
          Uicon = "\ue012";
          break;
        case 'ico-number-6' :
          Uicon = "\ue013";
          break;
        case 'ico-number-7' :
          Uicon = "\ue014";
          break;
        case 'ico-number-8' :
          Uicon = "\ue015";
          break;
        case 'ico-number-9' :
          Uicon = "\ue016";
          break;
        case 'ico-number-10' :
          Uicon = "\ue017";
          break;
        case 'ico-number-11' :
          Uicon = "\ue018";
          break;
        case 'ico-number-12' :
          Uicon = "\ue019";
          break;
        case 'ico-number-13' :
          Uicon = "\ue01a";
          break;
        case 'ico-number-14' :
          Uicon = "\ue01b";
          break;
        case 'ico-number-15' :
          Uicon = "\ue01c";
          break;
        case 'ico-number-16' :
          Uicon = "\ue01d";
          break;
        case 'ico-number-17' :
          Uicon = "\ue01e";
          break;
        case 'ico-number-18' :
          Uicon = "\ue01f";
          break;
        case 'ico-number-19' :
          Uicon = "\ue020";
          break;
        case 'ico-number-20' :
          Uicon = "\ue021";
          break;
        case 'ico-stamp' :
          Uicon = "\ue022";
          break;
        case 'ico-upload' :
          Uicon = "\ue023";
          break;
        case 'ico-spinner' :
          Uicon = "\ue024";
          break;
        case 'ico-cupcake' :
          Uicon = "\uf35b";
          break;
        case 'ico-birthday' :
          Uicon = "\uf36b";
          break;
        case 'ico-christmastree' :
          Uicon = "\uf37b";
          break;
        case 'ico-pizza' :
          Uicon = "\uf35c";
          break;
        case 'ico-cherry' :
          Uicon = "\uf35d";
          break;
        case 'ico-bone' :
          Uicon = "\uf35f";
          break;
        case 'ico-steak' :
          Uicon = "\uf360";
          break;
        case 'ico-muffin' :
          Uicon = "\uf363";
          break;
        case 'ico-loved' :
          Uicon = "\uf131";
          break;
        case 'ico-favorite' :
          Uicon = "\uf13a";
          break;
        case 'ico-favorite2' :
          Uicon = "\uf13b";
          break;
        case 'ico-love' :
          Uicon = "\uf132";
          break;
        case 'ico-laugh' :
          Uicon = "\uf13f";
          break;
        case 'ico-fantastico' :
          Uicon = "\uf0ae";
          break;
        case 'ico-aries' :
          Uicon = "\uf3aa";
          break;
        case 'ico-taurus' :
          Uicon = "\uf3ab";
          break;
        case 'ico-gemini' :
          Uicon = "\uf3ac";
          break;
        case 'ico-cancer' :
          Uicon = "\uf3ad";
          break;
        case 'ico-leo' :
          Uicon = "\uf3ae";
          break;
        case 'ico-virgo' :
          Uicon = "\uf3af";
          break;
        case 'ico-libra' :
          Uicon = "\uf3b0";
          break;
        case 'ico-scorpio' :
          Uicon = "\uf3b1";
          break;
        case 'ico-sagitarius' :
          Uicon = "\uf3b2";
          break;
        case 'ico-capricorn' :
          Uicon = "\uf3b3";
          break;
        case 'ico-aquarius' :
          Uicon = "\uf3b4";
          break;
        case 'ico-pisces' :
          Uicon = "\uf3b5";
          break;
        default:
          Uicon = 'Stamp Not Found !';
          break;
      }
      return Uicon;
    },

    createThumb: function () {

      var canvas = document.getElementById('previewCanvas');
      var context = canvas.getContext('2d');

      canvas.width = Board.canvas.width;
      canvas.height = Board.canvas.height;

      for (var i = 0; i < Board.element.length; i++) {
        var element = Board.element[i];
        switch (element.type) {
          case 'img':
            var image = new Image();
            image.src = element.data;
            context.drawImage(image, element.position.left, element.position.top, element.size.width, element.size.height);
            break;
          case 'image':
            var image = new Image();
            image.src = 'upload/images/' + element.data;
            context.drawImage(image, element.position.left, element.position.top, element.size.width, element.size.height);
            break;
          case 'comic':
          case 'text':
            context.fillStyle = element.style.fill;
            context.fillRect(element.position.left, element.position.top, element.size.width, element.size.height);
            break;
          case 'stamp':
            context.font = element.size.width + 'px infinitec-draw';
            context.textAlign = 'center';
            context.fillStyle = element.style.fill;
            var icon =Board.getUnicodeIcon(element.data);
            context.fillText(icon, element.position.left, element.position.top);
        }
      }

      thumbCanvas = document.createElement('canvas');
      thumbCanvas.width = 120;
      thumbCanvas.height = 120;
      thumbCanvas.getContext("2d").drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, 120, 120);

      $('.layers ul li:eq(' + Board.page + ') img').attr('src', thumbCanvas.toDataURL('image/png')).css('width', '100%').css('height', '100%');
      Board.thumbPreview = thumbCanvas.toDataURL('image/png');
      $('#minimap-thumbcanvas').attr('src', thumbCanvas.toDataURL('image/png'));

      // html2canvas(Board.canvasContainer, {
      // onrendered: function(xcanvas) {
      // var thumbData = Canvas2Image.convertToPNG(xcanvas, 80, 60);
      // $('.layers ul li:eq('+this.page+') img').attr('src', thumbData.src).css('width', '100%').css('height', '100%');
      //Board.thumbPreview = thumbData.src;
      // }
      // });
    },

    createPreview: function () {

      if (Board.previewMode) {
        $('.controls').attr('style', 'width: 170px;');
        $('#canvasContainer').show();
        $('#previewCanvasContainer').hide();
        Board.previewMode = false;
      } else {
        $('#previewCanvasContainer').css('width', $('#canvasContainer').width() + 'px').css('height', $('#canvasContainer').height() + 'px');
        $('#previewCanvasContainer').show();
        $('.controls').attr('style', 'width: 0px;');
        Board.previewMode = true;

        var canvas = document.getElementById('previewCanvas');
        var context = canvas.getContext('2d');

        canvas.width = Board.paper.w;
        canvas.height = Board.paper.h;

        for (var i = 0; i < Board.element.length; i++) {
          var element =Board.element[i];
          switch (element.type) {
            case 'img':
              var image = new Image();
              image.src = element.data;
              context.drawImage(image, element.position.left, element.position.top, element.size.width, element.size.height);
              break;
            case 'image':
              var image = new Image();
              image.src = 'upload/images/' + element.data;
              context.drawImage(image, element.position.left, element.position.top, element.size.width, element.size.height);
              break;
            case 'text':
              var left = element.position.left;
              var top = element.position.top;
              switch (element.style.textAlign) {
                case 'right':
                  left = left + element.size.width;
                  break;
                case 'center':
                  left = left + (element.size.width / 2);
                  break;
                default:
                  break;
              }

              //this.wrapCanvasText(context, element.data, left, top, element.size.width, element.style.fontsize);
              break;
            case 'stamp':
              var image = new Image();
              image.src = 'images/stamp/' + element.data;
              context.drawImage(image, element.position.left, element.position.top, element.size.width, element.size.height);
              break;
            case 'chart':

              break;
          }
        }

        $('.layers ul li:eq(' + Board.page + ') img').attr('src', canvas.toDataURL("image/png")).css('width', '100%').css('height', '100%');

        $('#canvasContainer').hide();
      }
    },

    htmlDecode: function (s) {
      var out = "";
      if (s == null) return;

      var l = s.length;
      for (var i = 0; i < l; i++) {
        var ch = s.charAt(i);

        if (ch == '&') {
          var semicolonIndex = s.indexOf(';', i + 1);

          if (semicolonIndex > 0) {
            var entity = s.substring(i + 1, semicolonIndex);
            if (entity.length > 1 && entity.charAt(0) == '#') {
              if (entity.charAt(1) == 'x' || entity.charAt(1) == 'X')
                ch = String.fromCharCode(eval('0' + entity.substring(1)));
              else
                ch = String.fromCharCode(eval(entity.substring(1)));
            }
            else {
              switch (entity) {
                case 'quot':
                  ch = String.fromCharCode(0x0022);
                  break;
                case 'amp':
                  ch = String.fromCharCode(0x0026);
                  break;
                case 'lt':
                  ch = String.fromCharCode(0x003c);
                  break;
                case 'gt':
                  ch = String.fromCharCode(0x003e);
                  break;
                case 'nbsp':
                  ch = String.fromCharCode(0x00a0);
                  break;
                case 'iexcl':
                  ch = String.fromCharCode(0x00a1);
                  break;
                case 'cent':
                  ch = String.fromCharCode(0x00a2);
                  break;
                case 'pound':
                  ch = String.fromCharCode(0x00a3);
                  break;
                case 'curren':
                  ch = String.fromCharCode(0x00a4);
                  break;
                case 'yen':
                  ch = String.fromCharCode(0x00a5);
                  break;
                case 'brvbar':
                  ch = String.fromCharCode(0x00a6);
                  break;
                case 'sect':
                  ch = String.fromCharCode(0x00a7);
                  break;
                case 'uml':
                  ch = String.fromCharCode(0x00a8);
                  break;
                case 'copy':
                  ch = String.fromCharCode(0x00a9);
                  break;
                case 'ordf':
                  ch = String.fromCharCode(0x00aa);
                  break;
                case 'laquo':
                  ch = String.fromCharCode(0x00ab);
                  break;
                case 'not':
                  ch = String.fromCharCode(0x00ac);
                  break;
                case 'shy':
                  ch = String.fromCharCode(0x00ad);
                  break;
                case 'reg':
                  ch = String.fromCharCode(0x00ae);
                  break;
                case 'macr':
                  ch = String.fromCharCode(0x00af);
                  break;
                case 'deg':
                  ch = String.fromCharCode(0x00b0);
                  break;
                case 'plusmn':
                  ch = String.fromCharCode(0x00b1);
                  break;
                case 'sup2':
                  ch = String.fromCharCode(0x00b2);
                  break;
                case 'sup3':
                  ch = String.fromCharCode(0x00b3);
                  break;
                case 'acute':
                  ch = String.fromCharCode(0x00b4);
                  break;
                case 'micro':
                  ch = String.fromCharCode(0x00b5);
                  break;
                case 'para':
                  ch = String.fromCharCode(0x00b6);
                  break;
                case 'middot':
                  ch = String.fromCharCode(0x00b7);
                  break;
                case 'cedil':
                  ch = String.fromCharCode(0x00b8);
                  break;
                case 'sup1':
                  ch = String.fromCharCode(0x00b9);
                  break;
                case 'ordm':
                  ch = String.fromCharCode(0x00ba);
                  break;
                case 'raquo':
                  ch = String.fromCharCode(0x00bb);
                  break;
                case 'frac14':
                  ch = String.fromCharCode(0x00bc);
                  break;
                case 'frac12':
                  ch = String.fromCharCode(0x00bd);
                  break;
                case 'frac34':
                  ch = String.fromCharCode(0x00be);
                  break;
                case 'iquest':
                  ch = String.fromCharCode(0x00bf);
                  break;
                case 'Agrave':
                  ch = String.fromCharCode(0x00c0);
                  break;
                case 'Aacute':
                  ch = String.fromCharCode(0x00c1);
                  break;
                case 'Acirc':
                  ch = String.fromCharCode(0x00c2);
                  break;
                case 'Atilde':
                  ch = String.fromCharCode(0x00c3);
                  break;
                case 'Auml':
                  ch = String.fromCharCode(0x00c4);
                  break;
                case 'Aring':
                  ch = String.fromCharCode(0x00c5);
                  break;
                case 'AElig':
                  ch = String.fromCharCode(0x00c6);
                  break;
                case 'Ccedil':
                  ch = String.fromCharCode(0x00c7);
                  break;
                case 'Egrave':
                  ch = String.fromCharCode(0x00c8);
                  break;
                case 'Eacute':
                  ch = String.fromCharCode(0x00c9);
                  break;
                case 'Ecirc':
                  ch = String.fromCharCode(0x00ca);
                  break;
                case 'Euml':
                  ch = String.fromCharCode(0x00cb);
                  break;
                case 'Igrave':
                  ch = String.fromCharCode(0x00cc);
                  break;
                case 'Iacute':
                  ch = String.fromCharCode(0x00cd);
                  break;
                case 'Icirc':
                  ch = String.fromCharCode(0x00ce);
                  break;
                case 'Iuml':
                  ch = String.fromCharCode(0x00cf);
                  break;
                case 'ETH':
                  ch = String.fromCharCode(0x00d0);
                  break;
                case 'Ntilde':
                  ch = String.fromCharCode(0x00d1);
                  break;
                case 'Ograve':
                  ch = String.fromCharCode(0x00d2);
                  break;
                case 'Oacute':
                  ch = String.fromCharCode(0x00d3);
                  break;
                case 'Ocirc':
                  ch = String.fromCharCode(0x00d4);
                  break;
                case 'Otilde':
                  ch = String.fromCharCode(0x00d5);
                  break;
                case 'Ouml':
                  ch = String.fromCharCode(0x00d6);
                  break;
                case 'times':
                  ch = String.fromCharCode(0x00d7);
                  break;
                case 'Oslash':
                  ch = String.fromCharCode(0x00d8);
                  break;
                case 'Ugrave':
                  ch = String.fromCharCode(0x00d9);
                  break;
                case 'Uacute':
                  ch = String.fromCharCode(0x00da);
                  break;
                case 'Ucirc':
                  ch = String.fromCharCode(0x00db);
                  break;
                case 'Uuml':
                  ch = String.fromCharCode(0x00dc);
                  break;
                case 'Yacute':
                  ch = String.fromCharCode(0x00dd);
                  break;
                case 'THORN':
                  ch = String.fromCharCode(0x00de);
                  break;
                case 'szlig':
                  ch = String.fromCharCode(0x00df);
                  break;
                case 'agrave':
                  ch = String.fromCharCode(0x00e0);
                  break;
                case 'aacute':
                  ch = String.fromCharCode(0x00e1);
                  break;
                case 'acirc':
                  ch = String.fromCharCode(0x00e2);
                  break;
                case 'atilde':
                  ch = String.fromCharCode(0x00e3);
                  break;
                case 'auml':
                  ch = String.fromCharCode(0x00e4);
                  break;
                case 'aring':
                  ch = String.fromCharCode(0x00e5);
                  break;
                case 'aelig':
                  ch = String.fromCharCode(0x00e6);
                  break;
                case 'ccedil':
                  ch = String.fromCharCode(0x00e7);
                  break;
                case 'egrave':
                  ch = String.fromCharCode(0x00e8);
                  break;
                case 'eacute':
                  ch = String.fromCharCode(0x00e9);
                  break;
                case 'ecirc':
                  ch = String.fromCharCode(0x00ea);
                  break;
                case 'euml':
                  ch = String.fromCharCode(0x00eb);
                  break;
                case 'igrave':
                  ch = String.fromCharCode(0x00ec);
                  break;
                case 'iacute':
                  ch = String.fromCharCode(0x00ed);
                  break;
                case 'icirc':
                  ch = String.fromCharCode(0x00ee);
                  break;
                case 'iuml':
                  ch = String.fromCharCode(0x00ef);
                  break;
                case 'eth':
                  ch = String.fromCharCode(0x00f0);
                  break;
                case 'ntilde':
                  ch = String.fromCharCode(0x00f1);
                  break;
                case 'ograve':
                  ch = String.fromCharCode(0x00f2);
                  break;
                case 'oacute':
                  ch = String.fromCharCode(0x00f3);
                  break;
                case 'ocirc':
                  ch = String.fromCharCode(0x00f4);
                  break;
                case 'otilde':
                  ch = String.fromCharCode(0x00f5);
                  break;
                case 'ouml':
                  ch = String.fromCharCode(0x00f6);
                  break;
                case 'divide':
                  ch = String.fromCharCode(0x00f7);
                  break;
                case 'oslash':
                  ch = String.fromCharCode(0x00f8);
                  break;
                case 'ugrave':
                  ch = String.fromCharCode(0x00f9);
                  break;
                case 'uacute':
                  ch = String.fromCharCode(0x00fa);
                  break;
                case 'ucirc':
                  ch = String.fromCharCode(0x00fb);
                  break;
                case 'uuml':
                  ch = String.fromCharCode(0x00fc);
                  break;
                case 'yacute':
                  ch = String.fromCharCode(0x00fd);
                  break;
                case 'thorn':
                  ch = String.fromCharCode(0x00fe);
                  break;
                case 'yuml':
                  ch = String.fromCharCode(0x00ff);
                  break;
                case 'OElig':
                  ch = String.fromCharCode(0x0152);
                  break;
                case 'oelig':
                  ch = String.fromCharCode(0x0153);
                  break;
                case 'Scaron':
                  ch = String.fromCharCode(0x0160);
                  break;
                case 'scaron':
                  ch = String.fromCharCode(0x0161);
                  break;
                case 'Yuml':
                  ch = String.fromCharCode(0x0178);
                  break;
                case 'fnof':
                  ch = String.fromCharCode(0x0192);
                  break;
                case 'circ':
                  ch = String.fromCharCode(0x02c6);
                  break;
                case 'tilde':
                  ch = String.fromCharCode(0x02dc);
                  break;
                case 'Alpha':
                  ch = String.fromCharCode(0x0391);
                  break;
                case 'Beta':
                  ch = String.fromCharCode(0x0392);
                  break;
                case 'Gamma':
                  ch = String.fromCharCode(0x0393);
                  break;
                case 'Delta':
                  ch = String.fromCharCode(0x0394);
                  break;
                case 'Epsilon':
                  ch = String.fromCharCode(0x0395);
                  break;
                case 'Zeta':
                  ch = String.fromCharCode(0x0396);
                  break;
                case 'Eta':
                  ch = String.fromCharCode(0x0397);
                  break;
                case 'Theta':
                  ch = String.fromCharCode(0x0398);
                  break;
                case 'Iota':
                  ch = String.fromCharCode(0x0399);
                  break;
                case 'Kappa':
                  ch = String.fromCharCode(0x039a);
                  break;
                case 'Lambda':
                  ch = String.fromCharCode(0x039b);
                  break;
                case 'Mu':
                  ch = String.fromCharCode(0x039c);
                  break;
                case 'Nu':
                  ch = String.fromCharCode(0x039d);
                  break;
                case 'Xi':
                  ch = String.fromCharCode(0x039e);
                  break;
                case 'Omicron':
                  ch = String.fromCharCode(0x039f);
                  break;
                case 'Pi':
                  ch = String.fromCharCode(0x03a0);
                  break;
                case ' Rho ':
                  ch = String.fromCharCode(0x03a1);
                  break;
                case 'Sigma':
                  ch = String.fromCharCode(0x03a3);
                  break;
                case 'Tau':
                  ch = String.fromCharCode(0x03a4);
                  break;
                case 'Upsilon':
                  ch = String.fromCharCode(0x03a5);
                  break;
                case 'Phi':
                  ch = String.fromCharCode(0x03a6);
                  break;
                case 'Chi':
                  ch = String.fromCharCode(0x03a7);
                  break;
                case 'Psi':
                  ch = String.fromCharCode(0x03a8);
                  break;
                case 'Omega':
                  ch = String.fromCharCode(0x03a9);
                  break;
                case 'alpha':
                  ch = String.fromCharCode(0x03b1);
                  break;
                case 'beta':
                  ch = String.fromCharCode(0x03b2);
                  break;
                case 'gamma':
                  ch = String.fromCharCode(0x03b3);
                  break;
                case 'delta':
                  ch = String.fromCharCode(0x03b4);
                  break;
                case 'epsilon':
                  ch = String.fromCharCode(0x03b5);
                  break;
                case 'zeta':
                  ch = String.fromCharCode(0x03b6);
                  break;
                case 'eta':
                  ch = String.fromCharCode(0x03b7);
                  break;
                case 'theta':
                  ch = String.fromCharCode(0x03b8);
                  break;
                case 'iota':
                  ch = String.fromCharCode(0x03b9);
                  break;
                case 'kappa':
                  ch = String.fromCharCode(0x03ba);
                  break;
                case 'lambda':
                  ch = String.fromCharCode(0x03bb);
                  break;
                case 'mu':
                  ch = String.fromCharCode(0x03bc);
                  break;
                case 'nu':
                  ch = String.fromCharCode(0x03bd);
                  break;
                case 'xi':
                  ch = String.fromCharCode(0x03be);
                  break;
                case 'omicron':
                  ch = String.fromCharCode(0x03bf);
                  break;
                case 'pi':
                  ch = String.fromCharCode(0x03c0);
                  break;
                case 'rho':
                  ch = String.fromCharCode(0x03c1);
                  break;
                case 'sigmaf':
                  ch = String.fromCharCode(0x03c2);
                  break;
                case 'sigma':
                  ch = String.fromCharCode(0x03c3);
                  break;
                case 'tau':
                  ch = String.fromCharCode(0x03c4);
                  break;
                case 'upsilon':
                  ch = String.fromCharCode(0x03c5);
                  break;
                case 'phi':
                  ch = String.fromCharCode(0x03c6);
                  break;
                case 'chi':
                  ch = String.fromCharCode(0x03c7);
                  break;
                case 'psi':
                  ch = String.fromCharCode(0x03c8);
                  break;
                case 'omega':
                  ch = String.fromCharCode(0x03c9);
                  break;
                case 'thetasym':
                  ch = String.fromCharCode(0x03d1);
                  break;
                case 'upsih':
                  ch = String.fromCharCode(0x03d2);
                  break;
                case 'piv':
                  ch = String.fromCharCode(0x03d6);
                  break;
                case 'ensp':
                  ch = String.fromCharCode(0x2002);
                  break;
                case 'emsp':
                  ch = String.fromCharCode(0x2003);
                  break;
                case 'thinsp':
                  ch = String.fromCharCode(0x2009);
                  break;
                case 'zwnj':
                  ch = String.fromCharCode(0x200c);
                  break;
                case 'zwj':
                  ch = String.fromCharCode(0x200d);
                  break;
                case 'lrm':
                  ch = String.fromCharCode(0x200e);
                  break;
                case 'rlm':
                  ch = String.fromCharCode(0x200f);
                  break;
                case 'ndash':
                  ch = String.fromCharCode(0x2013);
                  break;
                case 'mdash':
                  ch = String.fromCharCode(0x2014);
                  break;
                case 'lsquo':
                  ch = String.fromCharCode(0x2018);
                  break;
                case 'rsquo':
                  ch = String.fromCharCode(0x2019);
                  break;
                case 'sbquo':
                  ch = String.fromCharCode(0x201a);
                  break;
                case 'ldquo':
                  ch = String.fromCharCode(0x201c);
                  break;
                case 'rdquo':
                  ch = String.fromCharCode(0x201d);
                  break;
                case 'bdquo':
                  ch = String.fromCharCode(0x201e);
                  break;
                case 'dagger':
                  ch = String.fromCharCode(0x2020);
                  break;
                case 'Dagger':
                  ch = String.fromCharCode(0x2021);
                  break;
                case 'bull':
                  ch = String.fromCharCode(0x2022);
                  break;
                case 'hellip':
                  ch = String.fromCharCode(0x2026);
                  break;
                case 'permil':
                  ch = String.fromCharCode(0x2030);
                  break;
                case 'prime':
                  ch = String.fromCharCode(0x2032);
                  break;
                case 'Prime':
                  ch = String.fromCharCode(0x2033);
                  break;
                case 'lsaquo':
                  ch = String.fromCharCode(0x2039);
                  break;
                case 'rsaquo':
                  ch = String.fromCharCode(0x203a);
                  break;
                case 'oline':
                  ch = String.fromCharCode(0x203e);
                  break;
                case 'frasl':
                  ch = String.fromCharCode(0x2044);
                  break;
                case 'euro':
                  ch = String.fromCharCode(0x20ac);
                  break;
                case 'image':
                  ch = String.fromCharCode(0x2111);
                  break;
                case 'weierp':
                  ch = String.fromCharCode(0x2118);
                  break;
                case 'real':
                  ch = String.fromCharCode(0x211c);
                  break;
                case 'trade':
                  ch = String.fromCharCode(0x2122);
                  break;
                case 'alefsym':
                  ch = String.fromCharCode(0x2135);
                  break;
                case 'larr':
                  ch = String.fromCharCode(0x2190);
                  break;
                case 'uarr':
                  ch = String.fromCharCode(0x2191);
                  break;
                case 'rarr':
                  ch = String.fromCharCode(0x2192);
                  break;
                case 'darr':
                  ch = String.fromCharCode(0x2193);
                  break;
                case 'harr':
                  ch = String.fromCharCode(0x2194);
                  break;
                case 'crarr':
                  ch = String.fromCharCode(0x21b5);
                  break;
                case 'lArr':
                  ch = String.fromCharCode(0x21d0);
                  break;
                case 'uArr':
                  ch = String.fromCharCode(0x21d1);
                  break;
                case 'rArr':
                  ch = String.fromCharCode(0x21d2);
                  break;
                case 'dArr':
                  ch = String.fromCharCode(0x21d3);
                  break;
                case 'hArr':
                  ch = String.fromCharCode(0x21d4);
                  break;
                case 'forall':
                  ch = String.fromCharCode(0x2200);
                  break;
                case 'part':
                  ch = String.fromCharCode(0x2202);
                  break;
                case 'exist':
                  ch = String.fromCharCode(0x2203);
                  break;
                case 'empty':
                  ch = String.fromCharCode(0x2205);
                  break;
                case 'nabla':
                  ch = String.fromCharCode(0x2207);
                  break;
                case 'isin':
                  ch = String.fromCharCode(0x2208);
                  break;
                case 'notin':
                  ch = String.fromCharCode(0x2209);
                  break;
                case 'ni':
                  ch = String.fromCharCode(0x220b);
                  break;
                case 'prod':
                  ch = String.fromCharCode(0x220f);
                  break;
                case 'sum':
                  ch = String.fromCharCode(0x2211);
                  break;
                case 'minus':
                  ch = String.fromCharCode(0x2212);
                  break;
                case 'lowast':
                  ch = String.fromCharCode(0x2217);
                  break;
                case 'radic':
                  ch = String.fromCharCode(0x221a);
                  break;
                case 'prop':
                  ch = String.fromCharCode(0x221d);
                  break;
                case 'infin':
                  ch = String.fromCharCode(0x221e);
                  break;
                case 'ang':
                  ch = String.fromCharCode(0x2220);
                  break;
                case 'and':
                  ch = String.fromCharCode(0x2227);
                  break;
                case 'or':
                  ch = String.fromCharCode(0x2228);
                  break;
                case 'cap':
                  ch = String.fromCharCode(0x2229);
                  break;
                case 'cup':
                  ch = String.fromCharCode(0x222a);
                  break;
                case 'int':
                  ch = String.fromCharCode(0x222b);
                  break;
                case 'there4':
                  ch = String.fromCharCode(0x2234);
                  break;
                case 'sim':
                  ch = String.fromCharCode(0x223c);
                  break;
                case 'cong':
                  ch = String.fromCharCode(0x2245);
                  break;
                case 'asymp':
                  ch = String.fromCharCode(0x2248);
                  break;
                case 'ne':
                  ch = String.fromCharCode(0x2260);
                  break;
                case 'equiv':
                  ch = String.fromCharCode(0x2261);
                  break;
                case 'le':
                  ch = String.fromCharCode(0x2264);
                  break;
                case 'ge':
                  ch = String.fromCharCode(0x2265);
                  break;
                case 'sub':
                  ch = String.fromCharCode(0x2282);
                  break;
                case 'sup':
                  ch = String.fromCharCode(0x2283);
                  break;
                case 'nsub':
                  ch = String.fromCharCode(0x2284);
                  break;
                case 'sube':
                  ch = String.fromCharCode(0x2286);
                  break;
                case 'supe':
                  ch = String.fromCharCode(0x2287);
                  break;
                case 'oplus':
                  ch = String.fromCharCode(0x2295);
                  break;
                case 'otimes':
                  ch = String.fromCharCode(0x2297);
                  break;
                case 'perp':
                  ch = String.fromCharCode(0x22a5);
                  break;
                case 'sdot':
                  ch = String.fromCharCode(0x22c5);
                  break;
                case 'lceil':
                  ch = String.fromCharCode(0x2308);
                  break;
                case 'rceil':
                  ch = String.fromCharCode(0x2309);
                  break;
                case 'lfloor':
                  ch = String.fromCharCode(0x230a);
                  break;
                case 'rfloor':
                  ch = String.fromCharCode(0x230b);
                  break;
                case 'lang':
                  ch = String.fromCharCode(0x2329);
                  break;
                case 'rang':
                  ch = String.fromCharCode(0x232a);
                  break;
                case 'loz':
                  ch = String.fromCharCode(0x25ca);
                  break;
                case 'spades':
                  ch = String.fromCharCode(0x2660);
                  break;
                case 'clubs':
                  ch = String.fromCharCode(0x2663);
                  break;
                case 'hearts':
                  ch = String.fromCharCode(0x2665);
                  break;
                case 'diams':
                  ch = String.fromCharCode(0x2666);
                  break;
                default:
                  ch = '';
                  break;
              }
            }
            i = semicolonIndex;
          }
        }

        out += ch;
      }

      return out;
    },

    toPageX: function (e) {
      var posx = 0;

      if (e.pageX && !e.changedTouches) {
        posx = e.pageX;
      } else if (e.clientX) {
        posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      } else if (e.changedTouches[0].clientX) {
        posx = e.changedTouches[0].clientX;
      }
      return posx;
    },

    toPageY: function (e) {
      var posy = 0;

      if (e.pageY && !e.changedTouches) {
        posy = e.pageY;
      } else if (e.clientY) {
        posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      } else if (e.changedTouches[0].clientY) {
        posy = e.changedTouches[0].clientY;
      }

      return posy;
    },

    toCanvasX: function (e) {
      var posx = 0;
      if (e.pageX && !e.changedTouches) {
        posx = e.pageX +Board.drawContainer.scrollLeft();
      } else if (e.clientX) {
        posx = e.clientX + document.body.scrollLeft()
          + document.documentElement.scrollLeft +Board.drawContainer.scrollLeft();
      } else if (e.changedTouches) {
        posx = e.changedTouches[0].clientX + document.body.scrollLeft +Board.drawContainer.scrollLeft();
      }
      return posx;
    },

    toCanvasY: function (e) {
      var posy = 0;

      if (e.pageY && !e.changedTouches) {
        posy = e.pageY +Board.drawContainer.scrollTop();
      } else if (e.clientY) {
        posy = e.clientY + document.body.scrollTop
          + document.documentElement.scrollTop +Board.drawContainer.scrollTop();
      } else if (e.changedTouches) {
        posy = e.changedTouches[0].clientY + document.body.scrollTop +Board.drawContainer.scrollTop();
      }
      posy = posy - $('.drawContainer').offset().top;

      return posy;
    },

    handleImage: function (e) {
      var reader = new FileReader();
      var type, x, y, imgresult;
      reader.onload = function (event) {
        imgresult = event.target.result;
        type = 'dragend';
        e.offsetX = (e.layerX);
        e.offsetY = (e.layerY);
        x = e.offsetX;
        y = e.offsetY;
        Board.drawbg(Board.currentCtxId, x, y, type, imgresult);
      };
      reader.readAsDataURL(e.target.files[0]);
      console.log('background imaging..');
    },

    getElement: function (id) {

      for (var i = 0; i < Board.element.length; i++) {
        if (Board.element[i].id == id) {
          return Board.element[i];
        }
      }
    },

    updateElement: function (id, key, value) {

      for (var i = 0; i < Board.element.length; i++) {
        if (Board.element[i].id == id) {
          if(key === 'persistentState' && value === 'removed'){
            Board.element.splice(i, 1);
          } else {
            Board.element[i][key] = value;
          }
        }
      }
    },

    removeElement: function (id) {
      Board.updateElement(id, 'persistentState', 'deleted');
    },

    canvasPan: function(event, ui){
      var scaleUpW = ($('#canvasContainer').width() - $(window).width() ) / 90;
      var scaleUpH = ($('#canvasContainer').height() - $(window).height()) / 90;
      var left = ui.position.left * scaleUpW;
      var top = (ui.position.top - 12) * scaleUpH;
      left = -left;
      top = -top;
      if(top < 0 || left < 0){
        $('#canvasContainer').css({
          "transform":"translate("+left+"px,"+top+"px)",
          "-ms-transform":"translate("+left+"px,"+top+"px)",
          "-webkit-transform":"translate("+left+"px,"+top+"px)"
        });
      }
    },

    canvasMove: function (event, ui) {
      if (Board.editing ||Board.inserting)
        return;

      $('#previewCanvasContainer').css({
        top: ui.position.top,
        left: ui.position.left
      });

      $('#canvasFakeContainer').css({
        top: $(this)[0].getBoundingClientRect().top - 50,
        left: $(this)[0].getBoundingClientRect().left
      });

      App.socket.emit('cm_canvas_move', {
        memo: Board.memo,
        page: Board.page,
        top: ui.position.top,
        left: ui.position.left
      });
    },

    move: function (event, ui) {
      event.stopPropagation();
      if (Board.editing ||Board.inserting)
        return;

      var elid = $(this).attr('id') ? $(this).attr('id') : event;
      var zindex = parseInt($(this).css('z-index'));

      App.socket.emit('cm_move', {
        id: elid,
        memo: Board.memo,
        page: Board.page,
        top: ui.position.top,
        left: ui.position.left,
        z: zindex
      });

      Board.updateElement(elid, 'position', {
        top: ui.position.top,
        left: ui.position.left
      });

      Board.updateElement(elid, 'z', zindex);
      Board.zIndexCounter = zindex;

      Board.updateElement(elid.replace("el-wrap-", ""), 'persistentState', 'update_required');
    },

    rotate: function (event, ui) {
      if (Board.editing ||Board.inserting)
        return;

      var self = $(this);

      var elid = self.attr('id') ? self.attr('id') : event;
      var zindex = parseInt($(this).css('z-index'));
      App.socket.emit('cm_rotate', {
        id: elid,
        memo: Board.memo,
        page: Board.page,
        angle: ui.angle.current,
        z: zindex
      });

      Board.updateElement(elid, 'angle', ui.angle.current);
      Board.updateElement(elid, 'z', zindex);

      Board.updateElement(elid.replace("el-wrap-", ""), 'persistentState', 'update_required');
    },

    resize: function (event, ui) {

      if (Board.editing || Board.inserting) {
        return;

      }

      var elid = $(this).attr('id') ? $(this).attr('id') : event;
      var zindex = parseInt($(this).css('z-index'));
      App.socket.emit('cm_resize', {
        id: elid,
        memo: Board.memo,
        page: Board.page,
        width: ui.size.width,
        height: ui.size.height,
        top: ui.position.top,
        left: ui.position.left
      });

      Board.updateElement(elid, 'position', {
        top: ui.position.top,
        left: ui.position.left
      });
      Board.updateElement(elid, 'size', {
        width: ui.size.width,
        height: ui.size.height
      });
      Board.updateElement(elid, 'z', zindex);

      Board.updateElement(elid, 'persistentState', 'update_required');
    },

    zoom: function (event, ui) {
      if (Board.editing ||Board.inserting)
        return;

      App.socket.emit('cm_zoom', {
        memo: Board.memo,
        page: Board.page,
        zoom: ui.scale,
        z: zindex
      });
    },

    mouseout: function () {
      if (Board.drawing == 1 ||Board.drawing == 2) {
        Board.disable = 2;
        Board.drawing = 0;
      }
    },

    mouseover: function () {
      if (Board.disable == 2) {
        Board.disable = 1;
        Board.drawing = 1;
      }
    },

    mousedown: function (e) {
      if (e.type == "touchstart" && e.target.nodeName != "SELECT") {
        e.preventDefault();
      }
      Board.disable = 1;
      Board.drawing = 1;
    },

    mouseup: function (e) {
      if (Board.drawMode != "polyline") {
        Board.linePoints = [];
      }

      switch (Board.drawMode) {
        case 'lineOut':
        case 'line':
        case 'rect':
        case 'elips':
        case 'poly':
          Board.drawing = 0;

          if (Board.drawing == 0 && (e.type == 'touchend' || e.type == 'ontouchend')) {
            var type, x, y, color, thick;

            x =Board.toCanvasX(e);
            y =Board.toCanvasY(e);
            Board.mouse.x = x;
            Board.mouse.y = y;

            if (Board.drawMode == 'stamp') {
              $('#stamp-cursor').css('left',Board.toPageX(e) + 'px').css('top',Board.toPageY(e) + 'px').css('z-index',Board.zIndexCounter);
            }

            if (Board.drawMode == 'image') {
              $('#image-cursor').css('left',Board.toPageX(e) + 'px').css('top',Board.toPageY(e) + 'px').css('z-index',Board.zIndexCounter);
            }

            type = 'dragend';


            if (Board.disable == 2 && (Board.drawing == 1 ||Board.drawing == 2)) {
              return;
            }

            if (Board.drawing == 1 ||Board.drawing == 2 ||Board.drawing == 0) {
              Board.draw(Board.currentCtxId, x, y, type, color, thick);
            }
          }

          Board.clear(Board.memCanvas);
          Board.memCanvas.getContext("2d").drawImage(Board.canvas, 0, 0);
          if (Board.drawMode == 'lineOut') {
            Board.donedrawing();
          }
          break;
        case 'image':
          var x, y;
          x =Board.toCanvasX(e);
          y =Board.toCanvasY(e);

          var newCtxId = (new Date).getTime();

          var element = {
            id: newCtxId,
            type: 'image',
            style: {
              lineSize: App.views.mymemo.linethickslider.value(),
              fill:Board.fill,
              stroke: App.views.mymemo.strokepallete.getKendoColorPicker().value()
            },
            size: {
              width: 200,
              height: 200
            },
            position: {
              top: y,
              left: x
            },
            angle: 0.0,
            data:Board.imageFile,
            persistentState: 'new'
          };

          Board.createObject(element);

          App.socket.emit('cm_new', element);
          Board.donedrawing();
          break;
        case 'stamp':
          var x, y;
          x =Board.toCanvasX(e);
          y =Board.toCanvasY(e);

          var newCtxId = (new Date).getTime();

          var element = {
            id: newCtxId,
            type: 'stamp',
            style: {
              lineSize: App.views.mymemo.linethickslider.value(),
              fill:Board.fill,
              stroke: App.views.mymemo.strokepallete.getKendoColorPicker().value()
            },
            size: {
              width: 200,
              height: 200
            },
            position: {
              top: y,
              left: x
            },
            angle: 0.0,
            data:Board.stampIcon,
            persistentState: 'new'
          };

          Board.createObject(element);

          App.socket.emit('cm_new', element);
          Board.donedrawing();
        break;
        case 'polyline':
          var x, y;
          x =Board.toCanvasX(e);
          y =Board.toCanvasY(e);

          if (Board.linePoints.length > 0) {
            if (!$("#isstroke").is(':checked')) {
              Board.clear(Board.canvas);
            }

            Board.linePoints.push({x: x, y: y});
            Board.ctx.lineTo(x, y);
            if ($("#isstroke").is(':checked'))
              Board.ctx.stroke();
            if ($("#isfill").is(':checked'))
              Board.ctx.fill();
          } else {
            Board.linePoints.push({x: x, y: y});

            Board.ctx.lineCap = "round";
            Board.ctx.strokeStyle =App.views.mymemo.strokepallete.getKendoColorPicker().value();
            Board.ctx.fillStyle =App.views.mymemo.fillpallete.getKendoColorPicker().value();
            Board.ctx.lineWidth =App.views.mymemo.linethickslider.value();

            Board.ctx.beginPath();
            Board.ctx.moveTo(x, y);
            Board.ctx.lineTo(x + 1, y + 1);
            Board.ctx.stroke();
          }
          break;
        case 'text':
          if (!Board.inserting && !Board.editing) {
            var x, y;
            x =Board.toPageX(e);
            y =Board.toPageY(e);

            var newCtxId = (new Date).getTime();

            var element = {
              id: newCtxId,
              type: 'text',
              style: {
                lineSize:App.views.mymemo.linethickslider.value(),
                fill:Board.fill,
                stroke:App.views.mymemo.strokepallete.getKendoColorPicker().value()
              },
              size: {
                width: 250,
                height: 150
              },
              position: {
                top: y,
                left: x
              },
              angle: 0.0,
              data: ' ',
              persistentState: 'new'
            };


            Board.inserting = true;
            Board.currentCtxId = newCtxId;

            Board.createObject(element);

            $('#text-' + newCtxId).removeClass('disabled');
            $('#canvasFakeContainer').removeClass('active');
          }
          break;
        case 'comic':
          if (!Board.inserting && !Board.editing) {
            var x, y;
            x = $(window).width() / 2;
            y = $(window).height() / 2;

            var newCtxId = (new Date).getTime();

            var element = {
              id: newCtxId,
              type: 'comic',
              style: {
                lineSize:App.views.mymemo.linethickslider.value(),
                fill:Board.fill,
                stroke:App.views.mymemo.strokepallete.getKendoColorPicker().value()
              },
              size: {
                width: 250,
                height: 150
              },
              position: {
                top: y,
                left: x
              },
              angle: 0.0,
              data: ' ',
              persistentState: 'new'
            };


            Board.inserting = true;
            Board.currentCtxId = newCtxId;

            Board.createObject(element);

            $('#text-' + newCtxId).removeClass('disabled');
            $('#canvasFakeContainer').removeClass('active');
          }
          break;
        case 'table':
          if (!Board.inserting && !Board.editing) {
            var x, y;
            x =Board.toPageX(e);
            y =Board.toPageY(e);

            var newCtxId = (new Date).getTime();


            var table = '<table border="1" width="100%" height="100%">';

            for (var i = 0; i < parseInt($('#tbrow').val()); i++) {
              table += '<tr>';
              for (var j = 0; j < parseInt($('#tbcol').val()); j++) {
                table += '<td></td>';
              }
              table += '</tr>';
            }

            table += '</table>';

            var element = {
              id: newCtxId,
              type: 'table',
              style: {
                lineSize:App.views.mymemo.linethickslider.value(),
                fill:Board.fill,
                stroke:App.views.mymemo.strokepallete.getKendoColorPicker().value()
              },
              size: {
                width: 250,
                height: 150
              },
              position: {
                top: y,
                left: x
              },
              angle: 0.0,
              data: table,
              persistentState: 'new'
            };


            Board.inserting = true;
            Board.currentCtxId = newCtxId;

            Board.createObject(element);
            $('#textbox-window').closest(".k-window").css({
              top: y,
              left: x
            });

            $('#text-' + newCtxId).removeClass('disabled');
            $('#canvasFake').removeClass('active');
          }
          break;
        default:
          break;
      }
    },

    mousemove: function (e) {

      var type, x, y, color, thick;

      x =Board.toCanvasX(e);
      y =Board.toCanvasY(e);
      Board.mouse.x = x;
      Board.mouse.y = y;

      if (Board.drawMode == 'stamp') {
        $('#stamp-cursor').css('left', x + 'px').css('top', y + 'px');
      }

      if (Board.drawMode == 'image') {
        $('#image-cursor').css('left', x + 'px').css('top', y + 'px');
      }

      type =Board.drawing == 1 ? 'dragstart' :Board.drawing == 2 ? 'drag' :Board.drawing == 0 ? 'dragend' : 'unknown';

      if (Board.drawing == 1) {
        Board.drawing = 2;
      }


      if (Board.disable == 2 && (Board.drawing == 1 ||Board.drawing == 2)) {
        return;
      }
      if (Board.drawing == 1 ||Board.drawing == 2 ||Board.drawing == 0) {
        Board.draw(Board.currentCtxId, x, y, type, color, thick);
      }
    },

    // keypress: function(e){
    // if(Board.inserting){
    //Board.ctx.textAlign=Board.textAlign;
    // var key = (e.which) ? e.which : e.keyCode;
    // if(key == 13){
    //Board.tempText.line =Board.tempText.line + 1;
    //Board.tempText.text[Board.tempText.line] = "";
    //Board.tempText.pos[Board.tempText.line] = {x:Board.tempText.x, y:Board.tempText.y+((App.views.mymemo.fontsizeslider.value()+5) *Board.tempText.line)};

    //Board.clear(Board.canvas);
    // if($("#isfill").is(':checked')){
    // for(var i = 0; i <Board.tempText.text.length; i++){
    //Board.ctx.moveTo(Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    //Board.ctx.fillText(Board.tempText.text[i],Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    // }
    // }
    // if($("#isstroke").is(':checked')){
    // for(var i = 0; i <Board.tempText.text.length; i++){
    //Board.ctx.moveTo(Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    //Board.ctx.strokeText(Board.tempText.text[i],Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    // }
    // }

    //Board.ctx.moveTo(Board.tempText.pos[Board.tempText.line].x,Board.tempText.pos[Board.tempText.line].y);
    //Board.ctx.fillText('|',Board.tempText.pos[Board.tempText.line].x,Board.tempText.pos[Board.tempText.line].y);
    // return;
    // } else if(key == 8){
    // e.preventDefault();
    //Board.tempText.text[Board.tempText.line] =Board.tempText.text[Board.tempText.line].substring(0,Board.tempText.text[Board.tempText.line].length - 1);
    // } else {
    //Board.tempText.text[Board.tempText.line] += String.fromCharCode(key);
    // }

    //Board.clear(Board.canvas);
    // if($("#isfill").is(':checked')){
    // for(var i = 0; i <Board.tempText.text.length; i++){
    //Board.ctx.moveTo(Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    // if(i ==Board.tempText.line){
    //Board.ctx.fillText(Board.tempText.text[i]+'|',Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    // } else {
    //Board.ctx.fillText(Board.tempText.text[i],Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    // }
    // }
    // }
    // if($("#isstroke").is(':checked')){
    //			for(var i = 0; i <Board.tempText.text.length; i++){
    //				this.ctx.moveTo(Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    //				if(i ==Board.tempText.line){
    //					this.ctx.strokeText(Board.tempText.text[i]+'|',Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    //				} else {
    //					this.ctx.strokeText(Board.tempText.text[i],Board.tempText.pos[i].x,Board.tempText.pos[i].y);
    //				}
    //			}
    //		}
    //	}
    //},

    enddraw: function (id) {
      if (!Board.ctx)
        return;
      Board.ctx.closePath();
    },

    draw: function (id, x, y, type, color, thick, cut) {
      Board.ctx.lineCap = "round";
      Board.ctx.strokeStyle =Board.stroke;
      Board.ctx.fillStyle =Board.fill;
      Board.ctx.lineWidth =Board.lineThick;

      switch (Board.drawMode) {
        case 'lineOut':
        case 'line':
          if (type === "drag" &&Board.disable == 1) {
            Board.ctx.beginPath();
            Board.disable = 0;
            Board.linePoints.push({x: x, y: y});
            return Board.ctx.moveTo(x, y);
          } else if (type === "dragstart" || type === "touchstart") {

            Board.clear(Board.canvas);

            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            Board.ctx.beginPath();
            Board.disable = 0;
            Board.linePoints.push({x: x, y: y});
            return Board.ctx.moveTo(x, y);
          } else if (type === "drag" &&Board.disable == 0) {
            Board.linePoints.push({x: x, y: y});

            Board.clear(Board.canvas);

            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            if (Board.linePoints.length < 3)
              return;

            if (Board.linePoints.length < 3) {
              var b =Board.linePoints[0];
              Board.ctx.beginPath();
              Board.ctx.arc(b.x, b.y,Board.ctx.lineWidth / 2, 0, Math.PI * 2, !0);
              Board.ctx.closePath();
              Board.ctx.fill();
              return
            }

            Board.ctx.beginPath();
            Board.ctx.moveTo(Board.linePoints[0].x,Board.linePoints[0].y);
            for (i = 1; i <Board.linePoints.length - 2; i++) {
              var c = (Board.linePoints[i].x +Board.linePoints[i + 1].x) / 2,
                d = (Board.linePoints[i].y +Board.linePoints[i + 1].y) / 2;
              Board.ctx.quadraticCurveTo(Board.linePoints[i].x,Board.linePoints[i].y, c, d)
            }

            Board.ctx.quadraticCurveTo(Board.linePoints[i].x,Board.linePoints[i].y,Board.linePoints[i + 1].x,Board.linePoints[i + 1].y);
            Board.ctx.stroke();
          } else {
          }
          break;
        case 'rect':
          if (type === "drag" &&Board.disable == 1) {
            Board.tempRect.w = x -Board.tempRect.startX;
            Board.tempRect.h = y -Board.tempRect.startY;
            Board.disable = 0;
          } else if (type === "dragstart" || type === "touchstart") {
            Board.clear(Board.canvas);

            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            Board.tempRect.startX = x;
            Board.tempRect.startY = y;
            Board.ctx.beginPath();
          } else if (type === "drag" &&Board.disable == 0) {
            Board.tempRect.w = x -Board.tempRect.startX;
            Board.tempRect.h = y -Board.tempRect.startY;

            Board.clear(Board.canvas);
            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            if ($("#isstroke").is(':checked'))
              Board.ctx.strokeRect(Board.tempRect.startX,Board.tempRect.startY,Board.tempRect.w,Board.tempRect.h);

            if ($("#isfill").is(':checked'))
              Board.ctx.fillRect(Board.tempRect.startX,Board.tempRect.startY,Board.tempRect.w,Board.tempRect.h);
          } else {
            if (Board.tempRect.w) {
              Board.tempRect.w = x -Board.tempRect.startX;
              Board.tempRect.h = y -Board.tempRect.startY;

              Board.clear(Board.canvas);
              Board.ctx.drawImage(Board.memCanvas, 0, 0);

              if ($("#isstroke").is(':checked'))
                Board.ctx.strokeRect(Board.tempRect.startX,Board.tempRect.startY,Board.tempRect.w,Board.tempRect.h);

              if ($("#isfill").is(':checked'))
                Board.ctx.fillRect(Board.tempRect.startX,Board.tempRect.startY,Board.tempRect.w,Board.tempRect.h);

              Board.tempRect = {};
              Board.donedrawing();
            }
          }
          break;
        case 'elips':
          if (type === "drag" &&Board.disable == 1) {
            Board.tempElips.w = x -Board.tempElips.startX;
            Board.tempElips.h = y -Board.tempElips.startY;
            Board.disable = 0;
          } else if (type === "dragstart" || type === "touchstart") {
            Board.clear(Board.canvas);

            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            Board.tempElips.startX = x;
            Board.tempElips.startY = y;
          } else if (type === "drag" &&Board.disable == 0) {

            Board.tempElips.w = x -Board.tempElips.startX;
            Board.tempElips.h = y -Board.tempElips.startY;

            var xs = 0;
            var ys = 0;

            xs = Math.abs(x -Board.tempElips.startX);
            xs = xs * xs;

            ys = Math.abs(y -Board.tempElips.startY);
            ys = ys * ys;

            Board.clear(Board.canvas);
            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            Board.ctx.beginPath();
            Board.ctx.arc(Board.tempElips.startX,Board.tempElips.startY, Math.sqrt(xs + ys), 0, 2 * Math.PI, false);

            if ($("#isfill").is(':checked'))
              Board.ctx.fill();

            if ($("#isstroke").is(':checked'))
              Board.ctx.stroke();
          } else {
            if (Board.tempElips.w) {
              Board.tempElips.w = x -Board.tempElips.startX;
              Board.tempElips.h = y -Board.tempElips.startY;

              var xs = 0;
              var ys = 0;

              xs = Math.abs(x -Board.tempElips.startX);
              xs = xs * xs;

              ys = Math.abs(y -Board.tempElips.startY);
              ys = ys * ys;

              Board.clear(Board.canvas);

              Board.ctx.beginPath();
              Board.ctx.arc(Board.tempElips.startX,Board.tempElips.startY, Math.sqrt(xs + ys), 0, 2 * Math.PI, false);

              if ($("#isfill").is(':checked'))
                Board.ctx.fill();

              if ($("#isstroke").is(':checked'))
                Board.ctx.stroke();

              Board.donedrawing();
              Board.tempElips = {};
            }
          }
          break;
        case 'poly':
          if (type === "drag" &&Board.disable == 1) {
            Board.tempPoly.w = x -Board.tempPoly.startX;
            Board.tempPoly.h = y -Board.tempPoly.startY;
            Board.disable = 0;
          } else if (type === "dragstart" || type === "touchstart") {
            Board.clear(Board.canvas);

            Board.ctx.drawImage(Board.memCanvas, 0, 0);

            Board.tempPoly.startX = x;
            Board.tempPoly.startY = y;
          } else if (type === "drag" &&Board.disable == 0) {
            Board.tempPoly.w = x -Board.tempPoly.startX;
            Board.tempPoly.h = y -Board.tempPoly.startY;

            var xs = 0;
            var ys = 0;

            xs = Math.abs(x -Board.tempPoly.startX);
            xs = xs * xs;

            ys = Math.abs(y -Board.tempPoly.startY);
            ys = ys * ys;
            var size = Math.sqrt(xs + ys);

            Board.clear(Board.canvas);
            //this.ctx.drawImage(Board.memCanvas, 0, 0);
            Board.ctx.beginPath();
            Board.ctx.moveTo(Board.tempPoly.startX + size * Math.cos(0),Board.tempPoly.startY + size * Math.sin(0));

            for (var i = 1; i <= $('#polypointslider').val(); i += 1) {
              Board.ctx.lineTo(Board.tempPoly.startX + size * Math.cos(i * 2 * Math.PI / $('#polypointslider').val()),Board.tempPoly.startY + size * Math.sin(i * 2 * Math.PI / $('#polypointslider').val()));
            }

            if ($("#isstroke").is(':checked'))
              Board.ctx.stroke();

            if ($("#isfill").is(':checked'))
              Board.ctx.fill();
          } else {
            if (Board.tempPoly.w) {
              Board.tempPoly.w = x -Board.tempPoly.startX;
              Board.tempPoly.h = y -Board.tempPoly.startY;

              var xs = 0;
              var ys = 0;

              xs = Math.abs(x -Board.tempPoly.startX);
              xs = xs * xs;

              ys = Math.abs(y -Board.tempPoly.startY);
              ys = ys * ys;
              var size = Math.sqrt(xs + ys);

              Board.clear(Board.canvas);

              Board.ctx.beginPath();
              Board.ctx.moveTo(Board.tempPoly.startX + size * Math.cos(0),Board.tempPoly.startY + size * Math.sin(0));

              for (var i = 1; i <= $('#polypointslider').val(); i += 1) {
                Board.ctx.lineTo(Board.tempPoly.startX + size * Math.cos(i * 2 * Math.PI / $('#polypointslider').val()),Board.tempPoly.startY + size * Math.sin(i * 2 * Math.PI / $('#polypointslider').val()));
              }

              if ($("#isstroke").is(':checked'))
                Board.ctx.stroke();

              if ($("#isfill").is(':checked'))
                Board.ctx.fill();
              Board.donedrawing();
              Board.tempPoly = {};
            }
          }
          break;
        default:
          break;
      }
    },

    createStamp: function (stamp) {
      Board.drawMode = 'stamp';

      $('#stamp-cursor').removeClass('active');
      $('#stamp-cursor').addClass('active');

      Board.stampIcon = stamp;

      Board.drawcanvas('stamp');
    },

    createChart: function () {
      if (!Board.tempChart.data) {
        alert('Chart is empty');
        return;
      }

      var x, y;
      x = 0;
      y = 0;

      var newCtxId = (new Date).getTime();

      var element = {
        id: newCtxId,
        type: 'chart',
        style: {
          lineSize:App.views.mymemo.linethickslider.value(),
          fill:Board.fill,
          stroke:App.views.mymemo.strokepallete.getKendoColorPicker().value()
        },
        size: {
          width: $('#chart-width').val(),
          height: $('#chart-height').val()
        },
        position: {
          top: y,
          left: x
        },
        data:Board.tempChart,
        persistentState: 'new'
      };

      Board.createObject(element);

      App.socket.emit('cm_new', element);

      $('#chart-window').data("kendoWindow").close();
    },

    layerOrder: function (id, order) {
      Board.ordering = {id: id, order: order};

      Board.drawcanvas('ordering');
    },

    placeImage: function (image) {
      Board.drawMode = 'image';

      Board.drawcanvas('image');
    },

    drawcanvas: function (drawMode) {
      console.log('Drawing ' + drawMode);
      Board.drawMode = drawMode;


      $('#canvasFakeContainer').removeClass('active').addClass('active');

      if (drawMode == "text" || drawMode === 'comic') {
        $('#canvasFake').css('cursor', 'text');
      } else if (drawMode == 'ordering') {
        $('#canvasFake').css('cursor', 'pointer');
      } else {
        $('#canvasFake').css('cursor', 'crosshair');
      }

      if (drawMode == 'table') {
        $("#table-window").data("kendoWindow").close();
      }

      $('#canvasContainer > div').removeClass('active');
      $('#canvasFakeContainer').css('z-index',Board.zIndexCounter + 2);
      $('.canvascontrol').css('z-index',Board.zIndexCounter + 3);
      $('.canvascontrol').addClass('active');
    },

    editTextArea: function (id) {
      $('#sticky-update').openModal();

      if (!Board.inserting || !Board.editing) {
        Board.editing = true;
        Board.editElementId = id;

        $('#text-' + id).removeClass('disabled');
        if($('#text-' + id).parent().attr('data-type') === "comic"){
          $('#edit-id').val(id);
          var editor = $("#edit-sticky-text").data("kendoEditor");
          editor.value($('#text-' + id + ' .card-content').html());
          $('#edit-sticky-bg-'+$('#text-' + id).attr('data-bg')).prop("checked", true);
        }
      }
    },

    donedrawing: function () {
      $('#canvasFakeContainer').removeClass('active');
      $('.canvascontrol').removeClass('active');
      $('#stamp-cursor').removeClass('active');

      if (Board.inserting) {
        var newCtxId = Board.currentCtxId;

        if($('#text-' + newCtxId).parent().attr('data-type') === 'comic'){
          var htmldata = $('#text-' + newCtxId + ' .card-content').html();
        } else {
          var htmldata = $('#text-' + newCtxId).html();
        }

        $('#text-' + newCtxId).addClass('disabled');

        var element = {
          id: newCtxId,
          type: 'text',
          style: {
            lineSize: parseInt($('#' + newCtxId).css('-webkit-text-stroke-width')),
            fill: $('#' + newCtxId).css('color'),
            stroke: $('#' + newCtxId).css('-webkit-text-stroke')
          },
          size: {
            width: $('#' + newCtxId).width(),
            height: $('#' + newCtxId).height()
          },
          position: {
            top: parseInt($('#' + newCtxId).css('top')),
            left: parseInt($('#' + newCtxId).css('left'))
          },
          angle: 0.0,
          data: escape(htmldata),
          persistentState: 'new'
        };

        App.socket.emit('cm_new', element);

        Board.tempText = {};
        Board.inserting = false;
      } else if (Board.editing) {
        var newCtxId =Board.editElementId;
        if($('#text-' + newCtxId).parent().attr('data-type') === 'comic'){
          var htmldata = $('#text-' + newCtxId + ' .card-content').html();
        } else {
          var htmldata = $('#text-' + newCtxId).html();
        }

        $('#text-' + newCtxId).addClass('disabled');

        $('#textbox-window').data("kendoWindow").center().close();

        Board.updateElement(newCtxId, 'style', {
          font: $('#fonttypeselect option:selected').val(),
          fontsize: parseInt($('#' + newCtxId).css('font-size')),
          textAlign: $('#' + newCtxId).css('text-align') == 'start' ? 'left' : $('#' + newCtxId).css('text-align'),
          strokesize: $('#' + newCtxId).css('-webkit-text-stroke-width') ? $('#' + newCtxId).css('-webkit-text-stroke-width') : 1,
          fill: $('#' + newCtxId).css('color'),
          stroke: $('#' + newCtxId).css('text-shadow')
        });

        Board.updateElement(newCtxId, 'position', {
          top: parseInt($('#' + newCtxId).css('top')),
          left: parseInt($('#' + newCtxId).css('left'))
        });

        Board.updateElement(newCtxId, 'size', {
          width: parseInt($('#' + newCtxId).css('width')),
          height: parseInt($('#' + newCtxId).css('height'))
        });

        Board.updateElement(newCtxId, 'data', escape(htmldata));

        Board.updateElement(newCtxId, 'persistentState', 'update_required');

        var updatedElement =Board.getElement(newCtxId);
        Board.elementById[newCtxId] = updatedElement;

        App.socket.emit('cm_update', updatedElement);

        Board.editing = false;
        Board.editElementId = 0;
      } else {
        if (Board.drawmode == 'stamp') {
          $('#stamp-cursor').removeClass('active');
        } else if (Board.drawmode == 'image') {
          $('#image-cursor').removeClass('active');
        } else {
          Board.cloneCanvas();
        }
      }

      Board.saveCanvas();

    },

    createPhoto: function(url, width, height){
        var newCtxId = (new Date).getTime();

        var element = {
          id: newCtxId,
          type: 'img',
          style: {
            lineSize: '',
            fill: '',
            stroke: ''
          },
          size: {
            width: width,
            height: height
          },
          position: {
            top: 100,
            left: 100
          },
          data: url,
          persistentState: 'new'
        };
        Board.createObject(element, false, function(){
          Board.saveCanvas();
        });

        App.socket.emit('cm_new', element);
    },

    createSticky: function(data, bg, tag){
      var newCtxId = (new Date).getTime();

      var element = {
        id: newCtxId,
        type: 'comic',
        style: {
          lineSize: '',
          fill: bg,
          stroke: ''
        },
        size: {
          width: 200,
          height: 200
        },
        position: {
          top: 100,
          left: 100
        },
        data: data,
        persistentState: 'new'
      };
      Board.createObject(element);
      App.socket.emit('cm_new', element);
      Board.saveCanvas();
    },

    updateSticky: function(id, data, bg, tag){
      var newCtxId = id;

      Board.updateElement(newCtxId, 'data', escape(data));
      Board.updateElement(newCtxId, 'persistentState', 'update_required');
      Board.updateElement(newCtxId, 'style', {
        lineSize: '',
        fill: bg,
        stroke: ''
      });

      Board.saveCanvas();

      var sticky = $('#' + id + ' .text-edit');
      sticky.removeClass('lime purple red blue green pink cyan yellow orange brown grey');
      sticky.addClass(bg).attr('data-bg', bg);

      $('#text-' + id + ' .card-content').html(data);
      var element = Board.elementById[newCtxId];

      App.socket.emit('cm_update', element);
      Board.editing = false;
      Board.editElementId = 0;
    },

    cloneCanvas: function () {
      var ww =Board.canvas.width;
      var wh =Board.canvas.height;
      var newCtxId = (new Date).getTime();

      var imageData =Board.ctx.getImageData(0, 0, ww, wh);
      var topLeftCorner = {};
      topLeftCorner.x = 9999;
      topLeftCorner.y = 9999;

      /* WE INICIALIZE BOTTOM LEFT CORNER OUTSIDE THE CANVAS
       BELOW WE'LL SEE WHY :) */
      var bottomRightCorner = {};
      bottomRightCorner.x = -1;
      bottomRightCorner.y = -1;
      var nothing = true;

      /* NOW WE RUN TROUGHT ALL THE IMAGES'S PIXELS CHECKING IF THERE IS SOTHING ON THEM */
      for (var y = 0; y < wh; y++) {
        for (var x = 0; x < ww; x++) {

          var pixelPosition = ww * y + x;

          /* EACH PIXEL HAS 4 "BYTES" OF DATA CORRESPONDING TO red, green, blue AND alpha*/
          //r = imageData.data[pixelPosition]; //red
          //g = imageData.data[pixelPosition+1]; //green
          //b = imageData.data[pixelPosition+2]; //blue

          /* I'M ONLY INTERESTED IN ALPHA COMPONENT, IF SOMETHING IS PRESENT IN THAT PIXEL ALPHA (a) VALUE MUST BE > 0*/
          var a = imageData.data[pixelPosition * 4 + 3]; //alpha

          /* I IGNORE THE r,g,b COMPONENT, ONLY CHECK IF alpha > 0 */
          if (a > 0) {

            /* HERE I GET THE TOP MOST LEFT PIXEL, AND THE BOTTOM MOST RIGHT ONE */
            if (x < topLeftCorner.x) {
              topLeftCorner.x = x;
            }
            if (y < topLeftCorner.y) {
              topLeftCorner.y = y;
            }
            if (x > bottomRightCorner.x) {
              bottomRightCorner.x = x;
            }
            if (y > bottomRightCorner.y) {
              bottomRightCorner.y = y;
            }
            nothing = false;
          }
        }
      }

      /* HERE WE HAVE THE COORDINATES OF THE RECTANGLE CONTAINING SOMETHING DROWN */
      if (!nothing) {
        /* NOW WE SAVE THE REGION WE WANT TO TRIM TO  A VARIABLE */
        /* (x, y, width, heigth) */
        var relevantData =Board.ctx.getImageData(topLeftCorner.x, topLeftCorner.y, bottomRightCorner.x - topLeftCorner.x, bottomRightCorner.y - topLeftCorner.y);

        var realSize = {};
        realSize.w = bottomRightCorner.x - topLeftCorner.x;
        realSize.h = bottomRightCorner.y - topLeftCorner.y;

        var tempCanvas = document.createElement('canvas');
        tempCanvas.width = realSize.w;
        tempCanvas.height = realSize.h;
        tempCanvas.getContext("2d").putImageData(relevantData, 0, 0);

        var element = {
          id: newCtxId,
          type: 'img',
          style: {
            lineSize:App.views.mymemo.linethickslider.value(),
            fill:App.views.mymemo.fillpallete.getKendoColorPicker().value(),
            stroke:App.views.mymemo.strokepallete.getKendoColorPicker().value()
          },
          size: {
            width: realSize.w,
            height: realSize.h
          },
          position: {
            top: topLeftCorner.y - Board.drawContainer.offset().top + $('.drawContainer').offset().top,
            left: topLeftCorner.x - Board.drawContainer.offset().left
          },
          data: '',
          persistentState: 'new'
        };

        var imageData = tempCanvas.toDataURL("image/png");

        $('#loading').show();
        $.ajax({
          method: "POST",
          url: "/api/upload/element",
          data : {image: imageData},
          dataType: 'json',
          success: function(res){
            if(res.success){
              element.data = res.data;
            } else {
              element.data = imageData;
            }

            Board.createObject(element);

            App.socket.emit('cm_new', element);
            $('#loading').hide();
          },
          errro: function(){
            $('#loading').hide();
          }
        });

        Board.currentCtxId = newCtxId;
      } else {
        console.log('Canvas is empty');
      }

      Board.clear(Board.canvas);
      Board.clear(Board.memCanvas);
      Board.linePoints = [];
    },

    canceldrawing: function () {
      $('#canvasFakeContainer').removeClass('active');
      $('.canvascontrol').removeClass('active');
      Board.linePoints = [];
      Board.tempText = {};
      Board.clear(Board.canvas);
      Board.clear(Board.memCanvas);

      if (Board.inserting) {
        console.log('canceling text');
        $('#' + (Board.currentCtxId)).remove();
      }

      Board.inserting = false;

      if (Board.editing) {
        Board.donedrawing();
      }

      if (Board.drawmode == 'stamp') {
        $('#stamp-cursor').removeClass('active');
      }
    },

    clear: function (canvas) {
      if (canvas && canvas.getContext("2d")) {
        canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
      }
    },

    changeGrid: function () {
      var gridsize = parseInt($('#grid-size').val());
      if (gridsize > 5) {
        $('#canvasContainer > div[class^="obj-"]').draggable("option", "grid", [gridsize, gridsize]);

        $.each($('#canvasContainer > div[class^="obj-"]'), function () {
          var top = $(this).position().top;
          var left = $(this).position().left;
          $(this).css('top', Math.floor(top / gridsize) * gridsize);
          $(this).css('left', Math.floor(left / gridsize) * gridsize);
        });
      } else {
        $('#canvasContainer > div[class^="obj-"]').draggable("option", "grid", false);
      }

      $('#canvasContainer').attr('class', '');
      $('#canvasContainer').addClass('grid-' + gridsize);
    },

    setLineThick: function (e) {
      Board.lineThick = e.value;
      var CtxId = 0;

      if (Board.inserting) {
        CtxId =Board.currentCtxId;
      } else if (Board.editing) {
        CtxId =Board.editElementId;
      } else {
      }

      if ($('#isstroke').is(':checked') && (Board.inserting ||Board.editing)) {
        $('#' + CtxId).css('filter', 'dropshadow(color=' +Board.stroke + ', offx=0, offy=0)').css('text-shadow', '0px 0px ' +Board.lineThick + 'px ' +Board.stroke).css('-webkit-text-stroke',Board.stroke).css('-webkit-text-stroke-width',Board.lineThick + 'px');
      }
    },

    setStroke: function (e) {
      Board.stroke = e.value;
      var CtxId = 0;

      if (Board.inserting) {
        CtxId =Board.currentCtxId;
      } else if (Board.editing) {
        CtxId =Board.editElementId;
      } else {
      }

      if ($('#isstroke').is(':checked') && (Board.inserting ||Board.editing)) {
        $('#' + CtxId).css('filter', 'dropshadow(color=' +Board.stroke + ', offx=0, offy=0)').css('text-shadow', '0px 0px ' +Board.lineThick + 'px ' +Board.stroke).css('-webkit-text-stroke',Board.stroke).css('-webkit-text-stroke-width',Board.lineThick + 'px');
      }
    },

    setFill: function (e) {
      Board.fill = e.value;

      var CtxId = 0;

      if (Board.inserting) {
        CtxId =Board.currentCtxId;
      } else if (Board.editing) {
        CtxId =Board.editElementId;
      } else {
      }

      if ($('#isfill').is(':checked') && (Board.inserting ||Board.editing)) {
        $('#' + CtxId).css('color',Board.fill);
      } else {
        $('#' + CtxId).css('color', 'transparent');
      }
    },

    changeStampSize: function () {
      $('#stamp-cursor').css('font-size', $('#stamp-size').val() + 'px');
    },

    copyElement: function (e) {
      if (confirm('Copy selected object?')) {
        var newCtxId = (new Date).getTime();
        var cloneObject = $('#canvasContainer > div.active').clone();

        var cloneElement =Board.elementById[cloneObject.attr('id')];
        cloneElement.id = newCtxId;
        cloneElement.persistentState = 'new';

        Board.createObject(cloneElement);

        App.socket.emit('cm_new', cloneElement);

        Board.saveCanvas();
      } else {

      }
      e.preventDefault();
    },

    deleteElement: function (e) {
      if (confirm('Delete selected object?')) {
        var deleteObject = $('#canvasContainer > div.active');
        Board.removeElement(deleteObject.attr('id'));
        deleteObject.find('div').remove();
        deleteObject.remove();
        App.socket.emit('cm_delete', {
          id: deleteObject.attr('id'),
          memo:Board.memo,
          page:Board.page
        });
        Board.saveCanvas();
      } else {

      }
      e.preventDefault();
    },

    undo: function () {
      console.log('undo');
      if (Board.element[Board.element.length - 1]) {
        Board.deletedElement.push(Board.element[Board.element.length - 1]);
        $('#' +Board.currentCtxId + ' div').remove();
        $('#' +Board.currentCtxId).remove();
        Board.currentCtxId =Board.element[Board.element.length - 2].id;
        Board.element.pop();
        Board.elementById.splice(Board.elementById.indexOf(Board.currentCtxId), 1);
      }
    },

    redo: function () {
      if (Board.deletedElement[Board.deletedElement.length - 1]) {
        var element =Board.deletedElement[Board.deletedElement.length - 1];
        var newCtxId = (new Date).getTime();
        element.id = newCtxId;
        Board.createObject(element);

        Board.deletedElement.pop();
      }
    }
  };
});