'use strict';
var App;
var Board;
var kendo;
var Vel;
var Hammer = {};
var Materialize = {};

requirejs.config({
  "baseUrl": "/js",
  "paths": {
    jquery: "vendor/jquery/jquery-2.1.3.min",
    ui: "vendor/jquery-ui/jquery-ui.min",
    ro: "vendor/jquery-ui/jquery-ui.rotatable.min",
    dt: "vendor/jquery/jquery.dbltap",
    touchbox: "plugins/jquery.touch_box.min",
    touchpunch: "plugins/jquery.ui.touch-punch.min",
    velocity: "vendor/materialize/bin/velocity.min",
    hammer: "vendor/materialize/bin/hammer.min",
    hammerjq: "vendor/materialize/bin/jquery.hammer",
    materialize: "vendor/materialize/bin/main",
    contextMenu: "plugins/jquery.contextMenu",
    panzoom: "plugins/jquery.panzoom.min",
    kendo: "plugins/kendo.web.min"
  },
  waitSeconds: 30,
  "shim": {
    jquery: {
      exports: "jquery",
      init: function(){
        console.log('JQuery inited..');
      }
    },
    ui: ["jquery"],
    ro: ["jquery", "ui"],
    dt: ["jquery"],
    touchbox: ["jquery"],
    velocity: {
      deps: ["jquery"],
      init: function(){
        Vel = $.Velocity;
      }
    },
    hammerjq: ["jquery"],
    materialize: ["jquery", "hammerjq", "hammer", "velocity"],
    touchpunch: ["ui"],
    contextMenu: ["jquery"],
    panzoom: ["jquery"],
    kendo: {
      deps: ["jquery"],
      exports: "kendo"
    }

  },
  nodeRequire: require
});

define([
  "app",
  "kendo",
  "velocity",
  "hammer",
  "materialize",
], function(Application, Kendo, v, h, m) {
  kendo = Kendo;
  App = Application;
  Hammer = h;
  Materialize = m;
  var deviceAgent = navigator.userAgent.toLowerCase();
  var agentID = deviceAgent.match(/(iphone|ipod)/);
  if (agentID) {
    $("#body").css("min-height", "418px");
    setTimeout(function () {
      try { window.scrollTo(0, 1); } catch (e) { }
    }, 0);
  }
  App.init();
});