var nativeSplit = nativeSplit || String.prototype.split;
String.prototype.split = function (j, d) {
  if (!(j instanceof RegExp)) {
    return nativeSplit.apply(this, arguments)
  }
  if (d === undefined || +d < 0) {
    d = false
  } else {
    d = Math.floor(+d);
    if (!d) {
      return []
    }
  }
  var c = (j.global ? "g" : "") + (j.ignoreCase ? "i" : "") + (j.multiline ? "m" : ""), g = new RegExp("^" + j.source + "$", c), b = [], a = 0, e = 0, f;
  if (!j.global) {
    j = new RegExp(j.source, "g" + c)
  }
  while ((!d || e++ <= d) && (f = j.exec(this))) {
    var h = !f[0].length;
    if (h && j.lastIndex > f.index) {
      j.lastIndex = f.index
    }
    if (j.lastIndex > a) {
      if (f.length > 1) {
        f[0].replace(g, function () {
          for (var i = 1; i < arguments.length - 2; i++) {
            if (arguments[i] === undefined) {
              f[i] = undefined
            }
          }
        })
      }
      b = b.concat(this.slice(a, f.index), (f.index === this.length ? [] : f.slice(1)));
      a = j.lastIndex
    }
    if (h) {
      j.lastIndex++
    }
  }
  return (a === this.length) ? (j.test("") ? b : b.concat("")) : (d ? b : b.concat(this.slice(a)))
};
Element.addMethods({
  removeAllChildren: function (a) {
    while (a.lastChild) {
      a.removeChild(a.lastChild)
    }
  }, update2: function (a, b) {
    if (b.nodeType) {
      a.removeAllChildren();
      a.appendChild(b)
    } else {
      a.innerHTML = b
    }
  }, getTransform: function (b) {
    for (name in Element.TRANSFORM_NAMES) {
      var a = b.getStyle(new String(Element.TRANSFORM_NAMES[name]));
      if (a) {
        return a
      }
    }
    return undefined
  }, setTransform: function (b, a) {
    var c = {};
    Element.TRANSFORM_NAMES.each(function (d) {
      c[d] = a
    });
    b.setStyle(c)
  }, getRotateAngle: function (b) {
    var a = b.getTransform();
    if (a) {
      if (a.match(/rotate\((-?\d+(?:.\d*)?)deg\)/)) {
        return parseFloat(RegExp.$1)
      }
    }
    return 0
  }, setRotateAngle: function (a, b) {
    a.setTransform("rotate(" + b + "deg)")
  }, $$: function (a, b) {
    var c = a.select("." + b);
    return c.length != 0 ? c[0] : null
  }, $$$: function (a, c) {
    var b = a.select(c);
    return b.length != 0 ? b[0] : null
  }, getBounds: function (b) {
    var c = b.cumulativeOffset();
    var a = b.getDimensions();
    return new Rectangle(c.left, c.top, a.width, a.height)
  }
});
Element.TRANSFORM_NAMES = ["transform", "WebkitTransform", "MozTransform", "MsTransform", "-ms-transform", "OTransform"];
Object.extend(Event, {
  wheel: function (a) {
    var b = 0;
    if (!a) {
      a = window.event
    }
    if (a.wheelDelta) {
      b = a.wheelDelta / 120;
      if (window.opera) {
        b = -b
      }
    } else {
      if (a.detail) {
        b = -a.detail / 3
      }
    }
    return Math.round(b)
  }
});
var g_today = getToday();
function parseDateTime(a) {
  var b = a.match("([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})");
  return new Date(Date.UTC(b[1], parseInt(b[2], 10) - 1, b[3], b[4], b[5], b[6]))
}
function parseDate(a) {
  var b = a.match("([0-9]{4})-([0-9]{2})-([0-9]{2})");
  return new Date(b[1], parseInt(b[2], 10) - 1, b[3])
}
function formatDate(a) {
  return a.getFullYear() + "-" + show2(a.getMonth() + 1) + "-" + show2(a.getDate())
}
function show2(a) {
  return ("0" + a).slice(-2)
}
function isSameDay(a, b) {
  if (a == null || b == null) {
    return false
  } else {
    return a.getDate() == b.getDate() && a.getMonth() == b.getMonth() && a.getFullYear() == b.getFullYear()
  }
}
function getTodayDate() {
  return g_today
}
function getToday() {
  var a = new Date();
  return new Date(a.getFullYear(), a.getMonth(), a.getDate())
}
function formatDueDate(b) {
  var a = getTodayDate();
  var c = null;
  if (isSameDay(b, a)) {
    c = "today"
  } else {
    if (isSameDay(b, new Date(a.getTime() + 24 * 60 * 60 * 1000))) {
      c = "tomorrow"
    } else {
      if (isSameDay(b, new Date(a.getTime() - 24 * 60 * 60 * 1000))) {
        c = "yesterday"
      }
    }
  }
  if (c) {
    return new Template(MESSAGES["due." + c]).evaluate({day: formatDay(b.getDay())})
  } else {
    return formatMonthDateDay(b.getMonth(), b.getDate(), b.getDay())
  }
}
function getCookie(b) {
  var d = document.cookie.split(";");
  for (var f = 0; f < d.length; ++f) {
    var e = d[f];
    var a = e.indexOf("=");
    if (a != -1) {
      if (e.substring(0, a).strip() == b) {
        return decodeURIComponent(e.substring(a + 1).strip())
      }
    }
  }
  return null
}
function setCookie(d, e, a) {
  var b = null;
  if (a) {
    var c = new Date();
    c.setTime(c.getTime() + a * 24 * 60 * 60 * 1000);
    b = c.toGMTString()
  } else {
    b = "Tue, 1-Jan-2030 00:00:00 GMT"
  }
  document.cookie = d + "=" + encodeURIComponent(e) + ";expires=" + b + ";path=/"
}
function deleteCookie(a) {
  document.cookie = a + "=;expires=Tue, 1-Jan-1990 00:00:00 GMT;path=/"
}
AjaxUtil = Class.create({
  initialize: function () {
  }, get: function (d, c, b, a) {
    this.request(d, "GET", c, b, a)
  }, post: function (d, c, b, a) {
    this.request(d, "POST", c, b, a)
  }, request: function (d, e, c, b, a) {
    this.requestParams(d, e, c ? Form.serialize(c, true) : {}, b, a)
  }, requestParams: function (c, e, d, b, a) {
    if (!a) {
      a = typeof error != "undefined" && error ? error : alert
    }
    new Ajax.Request(c, {
      method: e, parameters: d || {}, evalJSON: false, onSuccess: function (f) {
        if (f.getResponseHeader("Content-Type").match(/^application\/json/)) {
          if (f.responseText.length != 0) {
            try {
              var h = this.evalJSON(f.responseText);
              if (b) {
                b(h)
              }
            } catch (g) {
              a([g.toString()])
            }
          }
        } else {
          b(f.responseText)
        }
      }.bind(this), onFailure: function (f) {
        if (f.getResponseHeader("Content-Type").match(/^application\/json/)) {
          a(this.evalJSON(f.responseText))
        } else {
          if (f.getResponseHeader("Content-Type").match(/^text\/plain/)) {
            a([f.responseText])
          } else {
            a([f.status + " " + f.statusText])
          }
        }
      }.bind(this)
    })
  }, evalJSON: function (a) {
    if (Prototype.Browser.WebKit && a.startsWith("/*-secure-")) {
      return a.slice(11, -3).evalJSON(false)
    } else {
      return a.evalJSON()
    }
  }
});
var Point = Class.create({
  initialize: function (a, b) {
    this.x = a;
    this.y = b
  }, clone: function () {
    return new Point(this.x, this.y)
  }
});
var Line = Class.create({
  initialize: function (b, a) {
    this.p1 = b;
    this.p2 = a
  }
});
function ccw(g, e, c) {
  var f = e.x - g.x;
  var b = e.y - g.y;
  var d = c.x - g.x;
  var a = c.y - g.y;
  if (f * a > b * d) {
    return 1
  } else {
    if (f * a < b * d) {
      return -1
    } else {
      if (f * d < 0 || b * a < 0) {
        return -1
      } else {
        if (f * f + b * b < d * d + a * a) {
          return 1
        } else {
          return 0
        }
      }
    }
  }
}
function intersect(b, a) {
  return ccw(b.p1, b.p2, a.p1) * ccw(b.p1, b.p2, a.p2) <= 0 && ccw(a.p1, a.p2, b.p1) * ccw(a.p1, a.p2, b.p2) <= 0
}
function inside(g, e) {
  var h = e.length;
  e[-1] = e[h - 1];
  e[h] = e[0];
  var a = new Line(g.clone(), g.clone());
  a.p2.x = 10000;
  var f = 0;
  var b = -1;
  for (var d = 0; d < h; ++d) {
    var c = new Line(e[d], e[d]);
    if (!intersect(c, a)) {
      if (d == b + 1) {
        c.p2 = e[b];
        if (intersect(c, a)) {
          ++f
        }
      } else {
        if (ccw(a.p1, a.p2, e[d]) * ccw(a.p1, a.p2, e[b]) < 0) {
          ++f
        }
      }
      b = d
    }
  }
  return (f & 1) != 0
}
var Size = Class.create({
  initialize: function (a, b) {
    this.width = a;
    this.height = b
  }
});
var Rectangle = Class.create({
  initialize: function (b, c, a, d) {
    this.left = b;
    this.top = c;
    this.width = a;
    this.height = d
  }, equals: function (a) {
    return this.left == a.left && this.top == a.top && this.width == a.width && this.height == a.height
  }, clone: function () {
    return new Rectangle(this.left, this.top, this.width, this.height)
  }
});
function showErrorDialog(a) {
  alert(getErrorString(a))
}
function getErrorString(a) {
  if (a == null || a.length == 0) {
    return MESSAGES["error.unspecified"]
  } else {
    return MESSAGES[a[0]] || a[0]
  }
}
function getWindowWidth() {
  return Prototype.Browser.IE ? document.documentElement.clientWidth : window.innerWidth
}
function getWindowHeight() {
  return Prototype.Browser.IE ? document.documentElement.clientHeight : window.innerHeight
}
function getScrollHeight() {
  return Prototype.Browser.WebKit ? document.body.scrollHeight : document.documentElement.scrollHeight
}
function observeInitialization(a) {
  Event.observe(document, "dom:loaded", a)
}
(function () {
  var a = navigator.userAgent;
  Prototype.Browser.IE6 = a.indexOf("MSIE 6.0") > -1;
  Prototype.Browser.IE9 = a.indexOf("MSIE 9.0") > -1;
  Prototype.Browser.IEOld = Prototype.Browser.IE && !Prototype.Browser.IE9;
  Prototype.Browser.IOS = a.indexOf("iPhone") > -1 || a.indexOf("iPad") > -1 || a.indexOf("iPod") > -1;
  Prototype.Browser.Android = a.indexOf("Android") > -1;
  Prototype.Browser.supportsTouches = Prototype.Browser.MobileSafari || Prototype.Browser.Android;
  if (Prototype.Browser.supportsTouches) {
    Event.MOUSE_DOWN = "touchstart";
    Event.MOUSE_UP = "touchend";
    Event.MOUSE_MOVE = "touchmove"
  } else {
    Event.MOUSE_DOWN = "mousedown";
    Event.MOUSE_UP = "mouseup";
    Event.MOUSE_MOVE = "mousemove"
  }
})();
var IE6 = {
  addClassName: function (b, a, d, c) {
    if (Prototype.Browser.IE6) {
      b.addClassName(d);
      b.removeClassName(c)
    } else {
      b.addClassName(a)
    }
  }, removeClassName: function (b, a, d, c) {
    if (Prototype.Browser.IE6) {
      b.addClassName(c);
      b.removeClassName(d)
    } else {
      b.removeClassName(a)
    }
  }
};
var TouchEvent = {
  attach: function (a) {
    if (Prototype.Browser.supportsTouches) {
      Object.extend(a, TouchEvent.Methods)
    }
  }
};
TouchEvent.Methods = {
  isLeftClick: function () {
    return this.touches.length == 1
  }, pointerX: function () {
    return this.changedTouches[0].pageX
  }, pointerY: function () {
    return this.changedTouches[0].pageY
  }
};
Object.extend(Function.prototype, {
  bindAsTouchEventListener: function () {
    if (Prototype.Browser.supportsTouches) {
      var a = this;
      return function (c) {
        var b = $A(arguments);
        b.shift();
        var d = c || window.event;
        TouchEvent.attach(d);
        return a.apply(this, [d].concat(b))
      }
    } else {
      return this
    }
  }
});
var Menu = Class.create();
Object.extend(Menu.prototype, {
  initialize: function (a) {
    this.elem = a
  }, layout: function () {
    var b = this.elem.getDimensions();
    var a = this.elem.$$("menuBg");
    a.setStyle({width: b.width + "px", height: b.height + "px"});
    a.$$("menuBgLeft").setStyle({height: (b.height - 9) + "px"});
    a.$$("menuBgCenter").setStyle({width: (b.width - 18) + "px", height: (b.height - 9) + "px"});
    a.$$("menuBgRight").setStyle({height: (b.height - 9) + "px"});
    a.$$("menuBgBottom").setStyle({width: (b.width - 18) + "px"});
    if (Prototype.Browser.IE) {
      this.elem.select(".menuSeparator").each(function (c) {
        c.setStyle({width: (b.width - 14) + "px"})
      })
    }
  }
});
var Notice = Class.create({
  title: null, background: false, timer: null, initialize: function () {
    Event.observe(window, "focus", function () {
      this.stop();
      this.background = false
    }.bindAsEventListener(this));
    Event.observe(window, "blur", function () {
      this.background = true
    }.bindAsEventListener(this))
  }, notify: function (a) {
    this.stop();
    if (this.background) {
      this.title = document.title;
      this.timer = window.setInterval(function () {
        document.title = document.title === this.title ? a : this.title
      }.bind(this), 1000)
    }
  }, stop: function () {
    if (this.timer) {
      window.clearInterval(this.timer);
      this.timer = null;
      document.title = this.title;
      this.title = null
    }
  }
});
if (typeof IEPNGFIX == "undefined") {
  var IEPNGFIX = {
    blank: "/image/iepngfix.gif",
    filter: "DXImageTransform.Microsoft.AlphaImageLoader",
    fixit: function (b, c, d) {
      if (b.filters[this.filter]) {
        var a = b.filters[this.filter];
        a.enabled = true;
        a.src = c;
        a.sizingMethod = d
      } else {
        b.style.filter = "progid:" + this.filter + '(src="' + c + '",sizingMethod="' + d + '")'
      }
    },
    fixwidth: function (a) {
      if (a.currentStyle.width == "auto" && a.currentStyle.height == "auto") {
        a.style.width = a.offsetWidth + "px"
      }
    },
    fixchild: function (d, a) {
      if (!/MSIE (5\.5|6\.)/.test(navigator.userAgent)) {
        return
      }
      for (var c = 0, e = d.childNodes.length; c < e; c++) {
        var b = d.childNodes[c];
        if (b.style) {
          if (b.style.position) {
            b.style.position = b.style.position
          } else {
            b.style.position = "relative"
          }
        }
        if (a && b.hasChildNodes()) {
          this.fixchild(b, a)
        }
      }
    },
    fix: function (c) {
      if (!/MSIE (5\.5|6\.)/.test(navigator.userAgent)) {
        return
      }
      var a = c.currentStyle.backgroundImage || c.style.backgroundImage;
      if (c.tagName == "IMG") {
        this.fixwidth(c);
        this.fixit(c, c.src, "scale");
        c.src = this.blank;
        c.runtimeStyle.behavior = "none"
      } else {
        if (a && a != "none") {
          if (a.match(/^url[("']+(.*\.png)[)"']+$/i)) {
            var b = RegExp.$1;
            this.fixwidth(c);
            c.style.backgroundImage = "none";
            this.fixit(c, b, "scale");
            if (c.tagName == "A" && c.style) {
              if (!c.style.cursor) {
                c.style.cursor = "pointer"
              }
            }
            this.fixchild(c);
            c.runtimeStyle.behavior = "none"
          }
        }
      }
    },
    hover: function (c, b) {
      var a = c.style.backgroundImage;
      if (!a && c.currentStyle) {
        a = c.currentStyle.backgroundImage
      }
      if (c.tagName == "IMG" && b) {
        var d = c.src;
        c.onmouseover = function () {
          c.src = b;
          IEPNGFIX.fix(c)
        };
        c.onmouseout = function () {
          c.src = d;
          IEPNGFIX.fix(c)
        }
      } else {
        if (a && a != "none" && b) {
          c.onmouseover = function () {
            c.style.backgroundImage = "url(" + b + ")";
            IEPNGFIX.fix(c)
          };
          c.onmouseout = function () {
            c.style.backgroundImage = a;
            IEPNGFIX.fix(c)
          }
        }
      }
      IEPNGFIX.fix(c)
    }
  }
}
var g_canvasPreferences = null;
var g_globalPreferences = null;
var g_savePreferenceTimer = null;
function restoreLocalPreferences() {
  if (g_canvasPreferences) {
    var j = g_canvasPreferences.find(function (l) {
      return l.canvasId == g_canvasId
    });
    if (j) {
      if (PrefUtil.valid(j.originX) && PrefUtil.valid(j.originY)) {
        setOrigin(j.originX, j.originY)
      }
      if (PrefUtil.valid(j.showPrivate) && g_canvasTypeFlags & CanvasType.FLAG_PRIVATE) {
        setShowPrivate(j.showPrivate)
      }
    }
  }
  var d = !g_inner;
  var k = !g_inner;
  var b = null;
  var i = !g_inner;
  var g = null;
  var h = null;
  var e = !g_inner;
  var c = !g_inner;
  var a = null;
  var f = !g_inner;
  if (g_globalPreferences) {
    var j = g_globalPreferences;
    k = PrefUtil.maybe(j.palettePinned, true);
    d = k && PrefUtil.maybe(j.paletteVisible, true);
    b = PrefUtil.maybe(j.paletteColors, null);
    e = PrefUtil.maybe(j.dockPinned, true);
    i = e && PrefUtil.maybe(j.dockVisible, true);
    g = PrefUtil.maybe(j.dockWidth, null);
    h = PrefUtil.maybe(j.dockScrollPos, null);
    f = PrefUtil.maybe(j.panelPinned, true);
    c = f && PrefUtil.maybe(j.panelVisible, true);
    a = PrefUtil.maybe(j.panelWidth, null);
    g_fontSize = PrefUtil.maybe(j.fontSize, 100)
  }
  g_panel.restore(a, c, f);
  g_dock.restore(g, h, i, e);
  g_palette.restore(k, d, b)
}
function loadLocalPreferences() {
  var c = getCookie("cp");
  if (c) {
    try {
      g_canvasPreferences = c.evalJSON()
    } catch (b) {
    }
  }
  if (!g_inner) {
    var a = getCookie("gp");
    if (a) {
      try {
        g_globalPreferences = a.evalJSON()
      } catch (b) {
      }
    }
  }
}
function saveLocalPreferences() {
  if (g_savePreferenceTimer) {
    clearTimeout(g_savePreferenceTimer)
  }
  g_savePreferenceTimer = setTimeout(_saveLocalPreferences, 500)
}
function _saveLocalPreferences() {
  var b = !g_canvasPreferences ? new Array() : g_canvasPreferences.select(function (c) {
    return c.canvasId != g_canvasId
  });
  b.push({
    canvasId: g_canvasId,
    originX: g_origin.x,
    originY: g_origin.y,
    zoom: g_zoom,
    showPrivate: g_canvasTypeFlags & CanvasType.FLAG_PRIVATE ? g_showPrivate : false
  });
  while (b.length > 10) {
    b.shift()
  }
  setCookie("cp", Object.toJSON(b));
  if (!g_inner) {
    var a = {
      paletteVisible: false,
      palettePinned: g_palette.isPinned(),
      paletteColors: g_palette.getColors(),
      dockVisible: g_dock.isVisible(),
      dockWidth: g_dock.getWidth(),
      dockScrollPos: g_dock.getMyScrollPos(),
      dockPinned: g_dock.isPinned(),
      panelVisible: g_panel.isVisible(),
      panelWidth: g_panel.getWidth(),
      panelPinned: g_panel.isPinned(),
      fontSize: g_fontSize
    };
    if (g_palette.isExpandable()) {
      a.paletteVisible = g_palette.isExpanded()
    } else {
      if (g_globalPreferences) {
        a.paletteVisible = g_globalPreferences.paletteExpanded || false
      }
    }
    setCookie("gp", Object.toJSON(a))
  }
}
var PrefUtil = {
  valid: function (a) {
    return !Object.isUndefined(a)
  }, maybe: function (a, b) {
    return PrefUtil.valid(a) ? a : b
  }
};
var CANVAS_SIZE = new Size(2560, 1920);
var MIN_ENTRY_SIZE = new Size(150, 60);
var ENTRY_SIZE_MARGIN = new Size(35, 50);
var MAX_AUTO_ENTRY_SIZE = new Size(300, 800);
var DOCK_ENTRY_DRAG_OFFSET = new Point(5, 5);
var FIRST_LOAD_LIMIT = 25;
var LOAD_STEP = 5;
var MIN_RELOAD_DURATION = 60 * 1000;
var MAX_RELOAD_DURATION = 3 * 60 * 1000;
var DEFAULT_RELOAD_DURATION = 60 * 1000;
var INITIAL_RELOAD_DURATION = 60 * 1000;
var MIN_GROUP_RELOAD_DURATION = 10 * 1000;
var INITIAL_GROUP_RELOAD_DURATION = 15 * 1000;
var NEW_ENTRY_MODE_ZINDEX = 10300;
var ANYWHERE_ID = "VwM4HWgw";
var CanvasType = {
  MY: 0,
  REFERENCE: 1,
  INBOX: 2,
  GROUP: 3,
  FLAG_NEW: 1,
  FLAG_DUE: 2,
  FLAG_TAG: 4,
  FLAG_SEND: 8,
  FLAG_PRIVATE: 16,
  FLAG_DOCK: 32,
  FLAG_CALENDAR: 64,
  FLAG_ZINDEX: 128
};
var g_userId = null;
var g_canvasId = null;
var g_canvasType = null;
var g_canvasTypeFlags = false;
var g_groupId = null;
var g_groupName = null;
var g_groupOwnerId = null;
var g_groups = [];
var g_hasBuddies = false;
var g_entries = new Array();
var g_embedded = false;
var g_inner = false;
var g_facebookAppId = null;
var g_origin = new Point(0, 0);
var g_zoom = 1;
var g_loadEntriesTimer = null;
var g_hilightedTags = new Array();
var g_reloadDuration = INITIAL_RELOAD_DURATION;
var g_reloadDurationCount = 3;
var g_reloadTimer = null;
var g_reloading = null;
var g_showPrivate = false;
var g_fontSize = 100;
var g_hideMovie = 0;
var g_movieEntry = null;
var g_newEntryMode = false;
var g_newEntryElems = [];
var g_currentNewEntryElem = null;
var g_currentNewEntryHilight = false;
var g_init = false;
var g_palette = null;
var g_dock = null;
var g_panel = null;
var g_popup = null;
var g_search = null;
var g_notification = null;
var g_movieNotification = null;
var g_board = null;
var g_social = null;
var g_notice = new Notice();
observeInitialization(function () {
  if (g_groupId) {
    MIN_RELOAD_DURATION = MIN_GROUP_RELOAD_DURATION;
    INITIAL_RELOAD_DURATION = INITIAL_GROUP_RELOAD_DURATION
  }
  g_reloadDuration = INITIAL_RELOAD_DURATION;
  initBackground();
  loadLocalPreferences();
  g_palette = new Palette(newEntryStartDrag);
  g_dock = new Dock();
  g_panel = new Panel($("canvasView"), g_canvasTypeFlags & CanvasType.FLAG_CALENDAR);
  g_notification = new Notification($("notification"));
  g_movieNotification = new MovieNotification($("movieNotification"));
  g_board = new Board($("board"));
  if ($("social")) {
    g_social = new Social($("social"), g_facebookAppId)
  }
  if (g_embedded) {
    initializeEntries()
  } else {
    loadEntries()
  }
  $("canvas").observe(Event.MOUSE_DOWN, originStartDrag);
  Event.observe(document, "mousewheel", scroll);
  Event.observe(document, "DOMMouseScroll", scroll);
  function a() {
    return !g_dialogsStub.isDialogVisible()
  }

  if (g_canvasTypeFlags & CanvasType.FLAG_NEW) {
    Event.observe(document, "keydown", function (g) {
      if (49 <= g.keyCode && g.keyCode < 49 + g_palette.PALETTE_ENTRIES.length && a()) {
        var f = g_palette.PALETTE_ENTRIES[g.keyCode - 49];
        var h = new NewEntry(f.color, f.type, f.width, f.height, f.index);
        g_dialogsStub.bind(function (i) {
          switch (f.type) {
            case NewEntry.TYPE_NORMAL:
              i.showNewEntryDialog(h);
              break;
            case NewEntry.TYPE_IMAGE:
              i.showImageEntryDialog(h);
              break;
            case NewEntry.TYPE_MOVIE:
              i.showMovieEntryDialog(h);
              break;
            case NewEntry.TYPE_ATTACHMENT:
              i.showAttachmentEntryDialog(h);
              break
          }
        });
        g.stop()
      }
    }, true)
  }
  Event.observe(document, "keydown", function (f) {
    if (f.keyCode == 77 && a()) {
      toggleNewEntryMode()
    } else {
      if (g_newEntryMode && (f.keyCode == 37 || f.keyCode == 80 || f.keyCode == 75) && a()) {
        previousNewEntry()
      } else {
        if (g_newEntryMode && (f.keyCode == 39 || f.keyCode == 78 || f.keyCode == 74) && a()) {
          nextNewEntry()
        } else {
          if (g_newEntryMode && (f.keyCode == 27) && a()) {
            leaveNewEntryMode()
          } else {
            if (f.keyCode == 81 && a()) {
              toggleShowPrivate()
            }
          }
        }
      }
    }
  }, true);
  Event.observe(document, "keypress", function (f) {
    if (f.charCode == 60 && a()) {
      g_dock.selector.previous()
    } else {
      if (f.charCode == 62 && a()) {
        g_dock.selector.next()
      } else {
        if (f.charCode == 63 && a()) {
          g_dock.selector.my()
        }
      }
    }
  }, true);
  g_dock.load.bind(g_dock).defer();
  layoutCanvasView();
  $("main").setStyle({visibility: "visible"});
  restoreLocalPreferences();
  g_dock.adjustWidth();
  saveLocalPreferences();
  var c = $("ad");
  if (c) {
    var e = $("getAdPositionForm");
    e.windowWidth.value = getWindowWidth();
    e.windowHeight.value = getWindowHeight();
    new AjaxUtil().get("/api/canvas/adPosition", e, function (f) {
      c.setStyle({left: f.left + "px", top: f.top + "px", display: "block"})
    }, function () {
    })
  }
  var d = $("recommendation");
  if (Prototype.Browser.IE && !$("trialBanner") && !getCookie("ieRec")) {
    d.show();
    d.remove.bind(d).delay(30);
    setCookie("ieRec", "1", 7)
  } else {
    d.remove()
  }
  var b = $("ios");
  if (Prototype.Browser.IOS && getCookie("ios") !== "1") {
    b.show();
    b.$$$(".close span").observe("click", function () {
      setCookie("ios", "1", 14);
      b.remove()
    })
  } else {
    b.remove()
  }
  g_init = true
});
Event.observe(window, "resize", function () {
  if (!g_init) {
    return
  }
  layoutCanvasView();
  updateEntriesVisibility();
  g_dock.adjustWidth();
  setOrigin(g_origin.x, g_origin.y)
});
function initBackground() {
  if (window != window.top && window.name == ANYWHERE_ID) {
    $(document.body).setStyle({background: "transparent url(" + g_imageAssetBaseURL + "/image/anywherebg.png)"});
    $("canvas").setStyle({background: "transparent none"})
  }
}
function showBackground() {
  if (window != window.top && window.id == ANYWHERE_ID) {
    $(document.body).setStyle({background: "transparent url(" + g_imageAssetBaseURL + "/image/anywherebg.png)"})
  }
}
function hideBackground() {
  if (window != window.top && window.id == ANYWHERE_ID) {
    $(document.body).setStyle({background: "transparent none"})
  }
}
function initializeEntries() {
  var a = !(g_canvasTypeFlags & CanvasType.FLAG_PRIVATE) || !g_showPrivate;
  g_entries.each(function (b) {
    var c = getEntryElemFromId(b.id);
    b.maskStatus = 0;
    Entry.bind(b, c);
    updateContentSize(c, c.getWidth(), c.getHeight());
    if (b.isFlag(Entry.FLAG_AUTOHIDE)) {
      setupAutoHide(b, c)
    }
    g_panel.initializeEntry(b);
    if (a && b.intimate) {
      b.maskPrivate()
    }
    if (b.intimate) {
      setAutoShowPrivate(b, c)
    }
    if (Prototype.Browser.supportsTouches) {
      installHandlers(c)
    } else {
      c.observe("mouseover", installHandlersWithEvent)
    }
  });
  updateEntriesVisibility();
  if (g_entries.length >= 120) {
    info("canvas.info.tooManyEntries")
  }
}
var MovingOrigin = Class.create({
  initialize: function (c, b, a, d) {
    this.originX = c;
    this.originY = b;
    this.offsetX = a;
    this.offsetY = d
  }
});
var movingOrigin = null;
var originStartDrag = function (c) {
  if (!c.isLeftClick()) {
    return
  }
  if (movingOrigin != null) {
    originEndDrag(c)
  }
  var b = c.pointerX();
  var e = c.pointerY();
  var a = $("canvas").select(".entry");
  for (var d = 0; d < a.length; ++d) {
    if (Position.within(a[d], b, e)) {
      return
    }
  }
  movingOrigin = new MovingOrigin(g_origin.x, g_origin.y, b, e);
  Event.observe(document, Event.MOUSE_MOVE, originDrag);
  Event.observe(document, Event.MOUSE_UP, originEndDrag);
  if (g_inner) {
    Event.observe(document, "mouseout", originOutDrag)
  }
  c.stop()
}.bindAsTouchEventListener();
var originEndDrag = function (a) {
  if (!movingOrigin) {
    return
  }
  originDrag(a);
  Event.stopObserving(document, Event.MOUSE_MOVE, originDrag);
  Event.stopObserving(document, Event.MOUSE_UP, originEndDrag);
  if (g_inner) {
    Event.stopObserving(document, "mouseout", originOutDrag)
  }
  movingOrigin = null
}.bindAsTouchEventListener();
var originOutDrag = function (a) {
  if (!Position.within($("canvasView"), a.pointerX(), a.pointerY())) {
    originEndDrag(a)
  }
}.bindAsEventListener();
var originDrag = function (a) {
  setOrigin(movingOrigin.originX + (movingOrigin.offsetX - a.pointerX()), movingOrigin.originY + (movingOrigin.offsetY - a.pointerY()));
  a.stop()
}.bindAsTouchEventListener();
function setOrigin(a, c) {
  var b = $("canvasView").getDimensions();
  g_origin.x = Math.max(0, Math.min(a, CANVAS_SIZE.width - Math.floor(b.width / g_zoom)));
  g_origin.y = Math.max(0, Math.min(c, CANVAS_SIZE.height - Math.floor(b.height / g_zoom)));
  $("canvas").setStyle({left: -Math.floor(g_origin.x * g_zoom) + "px", top: -Math.floor(g_origin.y * g_zoom) + "px"});
  updateEntriesVisibility();
  g_panel.layout();
  saveLocalPreferences()
}
function scroll(b) {
  if (g_dialogsStub.isDialogVisible() || (g_popup && g_popup.isPopupVisible())) {
    return
  }
  var a = g_origin.x;
  var c = g_origin.y;
  if (Prototype.Browser.WebKit) {
    a -= b.wheelDeltaX / 3;
    c -= b.wheelDeltaY / 3
  } else {
    if (Prototype.Browser.Gecko) {
      if (b.axis == MouseScrollEvent.HORIZONTAL_AXIS) {
        a += b.detail * 3
      }
      if (b.axis == MouseScrollEvent.VERTICAL_AXIS) {
        c += b.detail * 3
      }
    } else {
      c -= b.wheelDelta / 3
    }
  }
  setOrigin(a, c)
}
function ensureEntryElemVisible(d) {
  var b = d.visible();
  if (!b) {
    d.show()
  }
  var g = d.positionedOffset();
  var c = d.getDimensions();
  if (!b) {
    d.hide()
  }
  var f = $("canvasView").getDimensions();
  var a = g.left - Math.floor((f.width - c.width) / 2);
  var e = g.top - Math.floor((f.height - c.height) / 3);
  if (a != g_origin.x || e != g_origin.y) {
    setOrigin(a, e)
  }
  flushEntryElem(d)
}
function flushEntryElem(c) {
  var b = c.getBounds();
  var a = new Rectangle(b.left - 200, b.top - 200, b.width + 400, b.height + 400);
  showEffect(a, c.getRotateAngle(), b, c.getRotateAngle(), Entry.getSolidColor(c.entry.color))
}
function showEffect(i, e, h, j, d, a) {
  var c = new Element("div");
  document.body.appendChild(c);
  c.setStyle({
    position: "absolute",
    left: i.left + "px",
    top: i.top + "px",
    width: i.width + "px",
    height: i.height + "px",
    zIndex: 20000,
    backgroundColor: d,
    opacity: 0.5
  });
  if (e) {
    c.setRotateAngle(e)
  }
  var b = 0;
  var g = function () {
    if (b < 10) {
      c.setStyle({
        left: Math.floor((i.left * (10 - b) + h.left * b) / 10) + "px",
        top: Math.floor((i.top * (10 - b) + h.top * b) / 10) + "px",
        width: Math.floor((i.width * (10 - b) + h.width * b) / 10) + "px",
        height: Math.floor((i.height * (10 - b) + h.height * b) / 10) + "px"
      });
      c.setRotateAngle((e * (10 - b) + j * b) / 10);
      ++b;
      g.delay(0.03)
    } else {
      c.remove();
      if (a) {
        a()
      }
    }
  };
  g.defer()
}
function updateCanvasDragPosition() {
  var a = $("canvas").cumulativeOffset();
  ["canvasDrag", "canvasCopy"].each(function (b) {
    $(b).setStyle({left: a.left + "px", top: a.top + "px"})
  })
}
var NewEntry = Class.create({
  initialize: function (b, e, d, a, c) {
    this.color = b;
    this.type = e;
    this.fontSize = 100;
    this.left = 0;
    this.top = 0;
    this.width = d || 280;
    this.height = a || 200;
    this.index = Object.isUndefined(c) ? null : c
  }
});
NewEntry.TYPE_NORMAL = 0;
NewEntry.TYPE_IMAGE = 1;
NewEntry.TYPE_MOVIE = 2;
NewEntry.TYPE_ATTACHMENT = 3;
var newEntry = null;
var newEntryStartDrag = function (d, a) {
  if (newEntry != null) {
    newEntryEndDrag(d)
  }
  newEntry = new NewEntry(a.color, a.type, a.width, a.height, a.index);
  updateCanvasDragPosition();
  var b = $("canvas").cumulativeOffset();
  var e = new Point(d.pointerX() - b.left - 20, d.pointerY() - b.top - 20);
  var c = $("newEntryDrag");
  c.setStyle({left: e.x + "px", top: e.y + "px", width: ls2ps(a.width) + "px", height: ls2ps(a.height) + "px"});
  c.show();
  updateEntryBackgroundColor(c, TYPE_NORMAL, newEntry.color);
  updateEntryBackgroundSize(c, null);
  Event.observe(document, Event.MOUSE_MOVE, newEntryDrag);
  Event.observe(document, Event.MOUSE_UP, newEntryEndDrag)
}.bindAsTouchEventListener();
var newEntryEndDrag = function (a) {
  var c = $("newEntryDrag");
  var i = a.pointerX();
  var g = a.pointerY();
  if (Position.within($("canvasView"), i, g) && Position.within($("canvas"), i, g)) {
    if (newEntry.type == NewEntry.TYPE_ATTACHMENT && g_userId == 0) {
      if (confirm(MESSAGES["confirm.uploadAttachment"])) {
        openAtTop("/session/top?redirect=" + encodeURIComponent(document.URL))
      }
      c.hide()
    } else {
      var f = $("canvas").cumulativeOffset();
      var h = new Point(i - f.left - 20, g - f.top - 20);
      var j = c.getDimensions();
      var b = Position.within(g_palette.element(), i, g);
      newEntry.left = b ? 0 : px2lx(h.x);
      newEntry.top = b ? 0 : py2ly(h.y);
      newEntry.width = ps2ls(j.width);
      newEntry.height = ps2ls(j.height);
      var d = newEntry;
      g_dialogsStub.bind(function (e) {
        switch (d.type) {
          case NewEntry.TYPE_NORMAL:
            e.showNewEntryDialog(d, c);
            break;
          case NewEntry.TYPE_IMAGE:
            e.showImageEntryDialog(d, c);
            break;
          case NewEntry.TYPE_MOVIE:
            e.showMovieEntryDialog(d, c);
            break;
          case NewEntry.TYPE_ATTACHMENT:
            e.showAttachmentEntryDialog(d, c);
            break
        }
        if (b) {
          c.hide()
        }
      })
    }
  } else {
    c.hide()
  }
  if (!g_palette.isPinned()) {
    g_palette.collapse()
  }
  newEntry = null;
  Event.stopObserving(document, Event.MOUSE_MOVE, newEntryDrag);
  Event.stopObserving(document, Event.MOUSE_UP, newEntryEndDrag);
  a.stop()
}.bindAsTouchEventListener();
var newEntryDrag = function (c) {
  var b = $("canvas").cumulativeOffset();
  var a = c.pointerX() - b.left - 20;
  var d = c.pointerY() - b.top - 20;
  $("newEntryDrag").setStyle({left: a + "px", top: d + "px"});
  c.stop()
}.bindAsTouchEventListener();
function loadEntries() {
  cancelReloading();
  var a = $("getEntriesForm");
  new AjaxUtil().get("/api/entry/gets", a, updateEntries, error)
}
function updateEntries(c) {
  var i = parseDate(c.today);
  var g = i.getTime() != g_today.getTime();
  g_today = i;
  var d = c.entries;
  if (g_loadEntriesTimer) {
    clearTimeout(g_loadEntriesTimer);
    g_loadEntriesTimer = null
  }
  clearEntries();
  if (d.length >= 120) {
    info("canvas.info.tooManyEntries")
  }
  d.each(function (f) {
    Entry.prebind(f);
    g_entries.push(f)
  });
  var k = FIRST_LOAD_LIMIT;
  if (d.length > FIRST_LOAD_LIMIT) {
    var b = new Array();
    var h = new Array();
    var j = $("canvasView").getDimensions();
    d.each(function (f) {
      (isEntryInCanvasView(j, f) ? b : h).push(f)
    });
    b.sort(function (l, f) {
      return f.getUpdated().getTime() - l.getUpdated().getTime()
    });
    k = Math.min(k, b.length);
    d = b.concat(h)
  }
  for (var a = 0; a < Math.min(k, d.length); ++a) {
    insertEntryElem(d[a], false, false)
  }
  if (a < d.length) {
    var e = function () {
      if (a < d.length) {
        var f = Math.min(a + LOAD_STEP, d.length);
        for (; a < f; ++a) {
          insertEntryElem(d[a], false, false)
        }
        g_loadEntriesTimer = setTimeout(e, 0)
      } else {
        handleFragment();
        g_loadEntriesTimer = null;
        scheduleReloading()
      }
    };
    g_loadEntriesTimer = setTimeout(e, 0)
  } else {
    handleFragment();
    scheduleReloading()
  }
  if (g) {
    g_panel.getCalendar().rebuild()
  }
}
function handleFragment() {
  if (document.URL.match(/#e([0-9]+)$/)) {
    var a = getEntryElemFromId(RegExp.$1);
    if (a) {
      ensureEntryElemVisible(a);
      flushEntryElem(a)
    }
  } else {
    if (document.URL.match(/#showSocial$/)) {
      if (g_social) {
        g_social.showImmediate()
      }
    }
  }
}
function clearEntries() {
  $("canvas").select(".entry").each(Element.remove);
  g_entries.clear()
}
var Reloading = Class.create({
  initialize: function () {
    this.canceled = false
  }, cancel: function () {
    this.canceled = true
  }, process: function (a) {
    g_reloading = null;
    if (!this.canceled) {
      reupdateEntries(a)
    }
  }
});
function scheduleReloading() {
  if (g_reloadTimer) {
    clearTimeout(g_reloadTimer)
  }
  g_reloadTimer = setTimeout(reloadEntries, g_reloadDuration)
}
function cancelReloading() {
  if (g_reloading) {
    g_reloading.cancel();
    g_reloading = null
  }
  if (g_reloadTimer) {
    clearTimeout(g_reloadTimer);
    g_reloadTimer = null
  }
}
function reloadEntries() {
  g_reloadTimer = null;
  if (g_newEntryMode) {
    scheduleReloading()
  } else {
    g_reloading = new Reloading();
    var a = $("getEntriesForm");
    new AjaxUtil().get("/api/entry/gets", a, g_reloading.process.bind(g_reloading), function () {
      g_reloading = null;
      g_reloadDuration = Math.min(MAX_RELOAD_DURATION, Math.max(g_reloadDuration * 2, DEFAULT_RELOAD_DURATION));
      g_reloadDurationCount = 0;
      scheduleReloading()
    })
  }
}
function reupdateEntries(d) {
  if (g_newEntryMode) {
    scheduleReloading();
    return
  }
  if (shouldPostpondReupdate()) {
    return postpondReupdate(0)
  }
  if (parseDate(d.today).getTime() != g_today.getTime()) {
    updateEntries(d);
    return
  }
  var g = d.entries;
  var l = false;
  var i = false;
  var f = false;
  var h = g_entries.clone();
  for (var b = 0; b < g.length; ++b) {
    var p = g[b];
    var q = getEntryElemFromId(p.id);
    if (q) {
      var a = q.entry;
      var c = h.indexOf(a);
      h[c] = null;
      if (!a.equals(p)) {
        var k = q.getBounds();
        var e = q.getRotateAngle();
        var o = replaceEntryElem(p, true);
        var j = o.getBounds();
        var r = o.getRotateAngle();
        if (!k.equals(j) || e != r) {
          showEffect(k, e, j, r, Entry.getSolidColor(p.color))
        }
        f = true
      }
    } else {
      insertEntryElem(p, true, true);
      l = true
    }
  }
  h.each(function (m) {
    if (m) {
      var s = getEntryElemFromId(m.id);
      var n = function () {
        removeEntryElem(m.id)
      };
      dimEntryElemFunc(s, n)();
      i = true
    }
  });
  if (i || f) {
    ensureAllEntriesNotHidden()
  }
  if (l || i || f) {
    g_dock.loadEntries(g_canvasId);
    g_panel.getCalendar().refresh()
  }
  if (l) {
    info("canvas.info.added");
    g_notice.notify(MESSAGES["canvas.info.added"])
  } else {
    if (f) {
      info("canvas.info.updated");
      g_notice.notify(MESSAGES["canvas.info.updated"])
    }
  }
  if (l || i || f) {
    g_reloadDuration = Math.max(MIN_RELOAD_DURATION, Math.min(Math.floor(g_reloadDuration / 2), INITIAL_RELOAD_DURATION));
    g_reloadDurationCount = 0
  } else {
    ++g_reloadDurationCount;
    if (g_reloadDurationCount > 5) {
      g_reloadDuration = Math.min(g_reloadDuration * 2, MAX_RELOAD_DURATION);
      g_reloadDurationCount = 0
    }
  }
  scheduleReloading()
}
function shouldPostpondReupdate() {
  return Request.requestOngoing() || isMoving() || isResizing() || g_dialogsStub.isDialogVisible()
}
function postpondReupdate(a) {
  if (shouldPostpondReupdate()) {
    if (a > 3) {
      scheduleReloading()
    } else {
      postpondReupdate.curry(a + 1).delay(1)
    }
  } else {
    reloadEntries()
  }
}
function layoutCanvasView() {
  var a = $("canvasView");
  a.setStyle({height: calcCanvasViewHeight() + "px"});
  g_panel.layout()
}
function calcCanvasViewHeight() {
  return getWindowHeight() - g_board.getHeight()
}
function installZIndexHandler(c) {
  var b = getHandleFromDraggable(c);
  var a = b || (Prototype.Browser.IE ? c.$$("bg") : c);
  a.observe("mousedown", zIndexHandler);
  if (Prototype.Browser.IE) {
    c.$$("base").observe("mousedown", zIndexHandler)
  }
}
function uninstallZIndexHandler(c) {
  var b = getHandleFromDraggable(c);
  var a = b || (Prototype.Browser.IE ? c.$$("bg") : c);
  a.stopObserving("mousedown", zIndexHandler);
  if (Prototype.Browser.IE) {
    c.$$("base").stopObserving("mousedown", zIndexHandler)
  }
}
function zIndexHandler(a) {
  if (!a.isLeftClick() || a.shiftKey) {
    return
  }
  if (a.altKey) {
    bringEntryToBottom(getEntryElemFromChild(a.element()).entry)
  } else {
    bringEntryToTop(getEntryElemFromChild(a.element()).entry)
  }
}
function bringEntryToTop(b) {
  stopHilightCurrentNewEntryElem();
  var a = g_entries.clone();
  a.sort(Entry.compareZIndex);
  if (a.last() == b) {
    return
  }
  var c = 100;
  a.each(function (d) {
    if (d != b) {
      d.setZIndex(c);
      ++c
    }
  });
  b.setZIndex(c);
  if (g_canvasTypeFlags & CanvasType.FLAG_ZINDEX) {
    updateEntryZIndices(a)
  }
}
function bringEntriesToTop(a) {
  var c = g_entries.clone();
  c.sort(Entry.compareZIndex);
  var d = 100;
  c.each(function (f) {
    if (a.indexOf(f) == -1) {
      f.setZIndex(d);
      ++d
    }
  });
  var b = a.clone();
  b.sort(Entry.compareZIndex);
  b.each(function (f) {
    f.setZIndex(d);
    ++d
  });
  if (g_canvasTypeFlags & CanvasType.FLAG_ZINDEX) {
    updateEntryZIndices(c)
  }
}
function bringEntryToBottom(b) {
  stopHilightCurrentNewEntryElem();
  var a = g_entries.clone();
  a.sort(Entry.compareZIndex);
  if (a.first() == b) {
    return
  }
  b.setZIndex(100);
  var c = 101;
  a.each(function (d) {
    if (d != b) {
      d.setZIndex(c);
      ++c
    }
  });
  if (g_canvasTypeFlags & CanvasType.FLAG_ZINDEX) {
    updateEntryZIndices(a)
  }
}
function getTopZIndex() {
  var a = 100;
  g_entries.each(function (b) {
    a = Math.max(a, b.zIndex)
  });
  return a
}
var Dragging = Class.create({
  initialize: function (b, a, d, c) {
    this.draggable = b;
    this.offsetX = a;
    this.offsetY = d;
    this.others = c
  }, elems: function () {
    var a = this.others ? this.others.clone() : [];
    a.sort(function (b, c) {
      return b.entry.zIndex - c.entry.zIndex
    });
    a.push(this.draggable);
    return a
  }
});
var dragging = null;
function installDragHandler(a) {
  var b = getHandleFromDraggable(a);
  b.observe(Event.MOUSE_DOWN, moveStartDrag)
}
function uninstallDragHandler(a) {
  var b = getHandleFromDraggable(a);
  b.stopObserving(Event.MOUSE_DOWN, moveStartDrag)
}
var moveStartDrag = function (c) {
  if (!c.isLeftClick()) {
    return
  }
  if (dragging != null) {
    moveEndDrag(c)
  }
  if (c.altKey) {
    return
  }
  var g = c.element();
  if (g.hasClassName("nodrag") || g.up(".nodrag")) {
    return
  }
  var k = getDraggableFromHandle(g);
  var i = k.hasClassName("entry");
  var j = null;
  if (i && c.shiftKey) {
    var d = getMovableIntersectedEntries(k);
    if (d.length != 0) {
      d.each(Element.show);
      var f = d.map(function (l) {
        return l.entry
      });
      f.push(k.entry);
      bringEntriesToTop(f);
      j = d
    }
  }
  var h = k.cumulativeOffset();
  dragging = new Dragging(k, c.pointerX() - h.left, c.pointerY() - h.top, j);
  Event.observe(document, Event.MOUSE_MOVE, moveDrag);
  Event.observe(document, Event.MOUSE_UP, moveEndDrag);
  Event.observe(document, "keydown", moveKeyChange);
  Event.observe(document, "keyup", moveKeyChange);
  if (i) {
    updateCanvasDragPosition();
    var e = $("canvasDrag");
    var b = $("canvasCopy");
    var a = dragging.elems();
    a.each(function (m) {
      e.appendChild(m);
      var l = m.cloneNode(true);
      l.removeAttribute("id");
      b.appendChild(l)
    });
    if (c.ctrlKey || c.metaKey) {
      b.show()
    }
  }
  c.stop()
}.bindAsTouchEventListener();
var moveEndDrag = function (r) {
  moveDrag(r);
  var j = r.pointerX();
  var i = r.pointerY();
  var q = r.ctrlKey || r.metaKey;
  var d = dragging.draggable;
  var e = d.hasClassName("entry");
  var u = j - DOCK_ENTRY_DRAG_OFFSET.x;
  var s = i - DOCK_ENTRY_DRAG_OFFSET.y;
  var k = g_dock.getDockItemFromPosition(u, s);
  var a = g_panel.isInside(u, s);
  Event.stopObserving(document, Event.MOUSE_MOVE, moveDrag);
  Event.stopObserving(document, Event.MOUSE_UP, moveEndDrag);
  Event.stopObserving(document, "keydown", moveKeyChange);
  Event.stopObserving(document, "keyup", moveKeyChange);
  if (e) {
    var f = d;
    var v = dragging.elems();

    function n(z, A, y) {
      if (z != g_canvasId && y) {
        var x = v.find(function (B) {
          return B.entry.intimate
        });
        var p = q ? "confirm.copyPrivateEntry" : "confirm.transferPrivateEntry";
        if (x && !confirm(MESSAGES[p])) {
          v.each(function (B) {
            B.entry.updatePosition();
            B.show()
          });
          return
        }
      }
      if (dragging.others) {
        if (q || z != g_canvasId) {
          var w = v.map(function (B) {
            if (q) {
              B.entry.updatePosition();
              B.show()
            }
            return new Point(A.x - f.entry.left + B.entry.left, A.y - f.entry.top + B.entry.top)
          });
          if (q) {
            copyEntries(v, z, w)
          } else {
            transferEntries(v, z, w)
          }
        } else {
          v.each(function (B) {
            B.setStyle({
              left: lx2px(A.x - f.entry.left + B.entry.left) + "px",
              top: ly2py(A.y - f.entry.top + B.entry.top) + "px"
            });
            B.show()
          });
          moveEntries(v)
        }
      } else {
        if (q) {
          f.show();
          f.entry.updatePosition();
          copyEntry(f.entry, z, A)
        } else {
          if (z != g_canvasId) {
            transferEntry(f, z, A)
          } else {
            f.setStyle({left: lx2px(A.x) + "px", top: ly2py(A.y) + "px"});
            moveEntry(f, A)
          }
        }
      }
    }

    var c = $("canvas");
    v.each(function (p) {
      c.appendChild(p)
    });
    if (k != null) {
      var b = f.entry;
      f.setStyle({left: lx2px(b.left) + "px", top: ly2py(b.top) + "px"});
      var l = k.hasClassName("dockMyItem");
      if (l || k.hasClassName("dockWritableItem")) {
        var t = g_dock.getCanvasIdFromDockItem(k);
        var g = g_dock.mapScreenDockPosToCanvasPos(k, u, s);
        n(t, g, !l)
      }
      $("dockEntryDrag").hide()
    } else {
      if (a) {
        n(g_canvasId, g_panel.mapScreenPanelPosToCanvasPos(u, s), false);
        $("dockEntryDrag").hide()
      } else {
        if (dragging.others) {
          if (q) {
            var h = v.map(function (p) {
              var w = p.positionedOffset();
              p.entry.updatePosition();
              return new Point(w.left, w.top)
            });
            copyEntries(v, g_canvasId, h)
          } else {
            moveEntries(v)
          }
        } else {
          if (q) {
            var m = f.positionedOffset();
            f.entry.updatePosition();
            copyEntry(f.entry, g_canvasId, new Point(px2lx(m.left), py2ly(m.top)))
          } else {
            moveEntry(f)
          }
        }
      }
    }
  }
  var o = $("canvasCopy");
  o.removeAllChildren();
  o.hide();
  dragging = null
}.bindAsTouchEventListener();
var moveDrag = function (q) {
  var j = q.pointerX();
  var i = q.pointerY();
  var g = dragging.draggable;
  var h = g.hasClassName("entry");
  var t = j - DOCK_ENTRY_DRAG_OFFSET.x;
  var r = i - DOCK_ENTRY_DRAG_OFFSET.y;
  var k = g_dock.getDockItemFromPosition(t, r);
  var a = g_panel.isInside(t, r);
  var e = $("canvas");
  var m = e.cumulativeOffset();
  if (h && (a || (k && (k.hasClassName("dockMyItem") || k.hasClassName("dockWritableItem"))))) {
    var s = $("dockEntryDrag");
    var l = $("dockEntryDragMain");
    s.setStyle({left: (t - m.left) + "px", top: (r - m.top) + "px"});
    if (g.visible()) {
      g.hide();
      if (dragging.others) {
        dragging.others.each(Element.hide)
      }
      var b = g.entry;
      var o = a ? g_panel : g_dock;
      var d = a ? g_panel.mapCanvasToPanelX : g_dock.mapCanvasToDockX;
      var c = a ? g_panel.mapCanvasToPanelY : g_dock.mapCanvasToDockY;
      l.setStyle({
        width: d.call(o, b.width) + "px",
        height: c.call(o, b.height) + "px",
        backgroundColor: Entry.getSolidColor(b.color)
      });
      l.setRotateAngle(b.angle);
      s.select(".dockEntryDragOther").each(Element.remove);
      if (dragging.others) {
        dragging.others.each(function (u) {
          var v = u.entry;
          var w = new Element("div");
          w.addClassName("dockEntryDrag");
          w.addClassName("dockEntryDragOther");
          w.setStyle({
            left: d.call(o, v.left - b.left) + "px",
            top: d.call(o, v.top - b.top) + "px",
            width: d.call(o, v.width) + "px",
            height: c.call(o, v.height) + "px",
            backgroundColor: Entry.getSolidColor(v.color)
          });
          w.setRotateAngle(v.angle);
          s.appendChild(w)
        })
      }
      s.show()
    }
  } else {
    var f = 0;
    var n = 0;
    if (h) {
      f = j - m.left - dragging.offsetX;
      n = i - m.top - dragging.offsetY
    } else {
      f = j - dragging.offsetX;
      n = i - dragging.offsetY
    }
    g.setStyle({left: f + "px", top: n + "px"});
    if (h && dragging.others) {
      dragging.others.each(function (u) {
        u.setStyle({left: (f - g.entry.left + u.entry.left) + "px", top: (n - g.entry.top + u.entry.top) + "px"})
      })
    }
    if (!g.visible()) {
      g.show();
      if (dragging.others) {
        dragging.others.each(Element.show)
      }
      $("dockEntryDrag").hide()
    }
  }
  function p(y) {
    if (!y.isPinned()) {
      var x = y.isVisible();
      var u = y.within(j, i);
      if (!x && u) {
        y.showAuto(q)
      } else {
        if (x && !u) {
          y.hideAuto(q)
        }
      }
    }
  }

  p(g_dock);
  p(g_panel);
  q.stop()
}.bindAsTouchEventListener();
function moveKeyChange(a) {
  var b = $("canvasCopy");
  if (a.ctrlKey || a.metaKey) {
    b.show()
  } else {
    b.hide()
  }
}
function getHandleFromDraggable(a) {
  return a.$$("handle")
}
function getDraggableFromHandle(a) {
  return a.up(".draggable")
}
function isMoving() {
  return dragging != null
}
var Resizing = Class.create({
  initialize: function (b, a, c) {
    this.resizable = b;
    this.offsetX = a;
    this.offsetY = c
  }
});
var resizing = null;
function installResizeHandler(b) {
  var a = getGripFromResizable(b);
  a.observe(Event.MOUSE_DOWN, resizeStartDrag)
}
function uninstallResizeHandler(b) {
  var a = getGripFromResizable(b);
  a.stopObserving(Event.MOUSE_DOWN, resizeStartDrag)
}
var resizeStartDrag = function (d) {
  if (!d.isLeftClick()) {
    return
  }
  if (resizing != null) {
    resizeEndDrag(d)
  }
  var a = d.element();
  var c = getResizableFromGrip(a);
  var e = c.cumulativeOffset();
  var b = c.getDimensions();
  resizing = new Resizing(c, d.pointerX() - (e.left + b.width), d.pointerY() - (e.top + b.height));
  Event.observe(document, Event.MOUSE_MOVE, resizeDrag);
  Event.observe(document, Event.MOUSE_UP, resizeEndDrag);
  if (c.hasClassName("entry") && c.entry.isFlag(Entry.FLAG_AUTOHIDE)) {
    c.entry.resizing = true;
    c.$$("action").hide()
  }
  d.stop()
}.bindAsTouchEventListener();
var resizeEndDrag = function (b) {
  var a = resizing.resizable;
  var d = a.hasClassName("entry");
  var c = Prototype.Browser.IE && d && a.entry.intimate && !g_showPrivate;
  if (c) {
    a.entry.unmaskPrivate()
  }
  resizeDrag(b);
  if (c) {
    a.entry.maskPrivate()
  }
  Event.stopObserving(document, Event.MOUSE_MOVE, resizeDrag);
  Event.stopObserving(document, Event.MOUSE_UP, resizeEndDrag);
  resizing = null;
  if (d) {
    a.entry.resizing = false;
    moveEntry(a)
  }
}.bindAsTouchEventListener();
var resizeDrag = function (d) {
  var b = resizing.resizable;
  var g = b.cumulativeOffset();
  var e = b.minSize || MIN_ENTRY_SIZE;
  var a = Math.max(e.width, d.pointerX() - g.left - resizing.offsetX);
  var c = Math.max(e.height, d.pointerY() - g.top - resizing.offsetY);
  var f = b.maxSize;
  if (f) {
    a = Math.min(f.width || a, a);
    c = Math.min(f.height || c, c)
  }
  b.setStyle({width: a + "px", height: c + "px"});
  if (b.hasClassName("entryBg")) {
    updateEntryBackgroundSize(b, null)
  }
  if (b.resizeHandler) {
    b.resizeHandler()
  }
  d.stop()
}.bindAsTouchEventListener();
function getGripFromResizable(a) {
  return a.$$("grip")
}
function getResizableFromGrip(a) {
  return a.up(".resizable")
}
function isResizing() {
  return resizing != null
}
var Rotating = Class.create({
  initialize: function (e, c, b, f, d, a) {
    this.rotatable = e;
    this.centerX = c;
    this.centerY = b;
    this.offsetAngle = this.rawAngle(f, d);
    this.initialAngle = a
  }, angle: function (c, b) {
    var a = Rotating.normalizeAngle(this.rawAngle(c, b) - this.offsetAngle + this.initialAngle);
    if (this.initialAngle == 0) {
      return a
    } else {
      if (Math.abs(a % 360) < 5) {
        return 0
      } else {
        return a
      }
    }
  }, rawAngle: function (c, b) {
    var a = 0;
    if (c != this.centerX) {
      a = Math.atan2(b - this.centerY, c - this.centerX)
    } else {
      a = b > this.centerY ? Math.PI / 2 : -Math.PI / 2
    }
    return a * 360 / (2 * Math.PI)
  }
});
Rotating.normalizeAngle = function (a) {
  return (((a + 180) % 360) + 360) % 360 - 180
};
var rotating = null;
function installRotateHandler(b) {
  var a = getGripFromRotatable(b);
  a.observe(Event.MOUSE_DOWN, rotateStartDrag)
}
function uninstallRotateHandler(b) {
  var a = getGripFromRotatable(b);
  a.stopObserving(Event.MOUSE_DOWN, rotateStartDrag)
}
var rotateStartDrag = function (c) {
  if (!c.isLeftClick()) {
    return
  }
  if (rotating != null) {
    rotateEndDrag(c)
  }
  var a = c.element();
  var d = getRotatableFromGrip(a);
  var e = d.cumulativeOffset();
  var b = d.getDimensions();
  rotating = new Rotating(d, e.left + b.width / 2, e.top + b.height / 2, c.pointerX(), c.pointerY(), d.getRotateAngle());
  Event.observe(document, Event.MOUSE_MOVE, rotateDrag);
  Event.observe(document, Event.MOUSE_UP, rotateEndDrag);
  a.addClassName("rotating");
  c.stop()
}.bindAsTouchEventListener();
var rotateEndDrag = function (c) {
  var f = rotating.rotatable;
  var b = getGripFromRotatable(f);
  var d = f.hasClassName("entry");
  var e = rotating.angle(c.pointerX(), c.pointerY());
  rotateDrag(c);
  Event.stopObserving(document, Event.MOUSE_MOVE, rotateDrag);
  Event.stopObserving(document, Event.MOUSE_UP, rotateEndDrag);
  rotating = null;
  b.removeClassName("rotating");
  if (d) {
    var a = Math.round(e);
    f.setRotateAngle(a);
    rotateEntry(f, a)
  }
}.bindAsTouchEventListener();
var rotateDrag = function (a) {
  var c = rotating.rotatable;
  var b = rotating.angle(a.pointerX(), a.pointerY());
  c.setRotateAngle(b);
  a.stop()
}.bindAsTouchEventListener();
function getGripFromRotatable(a) {
  return a.$$("rotate")
}
function getRotatableFromGrip(a) {
  return a.up(".rotatable")
}
function isRotating() {
  return rotating != null
}
function installTagHandler(a) {
  a.select(".tag .tagItem").each(function (b) {
    b.observe("click", function (c) {
      toggleHilightedTag(b.tag)
    })
  })
}
function uninstallTagHandler(a) {
  a.select(".tag .tagItem").each(function (b) {
    b.stopObserving("click")
  })
}
function toggleHilightedTag(a) {
  var b = a.stringValue();
  if (g_hilightedTags.indexOf(b) != -1) {
    g_hilightedTags = g_hilightedTags.without(b)
  } else {
    g_hilightedTags.push(b)
  }
  updateEntriesForHilightedTags()
}
function clearHilightedTags() {
  g_hilightedTags = new Array();
  updateEntriesForHilightedTags()
}
function isTagsHilighted() {
  return g_hilightedTags.length != 0
}
function updateEntriesForHilightedTags() {
  $("canvas").select(".entry").each(function (a) {
    a.entry.updateForTag(g_hilightedTags)
  })
}
function ensureAllEntriesNotHidden() {
  if (!isTagsHilighted()) {
    return
  }
  var a = $("canvas").select(".entry");
  for (var c = 0; c < a.length; ++c) {
    var b = a[c].entry;
    if (!(b.maskStatus & Entry.TAG_MASKED)) {
      return
    }
  }
  clearHilightedTags()
}
function getMovableIntersectedEntries(a) {
  return getIntersectedEntries(a, function (b) {
    return b.entry.isFlag(Entry.FLAG_MOVE)
  })
}
function getIntersectedEntries(h, d) {
  var b = $("canvas").select(".entry");
  var c = [h];
  for (var a = 0; a < c.length; ++a) {
    var g = c[a];
    for (var i = 0; i < b.length; ++i) {
      var f = b[i];
      if ((!d || d(f)) && c.indexOf(f) == -1 && intersectEntry(f.entry, g.entry)) {
        c.push(f)
      }
    }
  }
  c.shift();
  return c
}
function calcEntrySize(f, h, l, k, c, g) {
  var a = $("bodySize");
  var d = c != 0 ? c - ENTRY_SIZE_MARGIN.width : 0;
  a.setStyle({width: d != 0 ? d + "px" : "auto", fontSize: k + "%", padding: g ? "0px 15px" : "0px"});
  a.update2(formatBody(f.gsub(/\r/, ""), h));
  var j = 0;
  if (d == 0) {
    d = Math.max(a.getWidth(), MIN_ENTRY_SIZE.width - ENTRY_SIZE_MARGIN.width);
    if (d > MAX_AUTO_ENTRY_SIZE.width) {
      for (var e = 2; e < 5; ++e) {
        var i = Math.ceil(d / e) + 10;
        if (i < MAX_AUTO_ENTRY_SIZE.width) {
          d = i;
          break
        }
      }
      if (d > MAX_AUTO_ENTRY_SIZE.width) {
        d = MAX_AUTO_ENTRY_SIZE.width
      }
      a.setStyle({width: d + "px"})
    }
    j = Math.max(a.getHeight(), MIN_ENTRY_SIZE.height - ENTRY_SIZE_MARGIN.height);
    if (d > j * 10) {
      d = Math.max(Math.floor(d / 3), MIN_ENTRY_SIZE.width - ENTRY_SIZE_MARGIN.width)
    }
    a.setStyle({width: d + "px"})
  }
  j = a.getHeight();
  if (l.length != 0) {
    var b = $("tagSize");
    b.update2(formatTags(l, true));
    b.setStyle({width: d + "px"});
    j += b.getHeight()
  }
  j = Math.min(MAX_AUTO_ENTRY_SIZE.height, j);
  if (g) {
    j += g + Entry.IMAGE_FRAME_MARGIN.height - ENTRY_SIZE_MARGIN.height
  } else {
    j = Math.max(j, MIN_ENTRY_SIZE.height - ENTRY_SIZE_MARGIN.height)
  }
  return new Size(Math.ceil(d) + ENTRY_SIZE_MARGIN.width, Math.ceil(j) + ENTRY_SIZE_MARGIN.height)
}
function getImageHeightToCalcSize(a) {
  if (a.image) {
    if (a.isParam(Entry.PARAM_IMAGE_FRAME)) {
      return a.image.height
    }
  } else {
    if (a.attachment) {
      return Entry.ATTACHMENT_IMAGE_HEIGHT
    }
  }
  return null
}
function getPopup() {
  if (!g_popup) {
    g_popup = new Popup($("canvasView"))
  }
  return g_popup
}
function addNormalEntry(d, e) {
  var b = d.entryDate.value.length != 0 ? parseDate(d.entryDate.value) : null;
  var a = Tag.getTags(d.entryTags ? Tag.split(d.entryTags.value) : [], b);
  var c = calcEntrySize(d.entryContent.value, d.entryIcon.value, a, d.entryFontSize.value, 0, null);
  d.entryWidth.value = c.width;
  d.entryHeight.value = c.height;
  d.entryZIndex.value = getTopZIndex() + 1;
  fillEntryPosition(d, e);
  Request.request("/api/entry/new", d, entryAdded, function (f) {
    showErrorDialog(f);
    g_dialogsStub.getDialogs().enableNewEntryDialog()
  });
  if (e.index != null) {
    g_palette.setColor(e.index, d.entryColor.value)
  }
}
function entryAdded(a) {
  var b = null;
  if (a.id) {
    b = insertEntryElem(a, true, true, function () {
      info("canvas.info.entryAdded")
    });
    g_dock.loadEntries(g_canvasId);
    if (a.date) {
      g_panel.getCalendar().refresh()
    }
  } else {
    info("canvas.info.entrySent")
  }
  g_dialogsStub.getDialogs().hideNewEntryDialog(b)
}
function addImageEntry(e, f) {
  if (e.imageType[2].checked) {
    var a = parseInt(e.entryWidth.value, 10) + Entry.IMAGE_FRAME_MARGIN.width;
    var c = e.entryDate.value.length != 0 ? parseDate(e.entryDate.value) : null;
    var b = Tag.getTags(e.entryTags ? Tag.split(e.entryTags.value) : [], c);
    var d = calcEntrySize(e.entryContent.value, 0, b, e.entryFontSize.value, a, parseInt(e.entryHeight.value, 10));
    e.entryWidth.value = a;
    e.entryHeight.value = d.height
  }
  e.entryZIndex.value = getTopZIndex() + 1;
  fillEntryPosition(e, f);
  Request.request("/api/entry/new", e, imageEntryAdded, function (g) {
    showErrorDialog(g);
    g_dialogsStub.getDialogs().enableImageEntryDialog()
  })
}
function imageEntryAdded(a) {
  var b = insertEntryElem(a, true, true, function () {
    info("canvas.info.entryAdded")
  });
  g_dialogsStub.getDialogs().hideImageEntryDialog(b);
  g_dock.loadEntries(g_canvasId)
}
function imageUploaded(a, b) {
  g_dialogsStub.getDialogs().updateImageEntryDialog(b)
}
function imageUploadFailed(a, b) {
  showErrorDialog(b);
  g_dialogsStub.getDialogs().updateImageEntryDialog(null)
}
function addMovieEntry(a, b) {
  a.entryZIndex.value = getTopZIndex() + 1;
  fillEntryPosition(a, b);
  Request.request("/api/entry/new", a, movieEntryAdded, function (c) {
    showErrorDialog(c);
    g_dialogsStub.getDialogs().enableMovieEntryDialog()
  })
}
function fillEntryPosition(a, b) {
  if (b.left == 0 && b.top == 0) {
    b.left = Math.floor((getWindowWidth() - a.entryWidth.value) / 2 + g_origin.x);
    b.top = Math.floor((getWindowHeight() - a.entryHeight.value) / 3 + g_origin.y)
  }
  a.entryLeft.value = b.left;
  a.entryTop.value = b.top
}
function movieEntryAdded(a) {
  var b = insertEntryElem(a, true, true, function () {
    info("canvas.info.entryAdded")
  });
  g_dialogsStub.getDialogs().hideMovieEntryDialog(b);
  g_dock.loadEntries(g_canvasId)
}
function addAttachmentEntry(d, e) {
  var b = d.entryDate.value.length != 0 ? parseDate(d.entryDate.value) : null;
  var a = Tag.getTags(d.entryTags ? Tag.split(d.entryTags.value) : [], b);
  var c = calcEntrySize(d.entryContent.value, 0, a, d.entryFontSize.value, parseInt(d.entryWidth.value, 10), Entry.ATTACHMENT_IMAGE_HEIGHT);
  d.entryHeight.value = c.height;
  d.entryZIndex.value = getTopZIndex() + 1;
  fillEntryPosition(d, e);
  Request.request("/api/entry/new", d, attachmentEntryAdded, function (f) {
    showErrorDialog(f);
    g_dialogsStub.getDialogs().enableAttachmentEntryDialog()
  })
}
function attachmentEntryAdded(a) {
  var b = insertEntryElem(a, true, true, function () {
    info("canvas.info.entryAdded")
  });
  g_dialogsStub.getDialogs().hideAttachmentEntryDialog(b);
  g_dock.loadEntries(g_canvasId)
}
function attachmentUploaded(a, b) {
  g_dialogsStub.getDialogs().updateAttachmentEntryDialog(b)
}
function attachmentUploadFailed(a, b) {
  showErrorDialog(b);
  g_dialogsStub.getDialogs().updateAttachmentEntryDialog(null)
}
function editEntry(g, f) {
  var d = g.entryDate.value.length != 0 ? parseDate(g.entryDate.value) : null;
  var c = Tag.getTags(g.entryTags ? Tag.split(g.entryTags.value) : [], d);
  var b = f.width;
  if (f.image && g.imageType[2].checked) {
    b = f.image.width + Entry.IMAGE_FRAME_MARGIN.width
  }
  var a = null;
  if (f.image) {
    if (g.imageType[2].checked) {
      a = f.image.height
    }
  } else {
    if (f.attachment) {
      a = Entry.ATTACHMENT_IMAGE_HEIGHT
    }
  }
  var e = calcEntrySize(g.entryContent.value, g.entryIcon ? g.entryIcon.value : 0, c, g.entryFontSize.value, b, a);
  g.entryLeft.value = f.left;
  g.entryTop.value = f.top;
  g.entryWidth.value = b;
  g.entryHeight.value = Math.max(f.height, e.height);
  Request.request("/api/entry/edit", g, entryEdited, function (h) {
    showErrorDialog(h);
    if (f.image) {
      g_dialogsStub.getDialogs().enableEditImageEntryDialog()
    } else {
      g_dialogsStub.getDialogs().enableEditEntryDialog()
    }
  })
}
function entryEdited(a) {
  var b = replaceEntryElem(a, true, function () {
    info("canvas.info.entryEdited")
  });
  ensureAllEntriesNotHidden();
  if (a.image) {
    g_dialogsStub.getDialogs().hideEditImageEntryDialog(b)
  } else {
    if (a.attachment) {
      g_dialogsStub.getDialogs().hideEditAttachmentEntryDialog(b)
    } else {
      g_dialogsStub.getDialogs().hideEditEntryDialog(b)
    }
  }
  g_dock.loadEntries(g_canvasId);
  g_panel.getCalendar().refresh()
}
function moveEntry(f, g) {
  if (!g) {
    var e = f.positionedOffset();
    g = new Point(px2lx(e.left), py2ly(e.top))
  }
  var b = ps2ls(f.getWidth());
  var a = ps2ls(f.getHeight());
  var d = f.entry;
  if (g.x == d.left && g.y == d.top && b == d.width && a == d.height) {
    return
  }
  d.left = g.x;
  d.top = g.y;
  d.width = b;
  d.height = a;
  var c = $("moveEntryForm");
  c.entryId.value = f.entry.id;
  c.entryLeft.value = g.x;
  c.entryTop.value = g.y;
  c.entryWidth.value = b;
  c.entryHeight.value = a;
  Request.request("/api/entry/move", c, entryMoved, error)
}
function entryMoved(a) {
  var b = getEntryElemFromId(a.id);
  var c = g_entries.indexOf(b.entry);
  g_entries[c] = a;
  Entry.rebind(a, b);
  a.updatePosition();
  b.show();
  a.updateBackgroundSize();
  a.adjustFont();
  g_panel.updateEntryElem(a);
  g_dock.loadEntries(g_canvasId)
}
function moveEntries(a) {
  var b = a.map(function (d) {
    var g = d.entry;
    var f = d.positionedOffset();
    g.left = px2lx(f.left);
    g.top = py2ly(f.top);
    g.width = ps2ls(d.getWidth());
    g.height = ps2ls(d.getHeight());
    return g.id + "=" + [g.left, g.top, g.width, g.height].join(" ")
  }).join(",");
  var c = $("moveEntriesForm");
  c.positions.value = b;
  Request.request("/api/entry/moves", c, entriesMoved, error)
}
function entriesMoved(a) {
  a.each(function (b) {
    var c = getEntryElemFromId(b.id);
    var d = g_entries.indexOf(c.entry);
    g_entries[d] = b;
    Entry.rebind(b, c);
    b.updatePosition();
    c.show();
    b.updateBackgroundSize();
    g_panel.updateEntryElem(b)
  });
  updateEntriesVisibility();
  g_dock.loadEntries(g_canvasId)
}
function setEntryDueDate(f, d, g, a) {
  var b = new Date(d, g, a);
  if (f.date != null && isSameDay(b, parseDate(f.date))) {
    return
  }
  var c = calcEntrySize(f.subject + "\n" + f.body, f.icon, Tag.getTags(f.tags, b), f.fontSize, f.width, getImageHeightToCalcSize(f));
  var e = $("setEntryDueDateForm");
  e.entryId.value = f.id;
  e.year.value = d;
  e.month.value = g + 1;
  e.day.value = a;
  e.entryWidth.value = f.width;
  e.entryHeight.value = Math.max(f.height, c.height);
  Request.request("/api/entry/setDueDate", e, entryDueDateSet, error)
}
function clearEntryDueDate(c) {
  var a = calcEntrySize(c.subject + "\n" + c.body, c.icon, Tag.getTags(c.tags, null), c.fontSize, c.width, getImageHeightToCalcSize(c));
  var b = $("setEntryDueDateForm");
  b.entryId.value = c.id;
  b.year.value = 0;
  b.month.value = 0;
  b.day.value = 0;
  b.entryWidth.value = c.width;
  b.entryHeight.value = Math.max(c.height, a.height);
  Request.request("/api/entry/setDueDate", b, entryDueDateSet, error)
}
function entryDueDateSet(a) {
  var b = a.entry;
  replaceEntryElem(b, false);
  ensureAllEntriesNotHidden();
  info("canvas.info.entryDueDateSet", a.undoId);
  g_dock.loadEntries(g_canvasId);
  g_panel.getCalendar().refresh()
}
function sendEntry(b, a) {
  var c = $("sendEntryForm");
  c.entryId.value = b;
  c.userId.value = a;
  Request.request("/api/entry/send", c, entrySent, error)
}
function sendEntryEx(b, d, a) {
  var c = $("sendEntryExForm");
  c.entryId.value = b;
  Request.request("/api/entry/sendEx", c, function (e) {
    d();
    entrySent(e)
  }, a)
}
function entrySent(a) {
  if (a.copy) {
    info("canvas.info.entrySent")
  } else {
    var b = function () {
      removeEntryElem(a.entryId);
      ensureAllEntriesNotHidden();
      info("canvas.info.entrySent")
    };
    var c = getEntryElemFromId(a.entryId);
    if (c.entry.isFlag(Entry.FLAG_EFFECT)) {
      hideEntryElemWithEffect(c, flickAction, b)
    } else {
      dimEntryElemFunc(c, b).delay(0.1)
    }
    g_dock.loadEntries(g_canvasId);
    g_panel.getCalendar().refresh()
  }
}
function copyEntry(b, c, d) {
  var a = $("copyEntryForm");
  a.entryId.value = b.id;
  a.canvasId.value = c;
  if (d) {
    a.entryLeft.value = d.x;
    a.entryTop.value = d.y
  }
  Request.request("/api/entry/copy", a, entryCopied, error)
}
function entryCopied(a) {
  if (a.canvasId == g_canvasId) {
    insertEntryElem(a, true, true, function () {
      info("canvas.info.entryCopied")
    })
  } else {
    info("canvas.info.entryCopied")
  }
  g_dock.loadEntries(a.canvasId)
}
function copyEntries(b, e, a) {
  var c = b.map(function (f, h) {
    var g = a[h];
    return f.entry.id + "=" + [g.x, g.y].join(" ")
  }).join(",");
  var d = $("copyEntriesForm");
  d.canvasId.value = e;
  d.positions.value = c;
  Request.request("/api/entry/copys", d, entriesCopied, error)
}
function entriesCopied(a) {
  if (a.length == 0) {
    return
  }
  var b = a[0];
  if (b.canvasId == g_canvasId) {
    for (var c = 0; c < a.length; ++c) {
      var b = a[c];
      insertEntryElem(b, true, true, c != a.length - 1 ? null : function () {
        info("canvas.info.entryCopied")
      })
    }
  } else {
    info("canvas.info.entryCopied")
  }
  g_dock.loadEntries(b.canvasId)
}
function doneEntry(b) {
  var a = $("doneEntryForm");
  a.entryId.value = b.id;
  Request.request("/api/entry/done", a, entryDone, error)
}
function entryDone(a) {
  var b = function () {
    removeEntryElem(a.entryId);
    ensureAllEntriesNotHidden();
    info("canvas.info.entryDone", a.undoId)
  };
  var c = getEntryElemFromId(a.entryId);
  if (c.entry.isFlag(Entry.FLAG_EFFECT)) {
    hideEntryElemWithEffect(c, screwUpAction, b)
  } else {
    dimEntryElemFunc(c, b).delay(0.1)
  }
  g_dock.loadEntries(g_canvasId);
  g_panel.getCalendar().refresh()
}
function lightEntryElem(c, a) {
  if (Prototype.Browser.IE && c.entry.isFlag(Entry.FLAG_SHADOW)) {
    c.select(".bg *").each(function (d) {
      if (!d.hasClassName("bgCenter")) {
        d.hide()
      }
    })
  }
  c.setStyle({opacity: 0});
  var b = function () {
    var d = Math.floor(c.getStyle("opacity") * 10);
    if (d < 10) {
      c.setStyle({opacity: (d + 1) / 10});
      b.delay(0.1)
    } else {
      if (Prototype.Browser.IE && c.entry.isFlag(Entry.FLAG_SHADOW)) {
        c.select(".bg *").each(function (e) {
          if (!e.hasClassName("bgCenter")) {
            e.show()
          }
        })
      }
      if (a) {
        a()
      }
    }
  };
  b()
}
function dimEntryElemFunc(c, a) {
  uninstallHandlers(c);
  if (Prototype.Browser.IE) {
    c.select(".bg *").each(function (d) {
      if (!d.hasClassName("bgCenter")) {
        d.hide()
      }
    })
  }
  var b = function () {
    var d = Math.floor(c.getStyle("opacity") * 10);
    if (d > 0) {
      c.setStyle({opacity: (d - 1) / 10});
      b.delay(0.1)
    } else {
      if (a) {
        a()
      }
    }
  };
  return b
}
function hideEntryElemWithEffect(c, b, a) {
  uninstallHandlers(c);
  effect(c, b, a)
}
function updateEntryPin(b, a) {
  var c = $("pinEntryForm");
  c.entryId.value = b;
  c.entryPin.value = a ? "1" : "0";
  Request.request("/api/entry/pin", c, entryPinUpdated, error)
}
function entryPinUpdated(a) {
  var b = replaceEntryElem(a, false);
  info(a.pin ? "canvas.info.entryPinned" : "canvas.info.entryUnpinned")
}
function updateEntryZIndices(a) {
  var b = a.map(function (d, e) {
    return d.id + "=" + d.zIndex
  }).join(",");
  var c = $("updateEntryZIndicesForm");
  c.zIndices.value = b;
  Request.request("/api/entry/updateZIndices", c, zIndicesUpdated, error)
}
function zIndicesUpdated() {
  g_dock.loadEntries(g_canvasId)
}
function rotateEntry(d, c) {
  var b = d.entry;
  if (c == b.angle) {
    return
  }
  b.angle = c;
  var a = $("rotateEntryForm");
  a.entryId.value = d.entry.id;
  a.entryAngle.value = c;
  Request.request("/api/entry/rotate", a, entryRotated, error)
}
function entryRotated(a) {
  var b = getEntryElemFromId(a.id);
  var c = g_entries.indexOf(b.entry);
  g_entries[c] = a;
  Entry.rebind(a, b);
  a.updatePosition();
  b.show();
  a.updateBackgroundSize();
  a.adjustFont();
  g_panel.updateEntryElem(a);
  g_dock.loadEntries(g_canvasId)
}
function transferEntry(c, b, d) {
  var a = $("transferEntryForm");
  a.canvasId.value = b;
  a.entryId.value = c.entry.id;
  a.entryLeft.value = d.x;
  a.entryTop.value = d.y;
  Request.request("/api/entry/transfer", a, entryTransferred, error)
}
function entryTransferred(a) {
  removeEntryElem(a.entryId);
  info("canvas.info.entryTransferred", a.undoId);
  ensureAllEntriesNotHidden();
  g_dock.loadEntries(a.canvasId);
  g_dock.loadEntries(g_canvasId)
}
function transferEntries(a, e, b) {
  var c = a.map(function (f, h) {
    var g = b[h];
    return f.entry.id + "=" + [g.x, g.y].join(" ")
  }).join(",");
  var d = $("transferEntriesForm");
  d.canvasId.value = e;
  d.positions.value = c;
  Request.request("/api/entry/transfers", d, entriesTransferred, error)
}
function entriesTransferred(a) {
  a.entryIds.each(removeEntryElem);
  info("canvas.info.entryTransferred");
  ensureAllEntriesNotHidden();
  g_dock.loadEntries(a.canvasId);
  g_dock.loadEntries(g_canvasId)
}
function undo(a) {
  var b = $("undoForm");
  b.undoId.value = a;
  Request.request("/api/entry/undo", b, undone, error)
}
function undone(a) {
  loadEntries();
  g_dock.loadAllEntries();
  g_panel.getCalendar().refresh();
  info(a.undone ? "canvas.info.undoSucceeded" : "canvas.info.undoFailed")
}
function updateSubscription() {
  var a = $("isSubscribedForm");
  new AjaxUtil().get("/api/canvas/subscribed", a, function (b) {
    g_palette.setSubscribed(b.subscribed)
  }, function (b) {
    g_palette.setSubscribed(false);
    error(b)
  });
  g_dock.load();
  g_panel.getCalendar().refresh()
}
function insertEntryElem(f, g, c, e) {
  var h = insertNormalEntryElem(f);
  g_panel.insertEntryElem(f);
  var b = h.$$("pin");
  if (b && f.pin) {
    b.addClassName("pushed")
  }
  if (!f.isFlag(Entry.FLAG_SHADOW)) {
    var d = ["bgLeftTop", "bgLeft", "bgLeftBottom", "bgRightTop", "bgRight", "bgRightBottom", "bgBottom"];
    d.each(function (i) {
      h.$$(i).remove()
    })
  }
  if (!f.isFlag(Entry.FLAG_LOGO) || Prototype.Browser.IE6) {
    h.select(".logo, .logoIE").each(Element.remove)
  } else {
    if (f.logo) {
      h.select(".logo, .logoIE").each(function (i) {
        i.setStyle({backgroundImage: "url(/image/logo/" + f.logo + ".png)"})
      })
    } else {
      if (f.color == "white") {
        h.$$("logo").addClassName("logoWhite");
        h.$$("logoIE").addClassName("logoWhiteIE")
      }
    }
  }
  if (!f.isFlag(Entry.FLAG_GRIP)) {
    var a = h.$$("grip");
    if (a) {
      a.setStyle({backgroundImage: "url(" + g_imageAssetBaseURL + "/image/nogrip.png)"})
    }
  }
  if (f.isFlag(Entry.FLAG_AUTOHIDE)) {
    setupAutoHide(f, h)
  }
  if (f.intimate) {
    setAutoShowPrivate(f, h)
  }
  if (g) {
    g_entries.push(f)
  }
  if (isTagsHilighted()) {
    f.updateForTag(g_hilightedTags)
  }
  f.updateVisibility($("canvasView").getDimensions());
  if (c && h.visible()) {
    lightEntryElem(h, e)
  }
  return h
}
function insertNormalEntryElem(g) {
  var b = g.image != null;
  var c = g.movie != null;
  var e = !b && g.attachment != null;
  g.maskStatus = 0;
  var h = null;
  if (b) {
    h = $("imageEntryTemplate").cloneNode(true)
  } else {
    if (c) {
      h = $("movieEntryTemplate").cloneNode(true)
    } else {
      if (e) {
        h = $("attachmentEntryTemplate").cloneNode(true)
      } else {
        h = $("entryTemplate").cloneNode(true)
      }
    }
  }
  Entry.bind(g, h);
  if (b && !g.isParam(Entry.PARAM_IMAGE_FRAME)) {
    h.select(".image, .body, .tag").each(Element.remove)
  }
  var a = g.intimate && (!(g_canvasTypeFlags & CanvasType.FLAG_PRIVATE) || !g_showPrivate);
  g.fill(a);
  if (g.intimate) {
    h.addClassName("privateEntry")
  }
  if (!g.isFlag(Entry.FLAG_MOVE)) {
    h.removeClassName("draggable");
    h.$$("handle").removeClassName("handle")
  }
  if (!c) {
    if (!g.isFlag(Entry.FLAG_RESIZE)) {
      h.removeClassName("resizable");
      h.$$("grip").remove()
    }
    if (!g.isFlag(Entry.FLAG_ROTATE)) {
      h.removeClassName("rotatable");
      h.$$("rotate").remove()
    }
    if (!g.isFlag(Entry.FLAG_EDIT)) {
      h.$$("editEntry").remove()
    }
    if (!g.isFlag(Entry.FLAG_DUE)) {
      h.$$("dueEntry").remove()
    }
    if (!g.isFlag(Entry.FLAG_DOWNLOAD)) {
      h.$$("downloadEntry").remove()
    }
  }
  if (!g.isFlag(Entry.FLAG_SEND)) {
    h.$$("sendEntry").remove()
  }
  if (!g.isFlag(Entry.FLAG_DONE)) {
    h.$$("doneEntry").remove()
  }
  if (!g.isFlag(Entry.FLAG_COPY)) {
    h.$$("copyEntry").remove()
  }
  if (!g.isFlag(Entry.FLAG_PIN)) {
    h.$$("pin").remove()
  }
  if (Prototype.Browser.IE) {
    var j = h.$$("tag");
    if (j) {
      j.setStyle({fontFamily: '"MS PGothic",Verdana,Arial,Helvetica,sans-serif'})
    }
  }
  $("canvas").appendChild(h);
  g.updatePosition();
  if (g.getTags(true).length == 0) {
    var j = h.$$("tag");
    if (j) {
      j.remove()
    }
  }
  h.show();
  if (Prototype.Browser.IE) {
    var d = h.$$("action");
    if (d) {
      var f = d.positionedOffset();
      var i = d.getOffsetParent().getDimensions();
      d.setStyle({marginBottom: (f.top + 16 - i.height) + "px"})
    }
  }
  g.updateBackgroundSize();
  g.adjustFont();
  if (a) {
    g.maskPrivate()
  }
  if (Prototype.Browser.supportsTouches) {
    installHandlers(h)
  } else {
    h.observe("mouseover", installHandlersWithEvent)
  }
  return h
}
function setupAutoHide(b, c) {
  var a = ["action", "creatorIcon"].map(function (d) {
    return c.$$(d)
  }).select(function (d) {
    return d != null
  });
  if (a.length != 0) {
    a.each(Element.hide);
    c.observe("mouseover", function () {
      if (!c.entry.resizing) {
        a.each(Element.show)
      }
    });
    c.observe("mouseout", function () {
      a.each(Element.hide)
    });
    b.updateBackgroundSize()
  }
}
function setAutoShowPrivate(a, c) {
  var b = false;
  c.observe("mouseover", function (d) {
    if (!b) {
      b = true;
      if (!g_showPrivate) {
        (function () {
          if (b) {
            a.unmaskPrivate()
          }
        }).delay(0.8)
      }
    }
  });
  c.observe("mouseout", function (d) {
    if (!Position.within(c, d.pointerX(), d.pointerY())) {
      if (g_showPrivate) {
        a.unmaskPrivate()
      } else {
        a.maskPrivate()
      }
      b = false
    }
  })
}
function installHandlers(l) {
  var j = l.entry;
  j.setupHandle();
  if (j.isFlag(Entry.FLAG_MOVE)) {
    installDragHandler(l)
  }
  if (j.isFlag(Entry.FLAG_RESIZE)) {
    installResizeHandler(l)
  }
  if (j.isFlag(Entry.FLAG_ROTATE)) {
    installRotateHandler(l)
  }
  var k = l.$$("editEntry");
  if (k) {
    k.observe("click", ActionHandlers.edit)
  }
  var i = l.$$("dueEntry");
  if (i) {
    i.observe("click", ActionHandlers.due)
  }
  var f = l.$$("sendEntry");
  if (f) {
    f.observe("click", ActionHandlers.send)
  }
  var c = l.$$("copyEntry");
  if (c) {
    c.observe("click", ActionHandlers.copy)
  }
  var b = l.$$("downloadEntry");
  if (b) {
    b.observe("click", ActionHandlers.open)
  }
  var g = l.$$("doneEntry");
  if (g) {
    g.observe("click", ActionHandlers.done)
  }
  var e = l.$$("creatorIcon");
  if (e) {
    if (j.creatorId != 0 && j.ownerId == g_userId) {
      e.observe("click", ActionHandlers.reply)
    }
  }
  var a = l.$$("pin");
  if (a) {
    a.observe("click", ActionHandlers.togglePin)
  }
  var d = l.$$$(".play span");
  if (d) {
    d.observe("click", ActionHandlers.play)
  }
  var h = l.$$("attachment");
  if (h) {
    h.select("img, .download span").each(function (m) {
      m.observe("click", ActionHandlers.download)
    })
  }
  if (j.isFlag(Entry.FLAG_ZINDEX)) {
    installZIndexHandler(l)
  }
  installTagHandler(l);
  if (Prototype.Browser.IE6) {
    if (k) {
      k.observe("mouseover", ActionHandlers.hoverEntryButton);
      k.observe("mouseout", ActionHandlers.unhoverEntryButton)
    }
    if (i) {
      i.observe("mouseover", ActionHandlers.hoverEntryButton);
      i.observe("mouseout", ActionHandlers.unhoverEntryButton)
    }
    if (f) {
      f.observe("mouseover", ActionHandlers.hoverEntryButton);
      f.observe("mouseout", ActionHandlers.unhoverEntryButton)
    }
    if (c) {
      c.observe("mouseover", ActionHandlers.hoverEntryButton);
      c.observe("mouseout", ActionHandlers.unhoverEntryButton)
    }
    if (g) {
      g.observe("mouseover", ActionHandlers.hoverEntryButton);
      g.observe("mouseout", ActionHandlers.unhoverEntryButton)
    }
    if (a) {
      a.observe("mouseover", ActionHandlers.hoverPin);
      a.observe("mouseout", ActionHandlers.unhoverPin)
    }
  }
}
function installHandlersWithEvent(a) {
  var b = getEntryElemFromChild(a.element());
  b.stopObserving("mouseover", installHandlersWithEvent);
  installHandlers(b)
}
function uninstallHandlers(l) {
  var j = l.entry;
  if (j.isFlag(Entry.FLAG_MOVE)) {
    uninstallDragHandler(l)
  }
  if (j.isFlag(Entry.FLAG_RESIZE)) {
    uninstallResizeHandler(l)
  }
  if (j.isFlag(Entry.FLAG_ROTATE)) {
    uninstallRotateHandler(l)
  }
  var k = l.$$("editEntry");
  if (k) {
    k.stopObserving("click", ActionHandlers.edit)
  }
  var i = l.$$("dueEntry");
  if (i) {
    i.stopObserving("click", ActionHandlers.due)
  }
  var f = l.$$("sendEntry");
  if (f) {
    f.stopObserving("click", ActionHandlers.send)
  }
  var c = l.$$("copyEntry");
  if (c) {
    c.stopObserving("click", ActionHandlers.copy)
  }
  var b = l.$$("downloadEntry");
  if (b) {
    b.stopObserving("click", ActionHandlers.open)
  }
  var g = l.$$("doneEntry");
  if (g) {
    g.stopObserving("click", ActionHandlers.done)
  }
  var e = l.$$("creatorIcon");
  if (e) {
    if (j.creatorId != 0 && j.ownerId == g_userId) {
      e.stopObserving("click", ActionHandlers.reply)
    }
  }
  var a = l.$$("pin");
  if (a) {
    a.stopObserving("click", ActionHandlers.togglePin)
  }
  var d = l.$$$(".play span");
  if (d) {
    d.stopObserving("click", ActionHandlers.play)
  }
  var h = l.$$("attachment");
  if (h) {
    h.select("img, .download").each(function (m) {
      m.stopObserving("click", ActionHandlers.download)
    })
  }
  if (j.isFlag(Entry.FLAG_ZINDEX)) {
    uninstallZIndexHandler(l)
  }
  uninstallTagHandler(l)
}
var ActionHandlers = {
  edit: function (b) {
    var c = getEntryElemFromChild(b.element());
    var a = c.entry;
    g_dialogsStub.bind(function (d) {
      if (a.image) {
        d.showEditImageEntryDialog(a, c)
      } else {
        if (a.attachment) {
          d.showEditAttachmentEntryDialog(a, c)
        } else {
          d.showEditEntryDialog(a, c)
        }
      }
    })
  }, due: function (c) {
    var b = getEntryElemFromChild(c.element()).entry;
    var a = b.date ? parseDate(b.date) : null;
    getPopup().showDueCalendar(a, c.pointerX(), c.pointerY(), function (g, e, f) {
      if (!Object.isUndefined(g) && !Object.isUndefined(e) && !Object.isUndefined(f)) {
        setEntryDueDate(b, g, e, f)
      } else {
        clearEntryDueDate(b)
      }
    })
  }, send: function (b) {
    var a = getEntryElemFromChild(b.element()).entry;
    if (g_hasBuddies) {
      getPopup().showUserList(a, b.pointerX(), b.pointerY())
    } else {
      g_dialogsStub.bind(function (c) {
        c.showSendEntryDialog(a)
      })
    }
  }, copy: function (b) {
    var a = getEntryElemFromChild(b.element()).entry;
    getPopup().showCanvasList(a, b.pointerX(), b.pointerY())
  }, download: function (b) {
    if (g_userId == 0) {
      if (confirm(MESSAGES["confirm.downloadAttachment"])) {
        openAtTop("/session/top?redirect=" + encodeURIComponent(document.URL))
      }
    } else {
      var a = getEntryElemFromChild(b.element()).entry;
      document.location.href = "/entry/attachment/" + a.attachment.id + "/" + encodeURIComponent(a.attachment.name)
    }
  }, open: function (b) {
    if (g_userId == 0) {
      if (confirm(MESSAGES["confirm.openImage"])) {
        openAtTop("/session/top?redirect=" + encodeURIComponent(document.URL))
      }
    } else {
      var a = getEntryElemFromChild(b.element()).entry;
      window.open("/entry/attachment/" + a.attachment.id + "/" + encodeURIComponent(a.attachment.name) + "?inline=1")
    }
  }, done: function (a) {
    doneEntry(getEntryElemFromChild(a.element()).entry)
  }, reply: function (c) {
    var b = getEntryElemFromChild(c.element()).entry;
    if (c.shiftKey) {
      var a = b.canReplyInSameColor() ? b.color : "blue";
      var d = new NewEntry(a, false);
      d.sendTo = b.creator;
      g_dialogsStub.bind(function (e) {
        e.showNewEntryDialog(d)
      });
      c.stop()
    }
  }, hoverEntryButton: function (b) {
    var a = b.element();
    var d = a.classNames().grep(/.+Entry/)[0];
    a.addClassName(d + "Hover")
  }, unhoverEntryButton: function (b) {
    var a = b.element();
    var d = a.classNames().grep(/.+EntryHover/)[0];
    a.removeClassName(d)
  }, togglePin: function (b) {
    var a = getEntryElemFromChild(b.element()).entry;
    updateEntryPin(a.id, !a.pin)
  }, hoverPin: function (a) {
    var b = getEntryElemFromChild(a.element());
    b.$$("pin").addClassName(b.entry.pin ? "pinPushedHover" : "pinHover")
  }, unhoverPin: function (a) {
    var b = getEntryElemFromChild(a.element());
    b.$$("pin").removeClassName(b.entry.pin ? "pinPushedHover" : "pinHover")
  }, play: function (a) {
    startMovie(getEntryElemFromChild(a.element()).entry)
  }
};
function getEntryElemFromChild(a) {
  return a.hasClassName("entry") ? a : a.up(".entry")
}
function removeEntryElem(b, a) {
  var c = getEntryElemFromId(b);
  uninstallHandlers(c);
  c.remove();
  g_panel.removeEntryElem(b);
  g_entries.splice(g_entries.indexOf(c.entry), 1);
  if (g_newEntryMode && !a) {
    var d = g_newEntryElems.indexOf(c);
    if (d != -1) {
      if (g_currentNewEntryElem == c) {
        nextNewEntry()
      }
      g_newEntryElems.splice(d, 1)
    }
  }
}
function replaceEntryElem(e, b, d) {
  var a = 0;
  var h = -1;
  var f = false;
  if (g_newEntryMode) {
    var c = getEntryElemFromId(e.id);
    a = c.entry.newIndex;
    h = g_newEntryElems.indexOf(c);
    f = c == g_currentNewEntryElem
  }
  removeEntryElem(e.id, true);
  var g = insertEntryElem(e, true, b, d);
  if (a) {
    e.markNew(a);
    g_newEntryElems[h] = g;
    if (f) {
      g_currentNewEntryElem = g
    }
  }
  return g
}
function stopEventHandler(a) {
  a.stopPropagation()
}
function startMovie(a) {
  leaveNewEntryMode();
  var b = $("movie");
  if (!b) {
    b = new Element("iframe", {id: "movie", frameBorder: "0", scrolling: "no", allowfullscreen: "allowfullscreen"});
    $("movieContainer").appendChild(b)
  }
  b.src = "/entry/movie/" + a.movie.id;
  layoutMovie(a);
  b.show();
  g_movieNotification.showInformation();
  g_movieEntry = a
}
function stopMovie() {
  if (!g_movieEntry) {
    return
  }
  var a = $("movie");
  a.hide();
  a.src = g_assetBaseURL + "/resource/empty.html";
  g_movieNotification.hide();
  g_movieEntry = null
}
function showMovie() {
  --g_hideMovie;
  if (g_hideMovie == 0) {
    if (g_movieEntry) {
      layoutMovie(g_movieEntry)
    }
    $("movieContainer").setStyle({visibility: "visible"})
  }
}
function hideMovie() {
  if (g_hideMovie == 0) {
    $("movieContainer").setStyle({visibility: "hidden"})
  }
  ++g_hideMovie
}
function layoutMovie(a) {
  var b = $("movie");
  b.setStyle({
    left: (a.left + 6) + "px",
    top: a.top + "px",
    width: (a.width - 14) + "px",
    height: (a.height - 8) + "px"
  })
}
function toggleShowPrivate() {
  setShowPrivate(!g_showPrivate)
}
function setShowPrivate(a) {
  g_showPrivate = a;
  $("canvas").select(".privateEntry").each(function (b) {
    if (g_showPrivate) {
      b.entry.unmaskPrivate()
    } else {
      b.entry.maskPrivate()
    }
  });
  saveLocalPreferences();
  g_palette.updateShowPrivate()
}
function toggleNewEntryMode() {
  if (g_newEntryMode) {
    leaveNewEntryMode()
  } else {
    enterNewEntryMode()
  }
}
function enterNewEntryMode() {
  if (g_newEntryMode) {
    return
  }
  g_newEntryMode = true;
  stopMovie();
  var a = g_entries.clone();
  a.sort(function (h, g) {
    var f = h.getUpdated().getTime();
    var e = g.getUpdated().getTime();
    return f < e ? 1 : f > e ? -1 : g.id - h.id
  });
  var b = [];
  for (var d = 0; d < Math.min(10, a.length); ++d) {
    var c = a[d];
    b.push(c.elem);
    c.markNew(d + 1)
  }
  for (; d < a.length; ++d) {
    a[d].markNew()
  }
  g_newEntryElems = b;
  $("newEntryShadow").show();
  if (b.length != 0) {
    setCurrentNewEntryElem(b[0])
  }
  g_palette.updateNewEntryMode()
}
function leaveNewEntryMode() {
  if (!g_newEntryMode) {
    return
  }
  g_newEntryMode = false;
  g_newEntryElems = [];
  setCurrentNewEntryElem(null);
  g_entries.each(function (a) {
    a.unmarkNew()
  });
  $("newEntryShadow").hide();
  g_palette.updateNewEntryMode()
}
function nextNewEntry() {
  var a = g_newEntryElems.indexOf(g_currentNewEntryElem);
  if (a != -1 && a + 1 < g_newEntryElems.length) {
    setCurrentNewEntryElem(g_newEntryElems[a + 1])
  } else {
    leaveNewEntryMode()
  }
}
function previousNewEntry() {
  var a = g_newEntryElems.indexOf(g_currentNewEntryElem);
  if (a != -1 && a != 0) {
    setCurrentNewEntryElem(g_newEntryElems[a - 1])
  } else {
    leaveNewEntryMode()
  }
}
function setCurrentNewEntryElem(b) {
  var a = g_currentNewEntryHilight ? g_currentNewEntryElem.entry : null;
  g_currentNewEntryElem = b;
  g_currentNewEntryHilight = b != null;
  if (a) {
    a.updateZIndex()
  }
  if (b) {
    ensureEntryElemVisible(b);
    b.entry.updateZIndex()
  }
}
function stopHilightCurrentNewEntryElem() {
  if (g_newEntryMode && g_currentNewEntryHilight) {
    g_currentNewEntryHilight = false;
    g_currentNewEntryElem.entry.updateZIndex()
  }
}
function getSearch() {
  if (!g_search) {
    g_search = new Search($("canvasView"))
  }
  return g_search
}
function getEntryElemFromId(a) {
  return $("entry" + a)
}
function isEntryInCanvasView(f, c) {
  var e = lx2px(c.left) - g_origin.x;
  var d = ly2py(c.top) - g_origin.y;
  var b = e + ls2ps(c.width);
  var a = d + ls2ps(c.height);
  return !(d > f.height || a < 0 || e > f.width || b < 0)
}
function updateEntriesVisibility() {
  var a = $("canvasView").getDimensions();
  $("canvas").select(".entry").each(function (b) {
    b.entry.updateVisibility(a)
  })
}
function lx2px(a) {
  return Math.floor(a * g_zoom)
}
function ly2py(a) {
  return Math.floor(a * g_zoom)
}
function px2lx(a) {
  return Math.floor(a / g_zoom)
}
function py2ly(a) {
  return Math.floor(a / g_zoom)
}
function ls2ps(a) {
  return g_zoom < 1 ? Math.floor(a * g_zoom) : a
}
function ps2ls(a) {
  return g_zoom < 1 ? Math.floor(a / g_zoom) : a
}
function formatUpdated(a) {
  return formatYearMonthDate(a.getFullYear(), a.getMonth(), a.getDate()) + " " + format2(a.getHours()) + ":" + format2(a.getMinutes())
}
function format2(b) {
  var a = b.toString();
  if (a.length == 1) {
    a = "0" + a
  }
  return a
}
function formatTags(a, b) {
  if (b) {
    var c = document.createDocumentFragment();
    a.each(function (d) {
      var f = new Element("span");
      var h = "tagItem";
      var e = d.flag;
      if (e & TF_TODAY) {
        h += " tagItemToday"
      } else {
        if (e & TF_TOMORROW) {
          h += " tagItemTomorrow"
        } else {
          if (e & TF_OVERDUE) {
            h += " tagItemOverDue"
          } else {
            if (e & TF_DUE) {
              h += " tagItemDue"
            }
          }
        }
      }
      f.addClassName(h);
      f.addClassName("nodrag");
      f.appendChild(document.createTextNode("["));
      if (e & TF_DUE) {
        var g = new Element("img", {src: g_imageAssetBaseURL + "/image/duetag.png"});
        g.addClassName("dueTag");
        f.appendChild(g)
      }
      f.appendChild(document.createTextNode(d.tag + "]"));
      f.tag = d;
      c.appendChild(f);
      c.appendChild(document.createTextNode(" "))
    });
    return c
  } else {
    return a.map(function (d) {
      return d.tag
    }).join(" ")
  }
}
function formatBody(a, e) {
  var d = "";
  if (e != 0) {
    d += '<div class="icon" style="background-position:-' + Entry.getIconIndex(e) * 32 + 'px 0px;"></div>'
  }
  var c = a.split("\n");
  var b = c.length;
  for (; b > 0; --b) {
    if (c[b - 1].length != 0) {
      break
    }
  }
  for (var f = 0; f < b; ++f) {
    d += formatLine(c[f])
  }
  return d
}
function formatLine(a) {
  if (a.match(/^\s*$/)) {
    return '<p><span class="nodrag">&nbsp;</span></p>'
  } else {
    var b = '<p><span class="nodrag">';
    var d = 0;
    while (d < a.length && a.charAt(d) == " ") {
      b += "&nbsp;";
      ++d
    }
    if (d != 0) {
      a = a.substring(d)
    }
    var c = a.split(/((?:https?:\/\/(?:\w+\.)?linoit.com\/(?:canvas|group)\/[^\/]+\/?[^ <>\",;]*)|(?:https?:\/\/(?:\w+\.)?linoit.com\/(?:users|groups)\/[^\/]+\/canvases\/[^ ?#<>\",;]+(?:[?#][^ <>\",;]*)?)|(?:https?:\/\/(?:\w+\.)?linoit.com\/(?:canvas|group)\/[^ \/]+\/?)|(?:https?:\/\/(?:\w+\.)?linoit.com\/(?:users|groups)\/[^ \/]+\/?)|(?:https?:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,%#]+[A-Za-z0-9_\/])|(?:\[[^\]]+\]\(https?:\/\/[^ \)]*(?: ".*")?\))|(?:\[https?:\/\/[^\]]+\]\(\)))/);
    for (var d = 0; d < c.length; d += 2) {
      b += c[d].escapeHTML();
      if (d + 1 < c.length) {
        b += formatLink(c[d + 1])
      }
    }
    b += "</span></p>";
    return b
  }
}
function formatLink(b) {
  var i = b.match(/\[([^\]]+)\]\(([^ \)]*)(?: "(.*)")?\)/);
  if (i) {
    var k = RegExp.$1;
    var l = RegExp.$2;
    if (l.length == 0) {
      l = RegExp.$1
    }
    var j = RegExp.$3;
    return '<a href="' + l + '" ' + (j.length != 0 ? 'title="' + j + '" ' : "") + 'target="_blank">' + k + "</a>"
  } else {
    var l = b.escapeHTML();
    var f = b.match(/^https?:\/\/(?:\w+\.)?linoit.com\//);
    var m = l;
    var e = false;
    if (f) {
      if (b.match(/^(https?:\/\/(?:\w+\.)?linoit.com\/)(canvas|group)\/([^\/]+)\/([^ ?#<>\",;]+)([?#][^ <>\",;]*)?$/) || b.match(/^(https?:\/\/(?:\w+\.)?linoit.com\/)(users|groups)\/([^\/]+)\/canvases\/([^ ?#<>\",;]+)([?#][^ <>\",;]*)?$/)) {
        var c = RegExp.$1;
        var h = RegExp.$2 == "canvas" || RegExp.$2 == "users" ? "users" : "groups";
        var g = RegExp.$3;
        var a = RegExp.$4;
        var d = RegExp.$5;
        if (a == "inbox") {
          m = MESSAGES["canvas.inbox"]
        } else {
          m = decodeURIComponent(a).escapeHTML()
        }
        l = (c + h + "/" + escapeName(g) + "/canvases/" + escapeName(a) + d).escapeHTML();
        e = true
      } else {
        if (b.match(/^(https?:\/\/(?:\w+\.)?linoit.com\/)(canvas|group)\/([^ \/]+)\/?$/) || b.match(/^(https?:\/\/(?:\w+\.)?linoit.com\/)(users|groups)\/([^ \/]+)\/?$/)) {
          var c = RegExp.$1;
          var h = RegExp.$2 == "canvas" || RegExp.$2 == "users" ? "users" : "groups";
          var a = RegExp.$3;
          m = decodeURIComponent(a).escapeHTML();
          l = (c + h + "/" + escapeName(a)).escapeHTML();
          e = true
        }
      }
    }
    var n = '<a href="' + l + '"' + (f ? "" : ' target="_blank"') + ">" + m + "</a>";
    if (e) {
      n += '<a href="' + l + '"' + (f ? "" : ' target="_blank"') + ' class="canvasLink"><img src="' + g_imageAssetBaseURL.escapeHTML() + '/image/link.png" /></a>'
    }
    return n
  }
}
function escapeName(a) {
  return a.split("%").map(encodeURIComponent).join("%")
}
function canvasPath(a) {
  if (a.group) {
    return "/group/" + encodeURIComponent(a.group) + "/" + encodeURIComponent(a.name)
  } else {
    return "/canvas/" + a.owner + "/" + encodeURIComponent(a.name)
  }
}
var Request = {
  ongoingRequest: 0, request: function (d, c, b, a) {
    ++this.ongoingRequest;
    new AjaxUtil().post(d, c, function (e) {
      b(e);
      --this.ongoingRequest
    }.bind(this), function (e) {
      a(e);
      --this.ongoingRequest
    }.bind(this))
  }, requestOngoing: function () {
    return this.ongoingRequest != 0
  }
};
function openAtTop(a) {
  if (g_inner) {
    window.open(a)
  } else {
    document.location.href = a
  }
}
function info(b, a) {
  g_notification.showInformation(MESSAGES[b], a)
}
function error(a) {
  g_notification.showError(getErrorString(a))
}
var Entry = {
  PRIVATE_MASKED: 1,
  TAG_MASKED: 2,
  PARAM_IMAGE_NOSHADOW: 1,
  PARAM_IMAGE_FRAME: 2,
  FLAG_DUE: 1,
  FLAG_TAG: 2,
  FLAG_SEND: 4,
  FLAG_MOVE: 8,
  FLAG_RESIZE: 16,
  FLAG_ZINDEX: 32,
  FLAG_EDIT: 64,
  FLAG_EDITMOBILE: 128,
  FLAG_DONE: 256,
  FLAG_COPY: 512,
  FLAG_DOWNLOAD: 1024,
  FLAG_FONT: 2048,
  FLAG_ICON: 4096,
  FLAG_PRIVATE: 8192,
  FLAG_DOCK: 16384,
  FLAG_PANEL: 32768,
  FLAG_AUTOHIDE: 65536,
  FLAG_GRIP: 131072,
  FLAG_LOGO: 262144,
  FLAG_SHADOW: 524288,
  FLAG_CREATOR: 1048576,
  FLAG_EFFECT: 2097152,
  FLAG_PIN: 4194304,
  FLAG_ADJUSTFONT: 8388608,
  FLAG_ROTATE: 16777216,
  IMAGE_FRAME_MARGIN: new Size(52, 60),
  IMAGE_FRAME_PADDING: 15,
  ATTACHMENT_IMAGE_HEIGHT: 48 - 10,
  prebind: function (a) {
    Object.extend(a, Entry.Methods)
  },
  bind: function (b, c) {
    Object.extend(b, Entry.Methods);
    b.elem = c;
    c.entry = b;
    b.setId();
    if (b.image) {
      if (b.isParam(Entry.PARAM_IMAGE_FRAME)) {
        var a = b.image.width + Entry.IMAGE_FRAME_MARGIN.width;
        c.minSize = new Size(a, b.image.height + Entry.IMAGE_FRAME_MARGIN.height);
        c.maxSize = new Size(a, null)
      }
    } else {
      if (b.attachment) {
        c.minSize = new Size(MIN_ENTRY_SIZE.width, MIN_ENTRY_SIZE.height + Entry.ATTACHMENT_IMAGE_HEIGHT)
      }
    }
  },
  rebind: function (b, c) {
    var a = c.entry;
    b.maskStatus = a.maskStatus;
    b.newIndex = a.newIndex;
    Entry.bind(b, c)
  },
  compareZIndex: function (a, b) {
    return a.zIndex - b.zIndex
  },
  getSolidColor: function (a) {
    switch (a) {
      case"blue":
        return "#a6dbff";
      case"yellow":
        return "#fff293";
      case"green":
        return "#c4ffd8";
      case"purple":
        return "#d3cdfc";
      case"red":
        return "#ffa09b";
      case"grass":
        return "#eafc86";
      case"gray":
        return "#e4e4e4";
      case"orange":
        return "#ffdb9b";
      case"peach":
        return "#ffc7c4";
      case"pink":
        return "#edbbff";
      case"white":
        return "#ffffff";
      case"black":
        return "#111111";
      default:
        return "white"
    }
  },
  getIconIndex: function (a) {
    if (a == 1) {
      return 0
    } else {
      if (100 <= a && a < 110) {
        return a - 100 + 1
      } else {
        if (201 <= a && a < 1000) {
          return a - 200 + 10
        } else {
          return a - 1000 + 46
        }
      }
    }
  }
};
Entry.Methods = {
  getContent: function () {
    return this.body.length != 0 ? this.subject + "\n" + this.body : this.subject
  }, fill: function (f, e) {
    e = e || this.elem;
    if (this.image && this.isParam(Entry.PARAM_IMAGE_FRAME)) {
      e.$$("image").setStyle({
        width: this.image.width + "px",
        height: this.image.height + "px",
        backgroundImage: "url(/entry/image/" + this.image.id + ")"
      })
    }
    var j = e.$$("body");
    if (j) {
      j.setStyle({fontSize: this.fontSize + "%", color: this.fontColor});
      j.update2(formatBody(this.subject + "\n" + this.body, this.icon))
    }
    var k = e.$$("tag");
    if (k) {
      k.$$("value").update2(formatTags(this.getTags(true), true))
    }
    if (this.movie) {
      var g = e.$$$(".thumbnail img");
      g.src = "/entry/movieThumbnail?movieId=" + this.movie.id;
      g.setStyle({width: this.movie.thumbnailWidth + "px", height: this.movie.thumbnailHeight + "px"});
      g.up().setStyle({height: this.movie.thumbnailHeight + "px"});
      function c(b) {
        return b > 60 * 60 ? Math.floor(b / (60 * 60)) + ":" + show2(Math.floor((b % 3600) / 60)) + ":" + show2(b % 60) : Math.floor(b / 60) + ":" + show2(b % 60)
      }

      e.$$("information").innerHTML = "<div>" + (this.movie.title ? this.movie.title.escapeHTML() : "") + "</div><div>" + (this.movie.duration ? "(" + c(this.movie.duration) + ")" : "") + "</div>"
    }
    if (!this.image && this.attachment) {
      var h = e.$$$(".attachment .attachmentIcon");
      h.src = "/entry/attachmentIcon/" + encodeURIComponent(this.attachment.contentType);
      var a = e.$$$(".attachment .name span");
      a.innerHTML = this.attachment.name.escapeHTML();
      var l = e.$$$(".attachment .size");
      l.innerHTML = this.attachment.size >= 1024 * 1024 ? Math.ceil(this.attachment.size / 1024 / 1024 * 10) / 10 + "MB" : Math.ceil(this.attachment.size / 1024) + "KB"
    }
    var d = e.$$("creatorIcon");
    if (d) {
      if (this.isFlag(Entry.FLAG_CREATOR)) {
        if (this.creatorId == g_groupOwnerId) {
          d.href = "/group/" + g_groupName
        } else {
          if (this.creatorId != 0) {
            d.href = "/canvas/" + this.creator
          }
        }
        var h = null;
        var a = null;
        if (this.creatorId == g_groupOwnerId) {
          h = "/grouphelper/icon/" + g_groupId;
          a = g_groupName
        } else {
          h = "/user/icon/" + this.creatorId;
          a = this.creator
        }
        d.setStyle({backgroundImage: "url(" + h + "?size=small)"});
        d.title = a + " (" + formatUpdated(this.getUpdated()) + ")"
      } else {
        d.remove()
      }
    }
    e.setRotateAngle(this.angle);
    this.updateBackgroundColor(f ? TYPE_MASKED : TYPE_NORMAL, e)
  }, updatePosition: function () {
    this.elem.setStyle({
      left: lx2px(this.left) + "px",
      top: ly2py(this.top) + "px",
      width: ls2ps(this.width) + "px",
      height: ls2ps(this.height) + "px",
      zIndex: this.getActualZIndex()
    })
  }, canReplyInSameColor: function () {
    return !this.image && !this.movie && this.color != "transparent"
  }, setupHandle: function () {
    if (Prototype.Browser.IE) {
      var a = this.elem.$$("handle");
      if (a) {
        a.removeClassName("handle");
        this.elem.$$("bg").addClassName("handle")
      }
    }
  }, updateBackgroundColor: function (a, b) {
    b = b || this.elem;
    if (this.image && !this.isParam(Entry.PARAM_IMAGE_FRAME) && a != TYPE_MASKED) {
      b.$$("bgCenter").src = "/entry/image/" + this.image.id
    } else {
      updateEntryBackgroundColor(b, a, this.color)
    }
  }, updateBackgroundSize: function (a) {
    updateEntryBackgroundSize(a || this.elem, this)
  }, setZIndex: function (a) {
    this.zIndex = a;
    if (this.elem) {
      this.elem.setStyle({zIndex: this.getActualZIndex()})
    }
    g_panel.setZIndex(this.id, a)
  }, updateZIndex: function () {
    this.setZIndex(this.zIndex)
  }, maskPrivate: function () {
    this.maskFlag(Entry.PRIVATE_MASKED)
  }, unmaskPrivate: function () {
    this.unmaskFlag(Entry.PRIVATE_MASKED)
  }, markNew: function (b) {
    if (b) {
      this.newIndex = b;
      var a = new Element("div");
      a.setStyle({backgroundPosition: -((b - 1) * 32) + "px 0px"});
      a.addClassName("newEntryIcon");
      this.elem.appendChild(a);
      this.updateZIndex()
    } else {
      this.newIndex = 0;
      g_panel.hideEntryElem(this.id)
    }
  }, unmarkNew: function () {
    var a = this.newIndex;
    this.newIndex = null;
    if (a) {
      this.elem.$$("newEntryIcon").remove();
      this.updateZIndex()
    } else {
      g_panel.showEntryElem(this.id)
    }
  }, maskFlag: function (a) {
    var b = this.maskStatus != 0;
    this.maskStatus |= a;
    if (!b) {
      this.mask()
    }
  }, unmaskFlag: function (a) {
    var b = this.maskStatus != 0;
    this.maskStatus &= ~a;
    if (b && this.maskStatus == 0) {
      this.unmask()
    }
  }, mask: function () {
    this.elem.select(".content, .grip, .footer").each(Element.hide);
    this.updateBackgroundColor(TYPE_MASKED);
    g_panel.maskEntryElem(this.id)
  }, unmask: function () {
    this.elem.select(".content, .grip" + (g_zoom >= 1 ? ", .footer" : "")).each(Element.show);
    this.updateBackgroundColor(TYPE_NORMAL);
    g_panel.unmaskEntryElem(this.id)
  }, getTags: function (a) {
    return Tag.getTags(this.tags, a ? this.date ? parseDate(this.date) : null : null)
  }, isParam: function (a) {
    return (this.params & a) != 0
  }, isFlag: function (a) {
    return (this.flags & a) != 0
  }, updateForTag: function (c) {
    var a = true;
    var b = this.getTags(true).map(function (e) {
      return e.stringValue()
    });
    if (b.length != 0) {
      for (var d = 0; d < c.length && a; ++d) {
        a = b.indexOf(c[d]) != -1
      }
    } else {
      a = c.length == 0
    }
    if (a) {
      this.unmaskFlag(Entry.TAG_MASKED);
      this.elem.select(".tag .tagItem").each(function (e) {
        var f = c.indexOf(e.tag.stringValue()) != -1;
        if (f) {
          e.addClassName("selected")
        } else {
          e.removeClassName("selected")
        }
      })
    } else {
      this.maskFlag(Entry.TAG_MASKED)
    }
  }, updateVisibility: function (b) {
    var a = this.elem.visible();
    if (isEntryInCanvasView(b, this)) {
      if (!a) {
        this.elem.show()
      }
    } else {
      if (a) {
        this.elem.hide()
      }
    }
  }, adjustFont: function () {
    if (this.isFlag(Entry.FLAG_ADJUSTFONT)) {
      var c = this.elem;
      var a = c.$$("body");
      a.setStyle({fontSize: this.fontSize + "%"});
      var b = function (j) {
        var i = a.getHeight();
        if (i > 0) {
          var g = c.$$("content");
          var f = g.getHeight();
          if (a.positionedOffset().top + i > g.positionedOffset().top + f) {
            if (this.image) {
              f -= this.image.height + Entry.IMAGE_FRAME_PADDING * 2
            }
            var d = parseInt(a.getStyle("fontSize"), 10);
            var e = Math.sqrt(f / i) * 0.95;
            var h = Math.max(50, Math.floor(d * e / 10) * 10);
            a.setStyle({fontSize: h + "%"})
          }
        }
        if (!j) {
          c.show()
        }
      }.bind(this);
      if (Prototype.Browser.IE) {
        this.elem.hide();
        b.defer()
      } else {
        b(true)
      }
    }
  }, getUpdated: function () {
    if (!this.updatedDate) {
      this.updatedDate = parseDateTime(this.updated)
    }
    return this.updatedDate
  }, getActualZIndex: function () {
    var a = this.zIndex;
    if (this.newIndex) {
      a += NEW_ENTRY_MODE_ZINDEX;
      if (g_currentNewEntryHilight && g_currentNewEntryElem == this.elem) {
        a += 200
      }
    }
    return a
  }, setId: function () {
    this.elem.setAttribute("id", "entry" + this.id)
  }, equals: function (a) {
    return this.id == a.id && this.updated == a.updated && this.pin == a.pin && this.left == a.left && this.top == a.top && this.width == a.width && this.height == a.height && this.angle == a.angle
  }
};
var TF_NORMAL = 0;
var TF_DUE = 1;
var TF_TODAY = 2;
var TF_TOMORROW = 4;
var TF_YESTERDAY = 8;
var TF_OVERDUE = 16;
var Tag = Class.create({
  initialize: function (a, b) {
    this.tag = a;
    this.flag = b
  }, equals: function (a) {
    return this.tag == a.tag && this.flag == a.flag
  }, stringValue: function () {
    return this.tag + "$$$" + this.flag
  }
});
Tag.split = function (a) {
  a = a.strip();
  return a.length != 0 ? a.split(/\s/) : []
};
Tag.parse = function (a) {
  return Tag.split(a).map(function (b) {
    return new Tag(b, TF_NORMAL)
  })
};
Tag.getTags = function (c, b) {
  var a = new Array();
  if (b) {
    a.push(Tag.getDateTag(b))
  }
  if (c) {
    a = a.concat(c.map(function (d) {
      return new Tag(d, TF_NORMAL)
    }))
  }
  return a
};
Tag.getDateTag = function (c) {
  var a = TF_DUE;
  var b = getTodayDate();
  if (isSameDay(c, b)) {
    a |= TF_TODAY
  } else {
    if (isSameDay(c, new Date(b.getTime() + 24 * 60 * 60 * 1000))) {
      a |= TF_TOMORROW
    } else {
      if (c.getTime() < b.getTime()) {
        a |= TF_OVERDUE
      }
    }
  }
  return new Tag(formatDueDate(c), a)
};
var TYPE_NORMAL = "normal";
var TYPE_MASKED = "masked";
var TYPE_DIALOG = "dialog";
function updateEntryBackgroundColor(d, b, a) {
  var c = b != TYPE_DIALOG ? b : TYPE_NORMAL;
  d.$$("bgCenter").src = g_imageAssetBaseURL + "/image/entry/" + c + "/" + a + ".png";
  if (Prototype.Browser.IE6) {
    if (a == "transparent") {
      d.$$("bgCenter").setOpacity(b == TYPE_DIALOG ? 0.2 : 0.1)
    }
  }
}
var LEFTSHADOW_WIDTH = 6;
var RIGHTSHADOW_WIDTH = 8;
var TOPSHADOW_HIGHT = 8;
var BOTTOMSHADOW_HIGHT = 8;
function updateEntryBackgroundSize(g, f) {
  var b = f ? ls2ps(f.width) : g.getWidth();
  var i = f ? ls2ps(f.height) : g.getHeight();
  if (b == 0 || i == 0) {
    return
  }
  g.$$("bgCenter").setStyle({
    width: (b - LEFTSHADOW_WIDTH - RIGHTSHADOW_WIDTH) + "px",
    height: (i - BOTTOMSHADOW_HIGHT) + "px"
  });
  var a = g.$$("bgBottom");
  if (a) {
    a.setStyle({width: (b - LEFTSHADOW_WIDTH - RIGHTSHADOW_WIDTH) + "px"})
  }
  var c = g.$$("bgLeft");
  if (c) {
    c.setStyle({height: (i - TOPSHADOW_HIGHT - BOTTOMSHADOW_HIGHT) + "px"})
  }
  var h = g.$$("bgRight");
  if (h) {
    h.setStyle({height: (i - TOPSHADOW_HIGHT - BOTTOMSHADOW_HIGHT) + "px"})
  }
  updateContentSize(g, b, i);
  if (Prototype.Browser.IE) {
    var e = g.$$("logoIE");
    if (e) {
      e.setStyle({width: (b - LEFTSHADOW_WIDTH - RIGHTSHADOW_WIDTH) + "px", height: (i - BOTTOMSHADOW_HIGHT) + "px"});
      if (!e.visible()) {
        e.show()
      }
      var d = g.$$("logo");
      if (d.visible()) {
        d.hide()
      }
    }
  } else {
    var d = g.$$("logo");
    if (d) {
      d.setStyle({width: (b - LEFTSHADOW_WIDTH - RIGHTSHADOW_WIDTH) + "px", height: (i - BOTTOMSHADOW_HIGHT) + "px"})
    }
  }
}
var CONTENT_MARGIN = 22;
function updateContentSize(e, b, a) {
  var c = e.$$("content");
  var f = c ? c.next(".footer") : e.$$("footer");
  var d = 0;
  if (f) {
    f.setStyle({width: (b - CONTENT_MARGIN) + "px"});
    if (f.visible() && (!e.entry || !e.entry.isFlag(Entry.FLAG_AUTOHIDE))) {
      d = f.getHeight() + 5
    }
  }
  if (c) {
    c.setStyle({height: Math.max(0, (a - d - 13)) + "px"})
  }
}
function intersectEntry(i, g) {
  var d = i.left + LEFTSHADOW_WIDTH;
  var f = i.top;
  var b = i.left + i.width - RIGHTSHADOW_WIDTH;
  var j = i.top + i.height - BOTTOMSHADOW_HIGHT;
  var c = g.left + LEFTSHADOW_WIDTH;
  var e = g.top;
  var a = g.left + g.width - RIGHTSHADOW_WIDTH;
  var h = g.top + g.height - BOTTOMSHADOW_HIGHT;
  return !(b < c || a < d || j < e || h < f)
}
var MiniCalendar = Class.create({
  elem: null,
  clickFunc: null,
  dayFunc: null,
  current: null,
  year: null,
  month: null,
  initialize: function (e, a, b) {
    this.elem = e;
    this.clickFunc = a;
    this.dayFunc = b;
    this.current = null;
    var d = e.$$("miniCalendarBar");
    if (Prototype.Browser.IE) {
      var f = e.$$("miniCalendar");
      f.observe("resize", function () {
        d.setStyle({width: (f.getWidth()) + "px"})
      })
    }
    d.$$("miniCalendarPrev").observe("click", function () {
      var c = this.year;
      var g = this.month;
      if (g == 0) {
        --c;
        g = 11
      } else {
        --g
      }
      this.refresh(c, g)
    }.bindAsEventListener(this));
    d.$$("miniCalendarNext").observe("click", function () {
      var c = this.year;
      var g = this.month;
      if (g == 11) {
        ++c;
        g = 0
      } else {
        ++g
      }
      this.refresh(c, g)
    }.bindAsEventListener(this));
    d.$$("miniCalendarCurrent").observe("click", function () {
      this.setDate(getTodayDate(), this.current)
    }.bindAsEventListener(this));
    this.setDate(getTodayDate(), null)
  },
  element: function () {
    return this.elem
  },
  refresh: function (f, h) {
    this.year = f;
    this.month = h;
    this.elem.$$("miniCalendarTitle").update(formatYearMonth(f, h));
    var c = this.elem.$$("miniCalendar");
    c.select(".miniCalendarWeek").each(Element.remove);
    var a = c.$$$("tbody");
    var d = getTodayDate();
    var e = new Date(f, h, 1);
    e.setDate(e.getDate() - e.getDay());
    do {
      var g = new Element("tr");
      g.addClassName("miniCalendarWeek");
      g.appendChild(this.createMarginCell());
      for (var i = 0; i < 7; ++i) {
        g.appendChild(this.createDay(e, d, e.getMonth() == h));
        e.setDate(e.getDate() + 1)
      }
      g.appendChild(this.createMarginCell());
      a.appendChild(g)
    } while (e.getMonth() == h);
    this.layoutBackground()
  },
  setDate: function (a, b) {
    this.current = b;
    this.refresh(a.getFullYear(), a.getMonth())
  },
  layoutBackground: function () {
    var a = this.elem.getDimensions();
    var c = this.elem.$$("miniCalendarHeader");
    var b = c.cumulativeOffset().top + c.getHeight() - this.elem.cumulativeOffset().top;
    this.elem.$$("miniCalendarBgTop").setStyle({width: Math.max(a.width - 7 * 2, 0) + "px", height: b + "px"});
    this.elem.$$("miniCalendarBgBody").setStyle({
      width: Math.max(a.width - 7 * 2, 0) + "px",
      height: Math.max(a.height - b - 7, 0) + "px"
    });
    this.elem.$$("miniCalendarBgLeft").setStyle({height: Math.max(a.height - 7, 0) + "px"});
    this.elem.$$("miniCalendarBgRight").setStyle({height: Math.max(a.height - 7, 0) + "px"});
    this.elem.$$("miniCalendarBgBottom").setStyle({width: Math.max(a.width - 7 * 2, 0) + "px"})
  },
  createDay: function (c, b, f) {
    var j = new Element("td");
    if (c != null) {
      var i = c.getFullYear();
      var a = c.getMonth();
      var g = c.getDate();
      j.appendChild(document.createTextNode(g));
      if (this.clickFunc) {
        j.observe("click", this.clickFunc.bind(this, i, a, g))
      }
      j.addClassName("miniCalendarDay");
      if (f) {
        switch (c.getDay()) {
          case 0:
            j.addClassName("miniCalendarSunday");
            break;
          case 6:
            j.addClassName("miniCalendarSaturday");
            break
        }
        if (isSameDay(b, c)) {
          j.addClassName("miniCalendarToday")
        }
        if (isSameDay(this.current, c)) {
          j.addClassName("miniCalendarSelection")
        }
        var e = getHolidayName(c);
        if (e) {
          j.addClassName("miniCalendarHoliday");
          j.setAttribute("title", e)
        }
      } else {
        j.addClassName("miniCalendarOtherMonth")
      }
      if (this.dayFunc) {
        this.dayFunc(j, c)
      }
      j.observe("mouseover", this.overDay);
      j.observe("mouseout", this.outDay)
    }
    return j
  },
  createMarginCell: function () {
    var a = new Element("td");
    a.addClassName("miniCalendarMarginCell");
    return a
  },
  overDay: function (a) {
    a.element().addClassName("hilighted")
  },
  outDay: function (a) {
    a.element().removeClassName("hilighted")
  }
});
var NotificationBase = Class.create({
  elem: null,
  messageElem: null,
  height: 0,
  message: "",
  delay: -1,
  timer: null,
  initialize: function (a) {
    this.elem = a;
    this.messageElem = this.elem.$$("notificationMessage");
    this.height = this.elem.getHeight() + 16;
    this.elem.setStyle({top: -this.height + "px", height: this.height + "px"})
  },
  showNotification: function (b, c, a) {
    if (this.timer != null) {
      clearTimeout(this.timer)
    }
    this.elem.setStyle({
      left: Math.floor((getWindowWidth() - this.elem.getWidth()) / 2) + "px",
      backgroundImage: "url(" + g_imageAssetBaseURL + "/image/notification/" + (c || "information") + ".png)"
    });
    this.message = b;
    this.delay = a || -1;
    if (this.message) {
      this.messageElem.update2(this.message);
      this.elem.setStyle({height: "auto"});
      this.height = this.elem.getHeight() + 16;
      this.elem.setStyle({top: -this.height + "px", height: this.height + "px"});
      this.messageElem.innerHTML = ""
    }
    this.elem.setStyle({top: -this.height + "px"});
    this.show()
  },
  show: function () {
    var a = this.elem.positionedOffset().top;
    if (a < 0) {
      a = Math.min(0, a + 10);
      this.elem.setStyle({top: a + "px"});
      this.timer = setTimeout(this.show.bind(this), 100)
    } else {
      if (this.message) {
        this.messageElem.update2(this.message)
      }
      if (this.delay != -1) {
        this.timer = setTimeout(this.hide.bind(this), this.delay)
      }
    }
  },
  hide: function () {
    var a = this.elem.positionedOffset().top;
    if (this.message) {
      this.messageElem.innerHTML = ""
    }
    if (a > -this.height) {
      a = Math.max(-this.height, a - 10);
      this.elem.setStyle({top: a + "px"});
      this.timer = setTimeout(this.hide.bind(this), 100)
    }
  }
});
var Notification = Class.create(NotificationBase, {
  initialize: function (a) {
    NotificationBase.prototype.initialize.apply(this, arguments);
    this.elem.$$("notificationClose").observe("click", function () {
      if (this.timer) {
        clearTimeout(this.timer)
      }
      this.hide()
    }.bindAsEventListener(this))
  }, showInformation: function (d, b) {
    var a = d.escapeHTML();
    var c = Notification.DELAY_INFORMATION;
    if (b) {
      a += ' (<a href="javascript:undo(' + b + ')" class="undo">' + MESSAGES["canvas.undo"].escapeHTML() + "</a>)";
      c = Notification.DELAY_UNDO
    }
    this.showNotification(a, "information", c)
  }, showError: function (a) {
    this.showNotification(a.escapeHTML(), "error", Notification.DELAY_ERROR)
  }
});
Notification.DELAY_INFORMATION = 3000;
Notification.DELAY_ERROR = 10000;
Notification.DELAY_UNDO = 20000;
var MovieNotification = Class.create(NotificationBase, {
  initialize: function (a) {
    NotificationBase.prototype.initialize.apply(this, arguments);
    this.elem.$$("stopMovie").observe("click", function () {
      stopMovie()
    })
  }, showInformation: function () {
    this.showNotification()
  }
});
var Board = Class.create({
  board: null, visible: false, initialize: function (a) {
    this.board = a
  }, show: function () {
    this.board.show();
    this.board.setStyle({height: "auto"});
    var a = this.board.getHeight();
    if (Prototype.Browser.IEOld) {
      this.board.setStyle({height: a + "px"});
      layoutCanvasView()
    } else {
      var c = 0;
      var b = function () {
        this.board.setStyle({height: c + "px"});
        layoutCanvasView();
        if (c < a) {
          c = Math.min(c + Board.STEP, a);
          b.delay(0.1)
        }
      }.bind(this);
      b()
    }
    this.visible = true
  }, hide: function () {
    if (Prototype.Browser.IEOld) {
      this.board.setStyle({height: "0px"});
      this.board.hide();
      layoutCanvasView()
    } else {
      var b = this.board.getHeight();
      var a = function () {
        this.board.setStyle({height: b + "px"});
        layoutCanvasView();
        if (b > 0) {
          b = Math.max(b - Board.STEP, 0);
          a.delay(0.1)
        } else {
          this.board.hide()
        }
      }.bind(this);
      a()
    }
    this.visible = false
  }, toggle: function () {
    if (this.visible) {
      this.hide()
    } else {
      this.show()
    }
  }, getHeight: function () {
    return this.board.visible() ? this.board.getHeight() : 0
  }
});
Board.STEP = 30;
var PaletteEntry = Class.create({
  initialize: function (d, c, g, a, b, f, e) {
    this.index = d;
    this.name = c;
    this.width = g;
    this.height = a;
    this.color = b;
    this.type = f;
    this.points = e
  }
});
var Palette = Class.create({
  MIN_PALETTE_HEIGHT: 52,
  MAX_MY_PALETTE_HEIGHT: 188,
  MAX_READWRITE_PALETTE_HEIGHT: 188,
  MAX_READONLY_PALETTE_HEIGHT: 77,
  MAX_PALETTE_ENTRIES: 8,
  MAX_NORMAL_ENTRIES: 4,
  PALETTE_ENTRIES: [new PaletteEntry(0, "yellow", 280, 200, "yellow", NewEntry.TYPE_NORMAL, [new Point(19, 45), new Point(19, 101), new Point(80, 101), new Point(80, 45)]), new PaletteEntry(1, "green", 280, 200, "green", NewEntry.TYPE_NORMAL, [new Point(92, 45), new Point(92, 101), new Point(153, 101), new Point(153, 45)]), new PaletteEntry(2, "blue", 280, 200, "blue", NewEntry.TYPE_NORMAL, [new Point(165, 45), new Point(165, 101), new Point(226, 101), new Point(226, 45)]), new PaletteEntry(3, "peach", 280, 200, "peach", NewEntry.TYPE_NORMAL, [new Point(238, 45), new Point(238, 101), new Point(298, 101), new Point(298, 45)]), new PaletteEntry(4, "image", 280, 200, "white", NewEntry.TYPE_IMAGE, [new Point(127, 110), new Point(127, 149), new Point(166, 149), new Point(166, 110)]), new PaletteEntry(5, "movie", 280, 200, "black", NewEntry.TYPE_MOVIE, [new Point(169, 110), new Point(169, 149), new Point(210, 149), new Point(210, 110)]), new PaletteEntry(6, "attachment", 280, 200, "white", NewEntry.TYPE_ATTACHMENT, [new Point(213, 110), new Point(213, 149), new Point(254, 149), new Point(254, 110)]), new PaletteEntry(7, "transparent", 280, 200, "transparent", NewEntry.TYPE_NORMAL, [new Point(257, 110), new Point(257, 149), new Point(298, 149), new Point(298, 110)])],
  initialize: function (d) {
    this.palette = $("palette");
    this.body = $("paletteBody");
    this.header = $("paletteHeader");
    this.footer = $("paletteFooter");
    this.newEntryMode = $("newEntryMode");
    this.newEntryNext = $("newEntryNext");
    this.showPrivate = $("showPrivate");
    this.expandElem = $("paletteExpand");
    this.pinElem = $("palettePin");
    this.imgBody = $("paletteImgBody");
    this.pinned = true;
    this.expandingTimer = null;
    this.preHideTimer = null;
    if (g_canvasTypeFlags & CanvasType.FLAG_NEW) {
      for (var c = 0; c < this.MAX_PALETTE_ENTRIES; ++c) {
        var h = this.PALETTE_ENTRIES[c];
        $("paletteItem" + c).setStyle({backgroundImage: "url(" + g_imageAssetBaseURL + "/image/palette/entry/" + h.name + ".png)"})
      }
    }
    if (this.body) {
      this.palette.observe(Event.MOUSE_DOWN, function (m) {
        if (!m.isLeftClick()) {
          return
        }
        if (!this.isExpanded()) {
          return
        }
        var e = this.getPaletteEntryFromScreenPos(m.pointerX(), m.pointerY());
        if (e) {
          d(m, e);
          m.stop()
        }
      }.bindAsTouchEventListener().bindAsEventListener(this))
    }
    if (this.expandElem) {
      this.palette.observe("mouseover", this.expandAuto.bindAsEventListener(this));
      this.palette.observe("mouseout", this.collapseAuto.bindAsEventListener(this))
    }
    var j = function (m) {
      var e = m.element();
      if (Prototype.Browser.IE6) {
        var n = e.classNames().grep(/action.+/)[0];
        e.addClassName(n + "Hover");
        e.removeClassName(n)
      } else {
        e.setStyle({backgroundPosition: e.getStyle("backgroundPosition").split(" ")[0] + " -22px"})
      }
    };
    var f = function (m) {
      var e = m.element();
      if (Prototype.Browser.IE6) {
        var n = e.classNames().grep(/action.+/)[0];
        e.addClassName(n.slice(0, -5));
        e.removeClassName(n)
      } else {
        e.setStyle({backgroundPosition: e.getStyle("backgroundPosition").split(" ")[0] + " 0px"})
      }
    };
    this.palette.select(".action").each(function (e) {
      e.observe("mouseover", j);
      e.observe("mouseout", f)
    }.bind(this));
    this.newEntryMode.observe("click", toggleNewEntryMode);
    this.newEntryNext.observe("click", function (e) {
      if (g_newEntryMode) {
        nextNewEntry();
        e.stop()
      }
    });
    if (Prototype.Browser.IE6) {
      var a = function () {
        if (!g_newEntryMode) {
          IE6.addClassName(this.newEntryMode, "", "newEntryButtonHover", "newEntryButton")
        }
      }.bindAsEventListener(this);
      var g = function () {
        if (!g_newEntryMode) {
          IE6.removeClassName(this.newEntryMode, "", "newEntryButtonHover", "newEntryButton")
        }
      }.bindAsEventListener(this);
      this.newEntryMode.observe("mouseover", a);
      this.newEntryMode.observe("mouseout", g)
    }
    if (this.showPrivate) {
      this.showPrivate.observe("click", toggleShowPrivate);
      if (Prototype.Browser.IE6) {
        var b = function () {
          if (!g_showPrivate) {
            IE6.addClassName(this.showPrivate, "", "showPrivateButtonHover", "showPrivateButton")
          }
        }.bindAsEventListener(this);
        var i = function () {
          if (!g_showPrivate) {
            IE6.removeClassName(this.showPrivate, "", "showPrivateButtonHover", "showPrivateButton")
          }
        }.bindAsEventListener(this);
        this.showPrivate.observe("mouseover", b);
        this.showPrivate.observe("mouseout", i)
      }
    }
    var k = function (e) {
      IE6.addClassName(e.element(), "pushed", "subscriptionPushed", "")
    };
    var l = function (e) {
      IE6.removeClassName(e.element(), "pushed", "subscriptionPushed", "")
    };
    this.palette.select(".subscription").each(function (e) {
      e.observe("mousedown", k);
      e.observe("mouseup", l);
      e.observe("mouseout", l)
    });
    this.palette.$$("subscribe").observe("click", function () {
      var e = $("subscribeCanvasForm");
      e.canvasId.value = g_canvasId;
      new AjaxUtil().post("/api/canvas/subscribe", e, function () {
        info("canvas.info.canvasSubscribed");
        updateSubscription()
      })
    });
    this.palette.$$("unsubscribe").observe("click", function () {
      var e = $("unsubscribeCanvasForm");
      e.canvasId.value = g_canvasId;
      new AjaxUtil().post("/api/canvas/unsubscribe", e, function () {
        info("canvas.info.canvasUnsubscribed");
        updateSubscription()
      })
    });
    if (this.expandElem) {
      this.expandElem.observe("click", function () {
        if (this.isExpanded()) {
          this.collapse()
        } else {
          this.expand()
        }
      }.bindAsEventListener(this))
    }
    if (this.pinElem) {
      this.pinElem.observe("click", function () {
        this.setPinned(!this.isPinned())
      }.bindAsEventListener(this))
    }
  },
  restore: function (b, c, a) {
    if (!b) {
      this.setPinned(b)
    }
    if (!c && this.expandElem) {
      if (this.body) {
        this.body.hide()
      }
      this.footer.hide();
      this.setHeight(this.MIN_PALETTE_HEIGHT);
      this.expandElem.addClassName("collapsed");
      if (this.pinElem) {
        this.pinElem.hide()
      }
    }
    if (a) {
      for (var d = 0; d < this.MAX_NORMAL_ENTRIES; ++d) {
        this.setColor(d, a[d])
      }
    }
  },
  element: function () {
    return this.palette
  },
  expand: function () {
    if (this.expandingTimer) {
      clearTimeout(this.expandingTimer);
      this.expandingTimer = null
    }
    var b = this.getMaxHeight();
    var a = this.palette.getHeight();
    var c = function () {
      a = Math.min(a + 10, b);
      this.setHeight(a);
      if (a != b) {
        this.expandingTimer = setTimeout(c, 20)
      } else {
        if (this.body) {
          this.body.show()
        }
        this.footer.show();
        this.expandElem.removeClassName("collapsed");
        if (this.pinElem) {
          this.pinElem.show()
        }
        saveLocalPreferences();
        this.expandingTimer = null
      }
    }.bind(this);
    this.expandingTimer = setTimeout(c, 0)
  },
  collapse: function () {
    if (this.expandingTimer) {
      clearTimeout(this.expandingTimer);
      this.expandingTimer = null
    }
    if (this.body) {
      this.body.hide()
    }
    this.footer.hide();
    var a = this.palette.getHeight();
    var b = function () {
      a = Math.max(a - 10, this.MIN_PALETTE_HEIGHT);
      this.setHeight(a);
      if (a != this.MIN_PALETTE_HEIGHT) {
        this.expandingTimer = setTimeout(b, 20)
      } else {
        this.expandElem.addClassName("collapsed");
        if (this.pinElem) {
          this.pinElem.hide()
        }
        saveLocalPreferences();
        this.expandingTimer = null
      }
    }.bind(this);
    this.expandingTimer = setTimeout(b, 0)
  },
  setHeight: function (a) {
    this.palette.setStyle({height: a + "px"});
    this.imgBody.setStyle({height: (a - (this.MIN_PALETTE_HEIGHT - 4)) + "px"})
  },
  getMaxHeight: function () {
    return g_canvasType == CanvasType.MY ? this.MAX_MY_PALETTE_HEIGHT : g_canvasTypeFlags & CanvasType.FLAG_NEW ? this.MAX_READWRITE_PALETTE_HEIGHT : this.MAX_READONLY_PALETTE_HEIGHT
  },
  isExpanded: function () {
    return this.expandElem && !this.expandElem.hasClassName("collapsed")
  },
  isExpandable: function () {
    return this.expandElem
  },
  isPinned: function () {
    return this.pinned
  },
  setPinned: function (a) {
    this.pinned = a;
    if (a) {
      this.pinElem.addClassName("pinned")
    } else {
      this.pinElem.removeClassName("pinned")
    }
    if (this.expandElem) {
      if (a) {
        this.expandElem.show()
      } else {
        this.expandElem.hide()
      }
    }
    saveLocalPreferences()
  },
  expandAuto: function (a) {
    if (!this.pinned) {
      if (this.preHideTimer) {
        clearTimeout(this.preHideTimer);
        this.preHideTimer = null
      }
      this.expand()
    }
  },
  collapseAuto: function (a) {
    if (Position.within(this.palette, a.pointerX(), a.pointerY())) {
      return
    }
    if (!this.pinned) {
      if (!this.preHideTimer) {
        this.preHideTimer = setTimeout(this.collapse.bind(this), 500)
      }
    }
  },
  getColors: function () {
    var a = [];
    for (var b = 0; b < this.MAX_NORMAL_ENTRIES; ++b) {
      a.push(this.PALETTE_ENTRIES[b].color)
    }
    return a
  },
  setColor: function (b, a) {
    if (!(g_canvasTypeFlags & CanvasType.FLAG_NEW)) {
      return
    }
    if (this.PALETTE_ENTRIES.find(function (f) {
        return f.color == a
      })) {
      return
    }
    var d = this.PALETTE_ENTRIES[b];
    if (d.type != NewEntry.TYPE_NORMAL || d.color == "transparent") {
      return
    }
    d.name = a;
    d.color = a;
    var c = $("paletteItem" + b);
    c.setStyle({backgroundImage: "url(" + g_imageAssetBaseURL + "/image/palette/entry/" + d.name + ".png)"});
    IEPNGFIX.fix(c);
    saveLocalPreferences()
  },
  getPaletteEntryFromScreenPos: function (b, g) {
    var f = this.palette.cumulativeOffset();
    var a = new Point(b - f.left, g - f.top);
    for (var d = 0; d < this.PALETTE_ENTRIES.length; ++d) {
      var c = this.PALETTE_ENTRIES[d];
      if (inside(a, c.points)) {
        return c
      }
    }
    return null
  },
  updateNewEntryMode: function () {
    if (g_newEntryMode) {
      var a = this.newEntryMode.classNames().grep(/newEntryButton.*/)[0];
      IE6.addClassName(this.newEntryMode, "pushed", "newEntryButtonPushed", a);
      this.newEntryNext.show()
    } else {
      IE6.removeClassName(this.newEntryMode, "pushed", "newEntryButtonPushed", "newEntryButton");
      this.newEntryNext.hide()
    }
  },
  updateShowPrivate: function () {
    if (g_showPrivate) {
      IE6.addClassName(this.showPrivate, "pushed", "showPrivateButtonPushed", "showPrivateButtonHover")
    } else {
      IE6.removeClassName(this.showPrivate, "pushed", "showPrivateButtonPushed", "showPrivateButton")
    }
  },
  setSubscribed: function (a) {
    (a ? Element.addClassName : Element.removeClassName)(this.header, "subscribed");
    (a ? Element.removeClassName : Element.addClassName)(this.header, "unsubscribed")
  }
});
var Dock = Class.create({
  initialize: function () {
    this.dock = $("dock");
    this.view = $("dockView");
    this.bg = $("dockBg");
    this.body = $("dockBody");
    this.grip = $("dockGrip");
    this.close = $("dockClose");
    this.pin = $("dockPin");
    this.leftArrow = $("dockLeft");
    this.rightArrow = $("dockRight");
    this.info = $("dockInfo");
    this.visible = true;
    this.width = Dock.DEFAULT_WIDTH;
    this.scrollPos = 0;
    this.restoredScrollPos = 0;
    this.type = Dock.TYPE_MY;
    this.resizing = null;
    this.showingTimer = null;
    this.pinned = true;
    this.preHideTimer = null;
    this.items = this.dock.select(".dockItem");
    var a = function (b) {
      if (b.isLeftClick()) {
        b.stop()
      }
    };
    this.grip.observe(Event.MOUSE_DOWN, this.resizeStartDrag.bindAsEventListener(this));
    this.close.observe("click", this.toggleShow.bindAsEventListener(this));
    this.close.observe("mousedown", a);
    this.pin.observe("click", this.togglePin.bindAsEventListener(this));
    this.pin.observe("mousedown", a);
    this.bg.observe("mouseover", this.showAuto.bindAsEventListener(this));
    this.bg.observe("mouseout", this.hideAuto.bindAsEventListener(this));
    this.leftArrow.observe("click", this.scrollLeft.bindAsEventListener(this));
    this.rightArrow.observe("click", this.scrollRight.bindAsEventListener(this));
    this.selector = new DockSelector(this)
  }, restore: function (b, d, c, a) {
    if (b) {
      this.width = b
    }
    if (d) {
      this.restoredScrollPos = d
    }
    this.setVisible(c);
    this.setWidth(c ? this.width : Dock.MIN_WIDTH);
    this.setPinned(a)
  }, load: function () {
    if (g_canvasType == CanvasType.GROUP) {
      this.type = Dock.TYPE_GROUP
    }
    this.loadWithType(this.type, g_groupId)
  }, loadWithType: function (c, b) {
    if (!(g_canvasTypeFlags & CanvasType.FLAG_DOCK)) {
      return
    }
    this.selector.set(c, b);
    if (this.type == Dock.TYPE_MY && this.restoredScrollPos == -1) {
      this.restoredScrollPos = this.getScrollPos()
    }
    this.type = c;
    var a = null;
    switch (c) {
      case Dock.TYPE_MY:
        a = "my";
        break;
      case Dock.TYPE_FAVORITES:
        a = "favorites";
        break;
      case Dock.TYPE_GROUP:
        a = "group";
        break
    }
    var d = "?type=" + a;
    if (c == Dock.TYPE_GROUP) {
      d += "&groupId=" + b
    }
    new AjaxUtil().get("/api/canvas/docks" + d, null, this.update.bindAsEventListener(this), error)
  }, update: function (b) {
    var d = this.hideInfo();
    if (d && !Prototype.Browser.IE) {
      this.dock.setOpacity(0)
    }
    var a = b.docks;
    var c = Dock.ITEM_TEMPLATE;
    this.dock.innerHTML = $A(a).map(function (f) {
      f.name = f.name.escapeHTML();
      f.path = f.path.escapeHTML();
      var j = [];
      if (f.id == g_canvasId) {
        j.push("dockSelfItem")
      }
      if (f.my) {
        j.push("dockMyItem")
      }
      if (f.writable) {
        j.push("dockWritableItem")
      }
      f.classNames = j.join(" ");
      return c.evaluate(f)
    }).join("");
    this.items = this.dock.select(".dockItem");
    this.bg.show();
    var g = this.type == Dock.TYPE_MY;
    var i = g ? this.restoredScrollPos : 0;
    if (Prototype.Browser.WebKit) {
      this.fixupScrollPos.curry(i).bind(this).defer()
    } else {
      this.fixupScrollPos(i)
    }
    if (g) {
      this.restoredScrollPos = -1
    }
    this.adjustScrollPos();
    this.updateArrows();
    if (d && !Prototype.Browser.IE) {
      var h = 0.1;
      var e = function () {
        this.dock.setOpacity(h);
        h += 0.1;
        if (h < 1) {
          e.delay(0.1)
        }
      }.bind(this);
      e()
    }
  }, showInfo: function (a, b, d) {
    this.info.setStyle({width: this.view.getWidth() + "px"});
    var c = this.info.$$$("img");
    c.src = a;
    IEPNGFIX.fix(c);
    if (d) {
      this.info.$$("dockInfoGroup").innerHTML = d.escapeHTML()
    }
    this.info.select(".dockInfo").each(Element.hide);
    this.info.$$(b).show();
    this.info.show();
    this.dock.hide()
  }, hideInfo: function () {
    if (!this.info.visible()) {
      return false
    }
    this.info.hide();
    this.dock.show();
    return true
  }, loadAllEntries: function () {
    this.items.each(this.loadEntriesByDockItem.bind(this))
  }, loadEntries: function (a) {
    var b = this.getDockItemFromCanvasId(a);
    if (b) {
      return this.loadEntriesByDockItem(b)
    }
  }, loadEntriesByDockItem: function (b) {
    var a = b.$$("dockItemImg");
    a.src = "/dock/" + this.getCanvasIdFromDockItem(b) + "?" + new Date().getTime();
    if (Prototype.Browser.IE) {
      IEPNGFIX.fix(a)
    }
  }, getWidth: function () {
    return this.width
  }, setWidth: function (a) {
    this.bg.setStyle({width: a + "px"});
    this.body.setStyle({width: Math.max(0, a - Dock.GRIP_WIDTH) + "px"});
    this.view.setStyle({width: Math.max(0, a - (Dock.ARROW_WIDTH * 2 + Dock.SELECTOR_WIDTH + Dock.GRIP_WIDTH)) + "px"})
  }, adjustWidth: function () {
    if (!this.visible) {
      return
    }
    var a = Math.min(this.bg.getWidth(), getWindowWidth() - $("panelBg").getWidth() - 50);
    var b = Dock.GRIP_WIDTH + Dock.ARROW_WIDTH * 2 + Dock.SELECTOR_WIDTH;
    var c = Math.floor((a - b) / Dock.ITEM_WIDTH + 0.5);
    if (c <= 0) {
      a = Dock.MIN_WIDTH
    } else {
      a = b + Dock.ITEM_WIDTH * c
    }
    this.setWidth(a);
    this.width = a;
    this.adjustScrollPos();
    this.updateArrows();
    saveLocalPreferences()
  }, isVisible: function () {
    return this.visible
  }, setVisible: function (a) {
    this.visible = a;
    if (a) {
      IE6.removeClassName(this.close, "closed", "dockOpenButton", "dockCloseButton")
    } else {
      IE6.addClassName(this.close, "closed", "dockOpenButton", "dockCloseButton")
    }
    saveLocalPreferences()
  }, resizeStartDrag: function (d) {
    if (!d.isLeftClick()) {
      return
    }
    if (this.resizing) {
      this.resizeEndDrag(d)
    }
    var e = this.grip.cumulativeOffset();
    var c = this.grip.getDimensions();
    var b = this.resizeDrag.bindAsEventListener(this);
    var a = this.resizeEndDrag.bindAsEventListener(this);
    this.resizing = new DockResizing(d.pointerX() - (e.left + c.width), b, a);
    Event.observe(document, Event.MOUSE_MOVE, b);
    Event.observe(document, Event.MOUSE_UP, a);
    d.stop()
  }.bindAsTouchEventListener(), resizeEndDrag: function (a) {
    this.resizeDrag(a);
    this.setVisible(true);
    this.adjustWidth();
    Event.stopObserving(document, Event.MOUSE_MOVE, this.resizing.move);
    Event.stopObserving(document, Event.MOUSE_UP, this.resizing.up);
    this.resizing = null
  }.bindAsTouchEventListener(), resizeDrag: function (b) {
    var a = Math.max(Dock.MIN_WIDTH, b.pointerX() - this.resizing.offsetX);
    this.setWidth(a);
    b.stop()
  }.bindAsTouchEventListener(), toggleShow: function (a) {
    if (this.visible) {
      this.hide()
    } else {
      this.show()
    }
    saveLocalPreferences()
  }, show: function () {
    if (this.visible) {
      return
    }
    this.changeWidth(Dock.MIN_WIDTH, function (a) {
      return a >= this.width ? null : Math.min(a + 50, this.width)
    }.bind(this));
    this.setVisible(true)
  }, hide: function () {
    if (!this.visible) {
      return
    }
    this.changeWidth(this.width, function (a) {
      return a <= Dock.MIN_WIDTH ? null : Math.max(a - 50, Dock.MIN_WIDTH)
    }.bind(this));
    this.setVisible(false)
  }, changeWidth: function (b, a) {
    if (this.showingTimer != null) {
      clearTimeout(this.showingTimer)
    }
    var c = function () {
      this.setWidth(b);
      b = a(b);
      if (b) {
        this.showingTimer = setTimeout(c, 10)
      } else {
        this.adjustWidth();
        this.showingTimer = null
      }
    }.bind(this);
    this.showingTimer = setTimeout(c, 10)
  }, togglePin: function (a) {
    this.setPinned(!this.pinned)
  }, setPinned: function (a) {
    this.pinned = a;
    if (a) {
      IE6.addClassName(this.pin, "pinned", "dockPinButton", "dockPinnedButton");
      this.close.show()
    } else {
      IE6.removeClassName(this.pin, "pinned", "dockPinButton", "dockPinnedButton");
      this.close.hide()
    }
    saveLocalPreferences()
  }, isPinned: function () {
    return this.pinned
  }, showAuto: function (a) {
    if (!this.pinned && !this.showingTimer) {
      if (this.preHideTimer) {
        clearTimeout(this.preHideTimer);
        this.preHideTimer = null
      }
      this.show()
    }
  }, hideAuto: function (a) {
    if (Position.within(this.bg, a.pointerX(), a.pointerY())) {
      return
    }
    if (!this.pinned && !this.resizing) {
      if (!this.preHideTimer) {
        this.preHideTimer = setTimeout(this.hide.bind(this), 800)
      }
    }
  }, within: function (a, b) {
    return Position.within(this.bg, a, b)
  }, scrollLeft: function (b) {
    var c = this.getScrollPos();
    if (c >= 0) {
      return
    }
    var a = this.view.getWidth();
    this.setScrollPos(Math.min(0, c + Math.max(1, Math.floor(a / Dock.ITEM_WIDTH - 1)) * Dock.ITEM_WIDTH))
  }, scrollRight: function (d) {
    var a = this.dock.getDimensions();
    var c = this.items.length;
    if (a.width >= c * Dock.ITEM_WIDTH) {
      return
    }
    var e = this.getScrollPos();
    var b = this.view.getWidth();
    this.setScrollPos(Math.max(-(Dock.ITEM_WIDTH * c - b), e - Math.max(1, Math.floor(b / Dock.ITEM_WIDTH - 1)) * Dock.ITEM_WIDTH))
  }, getScrollPos: function () {
    return this.scrollPos
  }, setScrollPos: function (c) {
    var a = this.getScrollPos();
    var b = function () {
      if (a < c) {
        a = Math.min(a + (Dock.ITEM_WIDTH), c)
      } else {
        if (a > c) {
          a = Math.max(a - (Dock.ITEM_WIDTH), c)
        }
      }
      if (a != c) {
        this.dock.setStyle({left: a + "px"});
        b.delay(0.1)
      } else {
        this.fixupScrollPos(a)
      }
    }.bind(this);
    b.defer()
  }, adjustScrollPos: function () {
    var b = this.view.getWidth();
    var d = this.dock.positionedOffset();
    var a = this.dock.getDimensions();
    if (d.left + a.width < b) {
      var c = Math.floor((b - (d.left + a.width)) / Dock.ITEM_WIDTH);
      this.fixupScrollPos(Math.min(0, d.left + c * Dock.ITEM_WIDTH))
    }
  }, fixupScrollPos: function (a) {
    this.scrollPos = Math.floor(a / Dock.ITEM_WIDTH) * Dock.ITEM_WIDTH;
    this.dock.setStyle({left: this.scrollPos + "px"});
    this.updateArrows();
    saveLocalPreferences()
  }, getMyScrollPos: function () {
    return this.selector.type == Dock.TYPE_MY ? this.getScrollPos() : this.restoredScrollPos
  }, updateArrows: function () {
    var c = this.getScrollPos();
    if (c == 0) {
      this.leftArrow.addClassName("disabled")
    } else {
      this.leftArrow.removeClassName("disabled")
    }
    var a = this.view.getWidth();
    var b = this.items.length;
    if (c <= -(Dock.ITEM_WIDTH * b - a)) {
      this.rightArrow.addClassName("disabled")
    } else {
      this.rightArrow.removeClassName("disabled")
    }
  }, getCanvasIdFromDockItem: function (a) {
    return a.id.substring(8)
  }, getDockItemFromCanvasId: function (a) {
    return $("dockItem" + a)
  }, getDockItemFromPosition: function (a, c) {
    if (this.isVisible() && Position.within(this.view, a, c)) {
      for (var b = 0; b < this.items.length; ++b) {
        if (Position.within(this.items[b].$$("dockView"), a, c)) {
          return this.items[b]
        }
      }
    }
    return null
  }, mapScreenDockPosToCanvasPos: function (d, a, c) {
    var b = d.$$("dockView").cumulativeOffset();
    return new Point(this.mapDockToCanvasX(a - b.left), this.mapDockToCanvasY(c - b.top))
  }, mapDockToCanvasX: function (a) {
    return Math.floor(a * CANVAS_SIZE.width / Dock.VIEW_SIZE.width)
  }, mapDockToCanvasY: function (a) {
    return Math.floor(a * CANVAS_SIZE.height / Dock.VIEW_SIZE.height)
  }, mapCanvasToDockX: function (a) {
    return Math.floor(a * Dock.VIEW_SIZE.width / CANVAS_SIZE.width)
  }, mapCanvasToDockY: function (a) {
    return Math.floor(a * Dock.VIEW_SIZE.height / CANVAS_SIZE.height)
  }
});
Dock.TYPE_MY = 0;
Dock.TYPE_FAVORITES = 1;
Dock.TYPE_GROUP = 2;
Dock.DEFAULT_WIDTH = 605;
Dock.MIN_WIDTH = 27;
Dock.ITEM_WIDTH = 130;
Dock.VIEW_SIZE = new Size(78, 58);
Dock.GRIP_WIDTH = 27;
Dock.ARROW_WIDTH = 17;
Dock.SELECTOR_WIDTH = 30;
Dock.ITEM_TEMPLATE = new Template('<div id="dockItem#{id}" class="dockItem #{classNames}">  <div class="dockName"><nobr><a href="#{path}" title="#{name}">#{name}</a></nobr></div>  <img class="dockItemImg" src="/dock/#{id}" />  <a class="dockView" href="#{path}" title="#{name}"></a></div>');
var DockResizing = Class.create({
  offsetX: null, move: null, up: null, initialize: function (b, c, a) {
    this.offsetX = b;
    this.move = c;
    this.up = a
  }
});
var DockSelector = Class.create({
  dock: null, type: null, groupId: null, timer: null, initialize: function (a) {
    this.dock = a;
    $("dockSelectorNext").observe("click", this.next.bindAsEventListener(this));
    $("dockSelectorPrev").observe("click", this.previous.bindAsEventListener(this));
    $("dockSelectorMy").observe("click", this.my.bindAsEventListener(this))
  }, set: function (b, a) {
    this.type = b;
    this.groupId = a
  }, next: function () {
    var b = (this.type || Dock.TYPE_MY);
    var a = null;
    switch (b) {
      case Dock.TYPE_MY:
        b = Dock.TYPE_FAVORITES;
        break;
      case Dock.TYPE_FAVORITES:
        if (g_groups.length == 0) {
          b = Dock.TYPE_MY
        } else {
          b = Dock.TYPE_GROUP;
          a = g_groups[0].id
        }
        break;
      case Dock.TYPE_GROUP:
        var c = g_groups.indexOf(g_groups.find(function (d) {
          return d.id == this.groupId
        }.bind(this)));
        if (c == -1 || c == g_groups.length - 1) {
          b = Dock.TYPE_MY
        } else {
          a = g_groups[c + 1].id
        }
        break
    }
    this.select(b, a)
  }, previous: function () {
    var b = (this.type || Dock.TYPE_MY);
    var a = null;
    switch (b) {
      case Dock.TYPE_MY:
        if (g_groups.length == 0) {
          b = Dock.TYPE_FAVORITES
        } else {
          b = Dock.TYPE_GROUP;
          a = g_groups[g_groups.length - 1].id
        }
        break;
      case Dock.TYPE_FAVORITES:
        b = Dock.TYPE_MY;
        break;
      case Dock.TYPE_GROUP:
        var c = g_groups.indexOf(g_groups.find(function (d) {
          return d.id == this.groupId
        }.bind(this)));
        if (c == -1 || c == 0) {
          b = Dock.TYPE_FAVORITES
        } else {
          a = g_groups[c - 1].id
        }
        break
    }
    this.select(b, a)
  }, my: function () {
    if (this.type != Dock.TYPE_MY) {
      this.select(Dock.TYPE_MY, null)
    }
  }, select: function (d, b) {
    this.type = d;
    this.groupId = b;
    var a = "";
    var c = "";
    var e = null;
    switch (d) {
      case Dock.TYPE_MY:
        a = "/user/icon/" + g_userId + "?size=large";
        c = "dockInfoMy";
        break;
      case Dock.TYPE_FAVORITES:
        a = g_imageAssetBaseURL + "/image/dock/favorites.png";
        c = "dockInfoFavorites";
        break;
      case Dock.TYPE_GROUP:
        a = "/grouphelper/icon/" + b + "?size=large";
        c = "dockInfoGroup";
        e = g_groups.find(function (f) {
          return f.id == b
        }).name;
        break
    }
    this.dock.showInfo(a, c, e);
    if (this.timer) {
      clearTimeout(this.timer)
    }
    this.timer = setTimeout(function () {
      this.dock.loadWithType(d, b);
      this.timer = null
    }.bind(this), 500)
  }
});
var Panel = Class.create({
  panel: null,
  view: null,
  grip: null,
  movingView: null,
  bg: null,
  close: null,
  pin: null,
  canvasView: null,
  calendar: null,
  viewSize: null,
  parentSize: null,
  visible: true,
  maxPanelWidth: 0,
  width: 0,
  resizing: null,
  moving: null,
  movingTimer: null,
  showingTimer: null,
  pinned: true,
  preHideTimer: null,
  initialize: function (c, b) {
    this.panel = $("panel");
    this.view = $("panelView");
    this.grip = $("panelGrip");
    this.movingView = $("panelViewMoving");
    this.bg = $("panelBg");
    this.close = $("panelClose");
    this.pin = $("panelPin");
    this.canvasView = c;
    this.calendar = new PanelCalendar($("panelCalendar"), b);
    this.viewSize = this.view.getDimensions();
    this.parentSize = this.view.getOffsetParent().getDimensions();
    this.maxPanelWidth = b ? Panel.MAX_WITH_CALENDAR_WIDTH : Panel.MAX_WITHOUT_CALENDAR_WIDTH;
    this.width = this.maxPanelWidth;
    var a = function (d) {
      if (d.isLeftClick()) {
        d.stop()
      }
    };
    this.grip.observe(Event.MOUSE_DOWN, this.resizeStartDrag.bindAsEventListener(this));
    this.close.observe("click", this.toggleShow.bindAsEventListener(this));
    this.close.observe("mousedown", a);
    this.pin.observe("click", this.togglePin.bindAsEventListener(this));
    this.pin.observe("mousedown", a);
    this.bg.observe("mouseover", this.showAuto.bindAsEventListener(this));
    this.bg.observe("mouseout", this.hideAuto.bindAsEventListener(this));
    this.view.observe(Event.MOUSE_DOWN, this.moveStartDrag.bindAsEventListener(this))
  },
  restore: function (b, c, a) {
    if (b) {
      this.width = Math.min(b, this.maxPanelWidth)
    }
    this.setVisible(c);
    this.setWidth(c ? this.width : Panel.MIN_WIDTH);
    this.setPinned(a)
  },
  layout: function () {
    var a = this.canvasView.getDimensions();
    this.view.setStyle({
      left: this.mapCanvasToPanelX(g_origin.x) + "px",
      top: this.mapCanvasToPanelY(g_origin.y) + "px",
      width: (this.mapCanvasToPanelX(a.width) + 1) + "px",
      height: (this.mapCanvasToPanelY(a.height) + 1) + "px"
    });
    this.viewSize = this.view.getDimensions()
  },
  adjustWidth: function () {
    if (!this.visible) {
      return
    }
    var a = this.bg.getWidth();
    if (a > (Panel.MAX_WITH_CALENDAR_WIDTH + Panel.MAX_WITHOUT_CALENDAR_WIDTH) / 2) {
      a = Panel.MAX_WITH_CALENDAR_WIDTH
    } else {
      if (a > (Panel.MAX_WITHOUT_CALENDAR_WIDTH + Panel.MIN_WIDTH) / 2) {
        a = Panel.MAX_WITHOUT_CALENDAR_WIDTH
      } else {
        a = Panel.MIN_WIDTH
      }
    }
    a = Math.min(this.maxPanelWidth, a);
    this.setWidth(a);
    this.width = a;
    saveLocalPreferences()
  },
  resizeStartDrag: function (c) {
    if (!c.isLeftClick()) {
      return
    }
    if (this.resizing) {
      this.resizeEndDrag(c)
    }
    var d = this.grip.cumulativeOffset();
    var b = this.resizeDrag.bindAsEventListener(this);
    var a = this.resizeEndDrag.bindAsEventListener(this);
    this.resizing = new PanelResizing(c.pointerX() - d.left, b, a);
    Event.observe(document, Event.MOUSE_MOVE, b);
    Event.observe(document, Event.MOUSE_UP, a);
    c.stop()
  }.bindAsTouchEventListener(),
  resizeEndDrag: function (a) {
    this.resizeDrag(a);
    this.setVisible(true);
    this.adjustWidth();
    g_dock.adjustWidth();
    Event.stopObserving(document, Event.MOUSE_MOVE, this.resizing.move);
    Event.stopObserving(document, Event.MOUSE_UP, this.resizing.up);
    this.resizing = null
  }.bindAsTouchEventListener(),
  resizeDrag: function (b) {
    var a = Math.min(this.maxPanelWidth, Math.max(Panel.MIN_WIDTH, getWindowWidth() - (b.pointerX() - this.resizing.offsetX)));
    this.setWidth(a);
    b.stop()
  }.bindAsTouchEventListener(),
  toggleShow: function () {
    if (this.visible) {
      this.hide()
    } else {
      this.show()
    }
    saveLocalPreferences()
  },
  show: function () {
    if (this.visible) {
      return
    }
    this.changeWidth(Panel.MIN_WIDTH, function (a) {
      return a >= this.width ? null : Math.min(a + 20, this.width)
    }.bind(this));
    this.setVisible(true)
  },
  hide: function () {
    if (!this.visible) {
      return
    }
    this.changeWidth(this.width, function (a) {
      return a <= Panel.MIN_WIDTH ? null : Math.max(a - 20, Panel.MIN_WIDTH)
    }.bind(this));
    this.setVisible(false)
  },
  changeWidth: function (b, a) {
    if (this.showingTimer) {
      clearTimeout(this.showingTimer)
    }
    var c = function () {
      this.setWidth(b);
      b = a(b);
      if (b) {
        this.showingTimer = setTimeout(c, 10)
      } else {
        g_dock.adjustWidth();
        this.showingTimer = null
      }
    }.bind(this);
    this.showingTimer = setTimeout(c, 10)
  },
  togglePin: function () {
    this.setPinned(!this.pinned)
  },
  setPinned: function (a) {
    this.pinned = a;
    if (a) {
      IE6.addClassName(this.pin, "pinned", "panelPinButton", "panelPinnedButton");
      this.close.show()
    } else {
      IE6.removeClassName(this.pin, "pinned", "panelPinButton", "panelPinnedButton");
      this.close.hide()
    }
    saveLocalPreferences()
  },
  isPinned: function () {
    return this.pinned
  },
  getWidth: function () {
    return this.width
  },
  setWidth: function (a) {
    this.bg.setStyle({width: a + "px"});
    if (a > Panel.MAX_WITHOUT_CALENDAR_WIDTH) {
      this.calendar.show()
    } else {
      this.calendar.hide()
    }
  },
  isVisible: function () {
    return this.visible
  },
  setVisible: function (a) {
    this.visible = a;
    if (a) {
      IE6.removeClassName(this.close, "closed", "panelOpenButton", "panelCloseButton")
    } else {
      IE6.addClassName(this.close, "closed", "panelOpenButton", "panelCloseButton")
    }
    saveLocalPreferences()
  },
  showAuto: function (a) {
    if (!this.pinned && !this.showingTimer) {
      if (this.preHideTimer) {
        clearTimeout(this.preHideTimer);
        this.preHideTimer = null
      }
      this.show()
    }
  },
  hideAuto: function (a) {
    if (Position.within(this.bg, a.pointerX(), a.pointerY())) {
      return
    }
    if (!this.pinned && !this.resizing) {
      if (!this.preHideTimer) {
        this.preHideTimer = setTimeout(this.hide.bind(this), 500)
      }
    }
  },
  within: function (a, b) {
    return Position.within(this.bg, a, b)
  },
  moveStartDrag: function (e) {
    if (!e.isLeftClick()) {
      return
    }
    if (this.moving) {
      moveEndDrag(e)
    }
    var f = this.view.cumulativeOffset();
    var b = this.moveDrag.bindAsEventListener(this);
    var a = this.moveEndDrag.bindAsEventListener(this);
    this.moving = new PanelMoving(e.pointerX() - f.left, e.pointerY() - f.top, b, a);
    var d = this.view.positionedOffset();
    var c = this.view.getDimensions();
    this.movingView.setStyle({left: d.left + "px", top: d.top + "px", width: c.width + "px", height: c.height + "px"});
    this.movingView.show();
    Event.observe(document, Event.MOUSE_MOVE, b);
    Event.observe(document, Event.MOUSE_UP, a);
    e.stop()
  }.bindAsTouchEventListener(),
  moveEndDrag: function (a) {
    var b = this.parentSize.width - this.viewSize.width;
    var g = this.parentSize.height - this.viewSize.height;
    var k = this.movingView.getOffsetParent().cumulativeOffset();
    var e = Math.max(0, Math.min(b, a.pointerX() - k.left - this.moving.offsetX));
    var m = Math.max(0, Math.min(g, a.pointerY() - k.top - this.moving.offsetY));
    var p = e == b ? CANVAS_SIZE.width - this.canvasView.getDimensions().width : e * CANVAS_SIZE.width / Panel.SIZE.width;
    var l = m == g ? CANVAS_SIZE.height - this.canvasView.getDimensions().height : m * CANVAS_SIZE.height / Panel.SIZE.height;
    var o = new Array(10);
    var r = p - g_origin.x;
    var q = l - g_origin.y;
    var j = Math.max(1, Math.min(10, Math.floor(Math.log(Math.sqrt(r * r + q * q)))));
    for (var d = 1; d < j; ++d) {
      o[d - 1] = {
        x: g_origin.x + Math.floor((p - g_origin.x) * d / j),
        y: g_origin.y + Math.floor((l - g_origin.y) * d / j)
      }
    }
    o[j - 1] = {x: p, y: l};
    var i = 0;
    var h = function () {
      setOrigin(o[i]["x"], o[i]["y"]);
      if (i < j - 1) {
        ++i;
        this.movingTimer = window.setTimeout(h, 0)
      } else {
        this.movingView.hide();
        this.movingTimer = null
      }
    }.bind(this);
    if (this.movingTimer) {
      window.clearTimeout(this.movingTimer)
    }
    this.movingTimer = window.setTimeout(h, 0);
    Event.stopObserving(document, Event.MOUSE_MOVE, this.moving.move);
    Event.stopObserving(document, Event.MOUSE_UP, this.moving.up);
    this.moving = null
  }.bindAsTouchEventListener(),
  moveDrag: function (a) {
    var d = this.movingView.getOffsetParent().cumulativeOffset();
    var c = Math.max(0, Math.min(this.parentSize.width - this.viewSize.width, a.pointerX() - d.left - this.moving.offsetX));
    var b = Math.max(0, Math.min(this.parentSize.height - this.viewSize.height, a.pointerY() - d.top - this.moving.offsetY));
    this.movingView.setStyle({left: c + "px", top: b + "px"});
    a.stop()
  }.bindAsTouchEventListener(),
  getCalendar: function () {
    return this.calendar
  },
  initializeEntry: function (a) {
    var b = this.getEntryElemFromId(a.id);
    b.entry = a
  },
  insertEntryElem: function (a) {
    var b = $("panelEntryTemplate").cloneNode(true);
    this.setEntryId(b, a.id);
    this.panel.appendChild(b);
    this.fillEntryElem(b, a);
    if (a.maskStatus != 0) {
      b.setStyle({opacity: 0.3})
    } else {
      if (a.image || a.movie) {
        b.setStyle({opacity: 0.5})
      }
    }
    b.show()
  },
  updateEntryElem: function (a) {
    this.fillEntryElem(this.getEntryElemFromId(a.id), a)
  },
  removeEntryElem: function (a) {
    this.getEntryElemFromId(a).remove()
  },
  showEntryElem: function (a) {
    var b = this.getEntryElemFromId(a);
    if (b) {
      b.show()
    }
  },
  hideEntryElem: function (a) {
    var b = this.getEntryElemFromId(a);
    if (b) {
      b.hide()
    }
  },
  maskEntryElem: function (a) {
    var b = this.getEntryElemFromId(a);
    if (b) {
      b.setStyle({opacity: 0.3})
    }
  },
  unmaskEntryElem: function (a) {
    var c = this.getEntryElemFromId(a);
    var b = c.entry;
    c.setStyle({opacity: (b.image || b.movie) ? 0.5 : 1})
  },
  fillEntryElem: function (b, a) {
    b.entry = a;
    b.setStyle({
      left: this.mapCanvasToPanelX(a.left) + "px",
      top: this.mapCanvasToPanelY(a.top) + "px",
      width: this.mapCanvasToPanelX(a.width) + "px",
      height: this.mapCanvasToPanelY(a.height) + "px",
      zIndex: a.zIndex,
      backgroundColor: a.isFlag(Entry.FLAG_PANEL) ? Entry.getSolidColor(a.color) : "transparent"
    });
    b.setRotateAngle(a.angle)
  },
  setZIndex: function (a, c) {
    var b = this.getEntryElemFromId(a);
    if (b) {
      b.setStyle({zIndex: c})
    }
  },
  setEntryId: function (b, a) {
    b.setAttribute("id", "pentry" + a)
  },
  getEntryElemFromId: function (a) {
    return $("pentry" + a)
  },
  isInside: function (a, b) {
    if (this.isVisible()) {
      if (Position.within(this.panel, a, b)) {
        return true
      }
    }
    return false
  },
  mapScreenPanelPosToCanvasPos: function (a, c) {
    var b = this.panel.cumulativeOffset();
    return new Point(this.mapPanelToCanvasX(a - b.left), this.mapPanelToCanvasY(c - b.top))
  },
  mapPanelToCanvasX: function (a) {
    return Math.floor(a * CANVAS_SIZE.width / Panel.SIZE.width)
  },
  mapPanelToCanvasY: function (a) {
    return Math.floor(a * CANVAS_SIZE.height / Panel.SIZE.height)
  },
  mapCanvasToPanelX: function (a) {
    return Math.floor(a * Panel.SIZE.width / CANVAS_SIZE.width)
  },
  mapCanvasToPanelY: function (a) {
    return Math.floor(a * Panel.SIZE.height / CANVAS_SIZE.height)
  }
});
Panel.SIZE = new Size(106, 79);
Panel.MIN_WIDTH = 27;
Panel.MAX_WITH_CALENDAR_WIDTH = 308;
Panel.MAX_WITHOUT_CALENDAR_WIDTH = 158;
var PanelMoving = Class.create({
  offsetX: null, offsetY: null, move: null, up: null, initialize: function (b, d, c, a) {
    this.offsetX = b;
    this.offsetY = d;
    this.move = c;
    this.up = a
  }
});
var PanelCalendar = Class.create({
  elem: null, date: null, visible: false, initialize: function (a, b) {
    this.elem = a;
    this.date = null;
    this.visible = b;
    if (this.visible) {
      this.build(getTodayDate());
      this.elem.$$$(".nav .prev").observe("click", function () {
        var c = this.date;
        c.setDate(c.getDate() - 3 * 7);
        this.build(c)
      }.bindAsEventListener(this));
      this.elem.$$$(".nav .next").observe("click", function () {
        var c = this.date;
        c.setDate(c.getDate() + 3 * 7);
        this.build(c)
      }.bindAsEventListener(this));
      this.elem.$$$(".nav .today").observe("click", function () {
        this.build(getTodayDate())
      }.bindAsEventListener(this))
    } else {
      this.elem.hide()
    }
  }, show: function () {
    this.elem.show()
  }, hide: function () {
    this.elem.hide()
  }, build: function (b) {
    this.date = new Date(b.getTime());
    var j = b.getFullYear();
    var f = b.getMonth();
    b = this.getStartDate(b);
    this.elem.$$("title").update2(formatYearMonth(j, f));
    var m = this.elem.$$$("tbody");
    m.removeAllChildren();
    var k = getTodayDate();
    for (var l = 0; l < 4; ++l) {
      var i = new Element("tr");
      for (var g = 0; g < 7; ++g) {
        var c = new Element("td");
        var a = new Element("div");
        c.appendChild(a);
        a.appendChild(document.createTextNode(b.getDate()));
        c.addClassName("day");
        a.addClassName("day" + formatDate(b));
        if (b.getMonth() == f) {
          c.addClassName("thisMonth")
        }
        switch (b.getDay()) {
          case 0:
            c.addClassName("sunday");
            break;
          case 6:
            c.addClassName("saturday");
            break
        }
        if (isSameDay(b, k)) {
          c.addClassName("today")
        }
        var e = getHolidayName(b);
        if (e) {
          c.addClassName("holiday");
          c.setAttribute("title", e)
        }
        i.appendChild(c);
        b.setDate(b.getDate() + 1)
      }
      m.appendChild(i)
    }
    this.refresh.bind(this).delay(0.1)
  }, rebuild: function () {
    if (this.visible) {
      this.build(this.date)
    }
  }, mark: function (a) {
    if (!this.visible) {
      return
    }
    var b = $("dueEntriesForm");
    b.year.value = a.getFullYear();
    b.month.value = a.getMonth() + 1;
    b.day.value = a.getDate();
    b.days.value = 4 * 7;
    new AjaxUtil().get("/api/entry/dueEntries", b, this.update.bind(this, a), error)
  }, update: function (d, b) {
    if (!isSameDay(this.getStartDate(this.date), d)) {
      return
    }
    this.elem.select(".day").each(function (e) {
      var f = e.$$$("div");
      f.removeClassName("due");
      f.removeClassName("overdue");
      f.removeClassName("todaydue");
      f.stopObserving("click");
      f.title = f.up("td").getAttribute("title")
    });
    var a = getTodayDate().getTime();
    var c = null;
    b.each(function (h) {
      var e = h.date;
      var g = this.elem.$$("day" + e);
      var f = parseDate(e).getTime();
      g.addClassName(f == a ? "todaydue" : f < a ? "overdue" : "due");
      g.title += (g.title.length != 0 ? " " : "") + h.subject;
      if (e != c) {
        g.observe("click", this.handler.bindAsEventListener(this))
      }
      c = e
    }.bind(this))
  }, handler: function (b) {
    var a = b.element();
    a.classNames().find(function (d) {
      return d.match(/day(\d+)-(\d+)-(\d+)/)
    });
    var c = g_userId == 0 ? g_canvasId : null;
    getSearch().searchByDueDate(RegExp.$1, RegExp.$2 - 1, RegExp.$3, c)
  }, refresh: function () {
    if (this.visible) {
      this.mark(this.getStartDate(this.date))
    }
  }, getStartDate: function (a) {
    var b = new Date(a.getTime());
    while (b.getDay() != 0) {
      b.setDate(b.getDate() - 1)
    }
    b.setDate(b.getDate() - 7);
    return b
  }
});
var PanelResizing = Class.create({
  offsetX: null, move: null, up: null, initialize: function (b, c, a) {
    this.offsetX = b;
    this.move = c;
    this.up = a
  }
});
var DialogsStub = Class.create({
  initialize: function () {
    this.dialogs = null;
    this.elem = null;
    this.func = null
  }, isDialogVisible: function () {
    return this.dialogs && this.dialogs.isDialogVisible()
  }, bind: function (a) {
    if (this.dialogs) {
      a(this.dialogs)
    } else {
      this.func = a;
      if (!this.elem) {
        this.elem = new Element("div");
        document.body.appendChild(this.elem);
        new Ajax.Request("/canvashelper/dialogs", {
          method: "get",
          parameters: {canvasId: g_canvasId},
          onSuccess: function (c) {
            this.elem.innerHTML = c.responseText;
            var b = new Element("script", {
              type: "text/javascript",
              src: g_assetBaseURL + "/script/canvas/dialog.js.1410232740"
            });
            document.body.appendChild(b)
          }.bind(this)
        })
      }
    }
  }, getDialogs: function () {
    return this.dialogs
  }, loadDialogs: function () {
    this.dialogs = new Dialogs();
    this.func(this.dialogs);
    this.func = null
  }
});
var g_dialogsStub = new DialogsStub();
var Popup = Class.create({
  canvasView: null,
  elem: null,
  dueCalendar: null,
  userList: null,
  canvasList: null,
  iconList: null,
  initialize: function (a) {
    this.canvasView = a;
    this.elem = $("popupBg");
    this.elem.observe("click", function (d) {
      var b = d.pointerX();
      var f = d.pointerY();
      var c = this.elem.select(".popup");
      for (var e = 0; e < c.length; ++e) {
        if (c[e].visible() && Position.within(c[e], b, f)) {
          return
        }
      }
      c.each(Element.hide);
      this.hide()
    }.bindAsEventListener(this));
    Event.observe(window, "resize", this.adjustBgSize.bindAsEventListener(this))
  },
  showDueCalendar: function (c, b, d, a) {
    if (!this.dueCalendar) {
      this.dueCalendar = new DueCalendar(this, $("dueCalendar"))
    }
    return this.dueCalendar.show(c, b, d, a)
  },
  showUserList: function (b, a, c) {
    return this.showUserList2(a, c, function (e, d) {
      if (!confirm(new Template(MESSAGES["confirm.sendEntry"]).evaluate({name: d}))) {
        return
      }
      sendEntry(b.id, e)
    }, function () {
      g_dialogsStub.bind(function (d) {
        d.showSendEntryDialog(b)
      })
    })
  },
  showUserList2: function (a, d, b, c) {
    if (!this.userList) {
      this.userList = new UserList(this, $("userList"))
    }
    return this.userList.show(a, d, b, c)
  },
  showCanvasList: function (b, a, c) {
    if (!this.canvasList) {
      this.canvasList = new CanvasList(this, $("canvasList"))
    }
    return this.canvasList.show(b, a, c)
  },
  showIconList: function (a, c, b) {
    if (!this.iconList) {
      this.iconList = new IconList(this, $("iconList"))
    }
    return this.iconList.show(a, c, b)
  },
  show: function () {
    this.adjustBgSize();
    this.elem.show();
    if (Prototype.Browser.IE6) {
      $(document.body).select("select").each(function (a) {
        a.setStyle({visibility: "hidden"})
      })
    }
  },
  hide: function () {
    this.elem.hide();
    if (Prototype.Browser.IE6) {
      $(document.body).select("select").each(function (a) {
        a.setStyle({visibility: "visible"})
      })
    }
  },
  adjustPosition: function (d, a, f) {
    if (this.canvasView) {
      var b = d.getDimensions();
      var c = this.canvasView.getDimensions();
      var e = new Point(Math.min(a, c.width - b.width - 10), Math.min(f, c.height - b.height - 10));
      d.setStyle({left: e.x + "px", top: e.y + "px"})
    } else {
      d.setStyle({left: a + "px", top: f + "px"})
    }
  },
  adjustBgSize: function () {
    this.elem.setStyle({width: getWindowWidth() + "px", height: getWindowHeight() + "px"})
  },
  isPopupVisible: function () {
    return this.elem.visible()
  }
});
var DueCalendar = Class.create(MiniCalendar, {
  popup: null, setFunc: null, initialize: function (b, d) {
    this.popup = b;
    var a = function (h, f, g) {
      this.setFunc(h, f, g);
      this.hide()
    }.bind(this);
    MiniCalendar.prototype.initialize.apply(this, [d, a, null]);
    var e = d.$$("miniCalendarFooter");
    e.innerHTML = '<span class="none">' + MESSAGES["canvas.dueCalendar.none"] + "</span>";
    var c = e.$$("none");
    c.observe("mouseover", function () {
      c.addClassName("selected")
    });
    c.observe("mouseout", function () {
      c.removeClassName("selected")
    });
    c.observe("click", function () {
      this.setFunc();
      this.hide()
    }.bindAsEventListener(this))
  }, show: function (c, b, e, a) {
    c = c || getTodayDate();
    this.setDate(c, c);
    this.setFunc = a;
    var d = this.element();
    d.show();
    this.popup.show();
    this.popup.adjustPosition(d, b, e);
    this.layoutBackground()
  }, hide: function () {
    this.element().hide();
    this.popup.hide()
  }
});
var UserList = Class.create({
  popup: null, elem: null, menu: null, initialize: function (b, c) {
    this.popup = b;
    this.elem = c;
    this.menu = new Menu(c);
    if (Prototype.Browser.IE6) {
      var a = this.elem.$$("otherUsers");
      a.observe("mouseover", function () {
        a.addClassName("otherUsersHover")
      });
      a.observe("mouseout", function () {
        a.removeClassName("otherUsersHover")
      })
    }
  }, show: function (b, f, c, e) {
    var d = this.elem.$$("users");
    new AjaxUtil().get("/api/user/directBuddies", null, function (h) {
      d.innerHTML = "";
      h.each(function (i, l) {
        if (l != 0) {
          var j = new Element("div");
          j.addClassName("menuSeparator");
          d.appendChild(j)
        }
        var k = new Element("div");
        k.addClassName("user");
        k.innerHTML = '<img class="icon" src="/user/icon/' + i.id + '" /><img src="' + g_imageAssetBaseURL.escapeHTML() + '/image/iconseparator.png" />' + i.name.escapeHTML();
        d.appendChild(k);
        k.observe("click", function () {
          c(i.id, i.name);
          this.hide()
        }.bindAsEventListener(this));
        if (Prototype.Browser.IE6) {
          k.observe("mouseover", function () {
            k.addClassName("userHover")
          });
          k.observe("mouseout", function () {
            k.removeClassName("userHover")
          })
        }
      }, this);
      if (Prototype.Browser.IE6) {
        var g = this.elem.$$("usersScroll");
        if (g.getHeight() > 300) {
          g.setStyle({height: "300px"})
        }
      }
      this.popup.adjustPosition(this.elem, b, f);
      this.menu.layout()
    }.bind(this), function () {
    });
    d.innerHTML = "Loading ...";
    var a = this.elem.$$("otherUsers");
    if (e) {
      a.stopObserving("click");
      a.observe("click", function () {
        this.hide();
        e()
      }.bindAsEventListener(this));
      a.show()
    } else {
      a.hide()
    }
    if (Prototype.Browser.IE6) {
      this.elem.$$("usersScroll").setStyle({height: "auto"})
    }
    this.elem.show();
    this.popup.show();
    this.popup.adjustPosition(this.elem, b, f);
    this.menu.layout()
  }, hide: function () {
    this.elem.hide();
    this.popup.hide()
  }
});
var CanvasList = Class.create({
  popup: null, elem: null, menu: null, initialize: function (a, b) {
    this.popup = a;
    this.elem = b;
    this.menu = new Menu(b)
  }, show: function (c, a, d) {
    var b = this.elem.$$("canvases");
    new AjaxUtil().get("/api/canvas/getWritables", null, function (f) {
      b.innerHTML = "";
      f.each(function (h, k) {
        if (k != 0) {
          var g = new Element("div");
          g.addClassName("menuSeparator");
          b.appendChild(g)
        }
        var j = new Element("div");
        j.addClassName("canvas");
        var i = "<div>" + h.name.escapeHTML() + "</div>";
        if (h.groupId) {
          i += '<div class="group">at ' + h.group.escapeHTML() + "</div>"
        } else {
          if (h.ownerId != g_userId) {
            i += '<div class="owner">by ' + h.owner.escapeHTML() + "</div>"
          }
        }
        j.innerHTML = i;
        b.appendChild(j);
        j.observe("click", function () {
          if (!c.intimate || h.ownerId == g_userId || confirm(MESSAGES["confirm.copyPrivateEntry"])) {
            copyEntry(c, h.id)
          }
          this.hide()
        }.bindAsEventListener(this));
        if (Prototype.Browser.IE6) {
          j.observe("mouseover", function () {
            j.addClassName("canvasHover")
          });
          j.observe("mouseout", function () {
            j.removeClassName("canvasHover")
          })
        }
      }, this);
      if (Prototype.Browser.IE6) {
        var e = this.elem.$$("canvasesScroll");
        if (e.getHeight() > 300) {
          e.setStyle({height: "300px"})
        }
      }
      this.popup.adjustPosition(this.elem, a, d);
      this.menu.layout()
    }.bind(this), function () {
    });
    b.innerHTML = "Loading ...";
    if (Prototype.Browser.IE6) {
      this.elem.$$("canvasesScroll").setStyle({height: "auto"})
    }
    this.elem.show();
    this.popup.show();
    this.popup.adjustPosition(this.elem, a, d);
    this.menu.layout()
  }, hide: function () {
    this.elem.hide();
    this.popup.hide()
  }
});
var IconList = Class.create({
  popup: null, elem: null, func: null, initialize: function (a, d) {
    this.popup = a;
    this.elem = d;
    var f = function (h) {
      var g = h.element().classNames().grep(/icon[0-9]+/)[0].substring(4);
      this.func(parseInt(g, 10));
      this.hide()
    }.bindAsEventListener(this);
    var e = function (h) {
      var g = h.element();
      g.setStyle({backgroundPosition: g.getStyle("backgroundPosition").split(" ")[0] + " -24px"})
    }.bindAsEventListener(this);
    var c = function (h) {
      var g = h.element();
      g.setStyle({backgroundPosition: g.getStyle("backgroundPosition").split(" ")[0] + " -0px"})
    }.bindAsEventListener(this);
    this.elem.select(".icon").each(function (g) {
      g.observe("click", f);
      g.observe("mouseover", e);
      g.observe("mouseout", c)
    });
    var b = this.elem.$$("iconNone");
    b.observe("click", function () {
      this.func(0);
      this.hide()
    }.bindAsEventListener(this));
    if (Prototype.Browser.IE6) {
      b.observe("mouseover", function () {
        b.addClassName("iconNoneHover")
      });
      b.observe("mouseout", function () {
        b.removeClassName("iconNoneHover")
      })
    }
  }, show: function (a, c, b) {
    this.func = b;
    this.elem.show();
    this.popup.show();
    this.popup.adjustPosition(this.elem, a, c);
    this.layoutBackground()
  }, hide: function () {
    this.elem.hide();
    this.popup.hide()
  }, layoutBackground: function () {
    var a = this.elem.getDimensions();
    this.elem.$$("iconListBgBody").setStyle({
      width: Math.max(a.width - 7 * 2, 0) + "px",
      height: Math.max(a.height - 7, 0) + "px"
    });
    this.elem.$$("iconListBgLeft").setStyle({height: Math.max(a.height - 7, 0) + "px"});
    this.elem.$$("iconListBgRight").setStyle({height: Math.max(a.height - 7, 0) + "px"});
    this.elem.$$("iconListBgBottom").setStyle({width: Math.max(a.width - 7 * 2, 0) + "px"})
  }
});
var Search = Class.create({
  canvasView: null, result: null, initialize: function (a) {
    this.canvasView = a;
    this.result = $("searchResult");
    this.result.observe("click", function () {
      this.hide()
    }.bindAsEventListener(this))
  }, searchByDueDate: function (c, d, a, e) {
    var b = "due:" + formatDate(new Date(c, d, a));
    if (e) {
      b += " canvasId:" + e
    }
    return this.search(b)
  }, search: function (c) {
    var b = $("searchForm");
    b.condition.value = c;
    var a = this.canvasView.getDimensions();
    b.width.value = a.width;
    b.height.value = a.height;
    new AjaxUtil().get("/api/entry/search", b, this.searched.bind(this), error)
  }, searched: function (a) {
    leaveNewEntryMode();
    hideMovie();
    this.result.show();
    a.entries.each(function (b) {
      b.canvas = a.canvases.find(function (d) {
        return d.id == b.canvasId
      });
      var c = $("searchEntryTemplate").cloneNode(true);
      if (b.image || b.attachment) {
        c.$$("body").setStyle({padding: "0px 15px"})
      }
      if (!b.image) {
        c.$$("image").remove()
      }
      if (b.image || !b.attachment) {
        c.$$("attachment").remove()
      }
      this.result.appendChild(c);
      Entry.bind(b, c);
      c.removeAttribute("id");
      b.fill();
      b.updatePosition();
      c.observe("click", this.showEntry.bindAsEventListener(this));
      c.show();
      b.updateBackgroundSize()
    }.bind(this))
  }, hide: function () {
    this.result.innerHTML = "";
    this.result.hide();
    showMovie()
  }, showEntry: function (b) {
    var a = getEntryElemFromChild(b.element()).entry;
    if (a.canvasId == g_canvasId) {
      var c = getEntryElemFromId(a.id);
      if (c) {
        ensureEntryElemVisible(c)
      }
    } else {
      if (a.canvas) {
        openAtTop(canvasPath(a.canvas) + "#e" + a.id)
      }
    }
    this.hide()
  }
});
var Mark = Class.create({
  initialize: function (d, c, b, a) {
    this.image = d;
    this.left = c;
    this.top = b;
    this.delay = a
  }
});
var EffectAction = Class.create({
  initialize: function (a, b) {
    this.name = a;
    this.marks = b
  }
});
var screwUpAction = new EffectAction("screwup", [new Mark(1, 0, 0, 80), new Mark(2, 0, 0, 80), new Mark(3, 0, 0, 80)]);
var flickAction = new EffectAction("flick", [new Mark(1, -30, 0, 80), new Mark(2, -35, 0, 80), new Mark(3, -80, -45, 80), new Mark(3, -150, -50, 80), new Mark(3, -250, -50, 80)]);
function effect(f, b, a) {
  var g = $("effect").cloneNode(true);
  $("canvas").appendChild(g);
  var d = f.positionedOffset();
  var i = f.getDimensions();
  var h = Math.min(i.width, i.height);
  d.left = Math.floor(d.left + (i.width - h) / 2);
  d.top = Math.floor(d.top + (i.height - h) / 2);
  g.setStyle({left: d.left + "px", top: d.top + "px", width: h + "px", height: h + "px", opacity: 1});
  function e(j) {
    return function () {
      var k = b.marks[j];
      g.src = g_imageAssetBaseURL + "/image/entry/" + b.name + "/" + f.entry.color + "/" + k.image + ".png";
      g.setStyle({left: (d.left + k.left) + "px", top: (d.top + k.top) + "px"});
      if (j < b.marks.length - 1) {
        e(j + 1).delay(k.delay / 1000)
      } else {
        c(10)()
      }
      if (j == 0) {
        if (Prototype.Browser.IE) {
          f.select(".bg *").each(function (l) {
            if (!l.hasClassName("bgCenter")) {
              l.hide()
            }
          })
        }
        f.setStyle({opacity: 0.5})
      } else {
        if (j == 1) {
          f.hide()
        }
      }
    }
  }

  function c(j) {
    return function () {
      $("effect").setStyle({opacity: j / 10});
      if (j != 0) {
        c(j - 1).delay(0.05)
      } else {
        g.remove();
        a()
      }
    }
  }

  g.show();
  e(0)()
}
var Social = Class.create({
  elem: null, facebookAppId: null, initialize: function (b, a) {
    this.elem = b;
    this.facebookAppId = a;
    this.elem.observe("mouseover", this.show.bindAsEventListener(this));
    this.elem.observe("mouseout", function (c) {
      if (!Position.within(this.elem, c.pointerX(), c.pointerY())) {
        this.hide()
      }
    }.bindAsEventListener(this))
  }, showImmediate: function () {
    createLikeButton(this.facebookAppId);
    this.elem.setStyle({width: Social.MAX_WIDTH + "px"})
  }, show: function () {
    createLikeButton(this.facebookAppId);
    var a = this.elem.getWidth();
    var b = function () {
      this.elem.setStyle({width: a + "px"});
      if (a < Social.MAX_WIDTH) {
        a = Math.min(a + Social.STEP, Social.MAX_WIDTH);
        b.delay(0.05)
      }
    }.bind(this);
    b()
  }, hide: function () {
    var a = this.elem.getWidth();
    var b = function () {
      this.elem.setStyle({width: a + "px"});
      if (a > Social.MIN_WIDTH) {
        a = Math.max(a - Social.STEP, Social.MIN_WIDTH);
        b.delay(0.05)
      }
    }.bind(this);
    b.delay(0.5)
  }
});
Social.MAX_WIDTH = 276;
Social.MIN_WIDTH = 26;
Social.STEP = 20;
function createLikeButton(b) {
  var c = "facebook-jssdk";
  if (document.getElementById(c)) {
    return
  }
  var a = document.createElement("script");
  a.id = c;
  a.src = "//connect.facebook.net/" + getLocale() + "/all.js#xfbml=1&appId=" + b;
  document.body.appendChild(a)
};