'use strict';

define([
  "jquery",
  "velocity",
  "hammer",
  'vendor/materialize/bin/date_picker/picker',
  'vendor/materialize/bin/date_picker/picker.date',
  'vendor/materialize/bin/cards',
  'vendor/materialize/bin/character_counter',
  'vendor/materialize/bin/collapsible',
  'vendor/materialize/bin/dropdown',
  'vendor/materialize/bin/forms',
  'vendor/materialize/bin/leanModal',
  'vendor/materialize/bin/materialbox',
  'vendor/materialize/bin/parallax',
  'vendor/materialize/bin/pushpin',
  'vendor/materialize/bin/scrollspy',
  'vendor/materialize/bin/sideNav',
  'vendor/materialize/bin/slider',
  'vendor/materialize/bin/tabs',
  //'vendor/materialize/bin/tooltip',
  'vendor/materialize/bin/buttons',
  'vendor/materialize/bin/jquery.easing.1.3',
  'vendor/materialize/bin/transitions',
  'vendor/materialize/bin/animation',
  'vendor/materialize/bin/waves'
], function($, h, v){
  Hammer = h;
  Vel = v;

  return {
    guid : function() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      };
    },
    elementOrParentIsFixed : function(element) {
      var $element = $(element);
      var $checkElements = $element.add($element.parents());
      var isFixed = false;
      $checkElements.each(function(){
        if ($(this).css("position") === "fixed") {
          isFixed = true;
          return false;
        }
      });
      return isFixed;
    },
    scrollFire : function(options) {

      var didScroll = false;

      window.addEventListener("scroll", function () {
        didScroll = true;
      });

      // Rate limit to 100ms
      setInterval(function () {
        if (didScroll) {
          didScroll = false;

          var windowScroll = window.pageYOffset + window.innerHeight;

          for (var i = 0; i < options.length; i++) {
            // Get options from each line
            var value = options[i];
            var selector = value.selector,
              offset = value.offset,
              callback = value.callback;

            var currentElement = document.querySelector(selector);
            if (currentElement !== null) {
              var elementOffset = currentElement.getBoundingClientRect().top + document.body.scrollTop;

              if (windowScroll > (elementOffset + offset)) {
                if (value.done != true) {
                  var callbackFunc = new Function(callback);
                  callbackFunc();
                  value.done = true;
                }
              }
            }
          }
        }
      }, 100);
    },

    toast : function (message, displayLength, className, completeCallback) {
      className = className || "";

      var container = document.getElementById('toast-container');

      // Create toast container if it does not exist
      if (container === null) {
        // create notification container
        var container = document.createElement('div');
        container.id = 'toast-container';
        document.body.appendChild(container);
      }

      // Select and append toast
      var newToast = createToast(message);
      container.appendChild(newToast);

      newToast.style.top = '35px';
      newToast.style.opacity = 0;

      // Animate toast in
      Vel(newToast, { "top" : "0px", opacity: 1 }, {duration: 300,
        easing: 'easeOutCubic',
        queue: false});

      // Allows timer to be pause while being panned
      var timeLeft = displayLength;
      var counterInterval = setInterval (function(){


        if (newToast.parentNode === null)
          window.clearInterval(counterInterval);

        // If toast is not being dragged, decrease its time remaining
        if (!newToast.classList.contains('panning')) {
          timeLeft -= 20;
        }

        if (timeLeft <= 0) {
          // Animate toast out
          Vel(newToast, {"opacity": 0, marginTop: '-40px'}, { duration: 375,
            easing: 'easeOutExpo',
            queue: false,
            complete: function(){
              // Call the optional callback
              if(typeof(completeCallback) === "function")
                completeCallback();
              // Remove toast after it times out
              this[0].parentNode.removeChild(this[0]);
            }
          });
          window.clearInterval(counterInterval);
        }
      }, 20);



      function createToast(html) {

        // Create toast
        var toast = document.createElement('div');
        toast.classList.add('toast');
        if (className) {
          var classes = className.split(' ');

          for (var i = 0, count = classes.length; i < count; i++) {
            toast.classList.add(classes[i]);
          }
        }
        toast.innerHTML = html;

        // Bind hammer
        var hammerHandler = new Hammer(toast, {prevent_default: false});
        hammerHandler.on('pan', function(e) {
          var deltaX = e.deltaX;
          var activationDistance = 80;

          // Change toast state
          if (!toast.classList.contains('panning')){
            toast.classList.add('panning');
          }

          var opacityPercent = 1-Math.abs(deltaX / activationDistance);
          if (opacityPercent < 0)
            opacityPercent = 0;

          Vel(toast, {left: deltaX, opacity: opacityPercent }, {duration: 50, queue: false, easing: 'easeOutQuad'});

        });

        hammerHandler.on('panend', function(e) {
          var deltaX = e.deltaX;
          var activationDistance = 80;

          // If toast dragged past activation point
          if (Math.abs(deltaX) > activationDistance) {
            Vel(toast, {marginTop: '-40px'}, { duration: 375,
              easing: 'easeOutExpo',
              queue: false,
              complete: function(){
                if(typeof(completeCallback) === "function") {
                  completeCallback();
                }
                toast.parentNode.removeChild(toast);
              }
            });

          } else {
            toast.classList.remove('panning');
            // Put toast back into original position
            Vel(toast, { left: 0, opacity: 1 }, { duration: 300,
              easing: 'easeOutExpo',
              queue: false
            });

          }
        });

        return toast;
      }
    },
    fadeInImage :  function(selector){
      var element = $(selector);
      element.css({opacity: 0});
      $(element).velocity({opacity: 1}, {
        duration: 650,
        queue: false,
        easing: 'easeOutSine'
      });
      $(element).velocity({opacity: 1}, {
        duration: 1300,
        queue: false,
        easing: 'swing',
        step: function(now, fx) {
          fx.start = 100;
          var grayscale_setting = now/100;
          var brightness_setting = 150 - (100 - now)/1.75;

          if (brightness_setting < 100) {
            brightness_setting = 100;
          }
          if (now >= 0) {
            $(this).css({
              "-webkit-filter": "grayscale("+grayscale_setting+")" + "brightness("+brightness_setting+"%)",
              "filter": "grayscale("+grayscale_setting+")" + "brightness("+brightness_setting+"%)"
            });
          }
        }
      });
    },
    showStaggeredList : function(selector) {
      var time = 0;
      $(selector).find('li').velocity(
        { translateX: "-100px"},
        { duration: 0 });

      $(selector).find('li').each(function() {
        $(this).velocity(
          { opacity: "1", translateX: "0"},
          { duration: 800, delay: time, easing: [60, 10] });
        time += 120;
      });
    }
  };
});