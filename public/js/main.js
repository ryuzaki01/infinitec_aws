'use strict';
var App;
var Board;
var kendo;
var Vel;
var Hammer = {};
var Materialize = {};

define([
  "app",
  "kendo",
  "velocity",
  "hammer",
  "materialize",
], function(Application, Kendo, v, h, m) {
  kendo = Kendo;
  App = Application;
  Hammer = h;
  Materialize = m;
  var deviceAgent = navigator.userAgent.toLowerCase();
  var agentID = deviceAgent.match(/(iphone|ipod)/);
  if (agentID) {
    $("#body").css("min-height", "418px");
    setTimeout(function () {
      try { window.scrollTo(0, 1); } catch (e) { }
    }, 0);
  }
  App.init();
});