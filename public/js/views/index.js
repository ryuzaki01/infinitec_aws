'use strict';

define([], function () {
  return {
    init: function () {
      console.log('Initializing');
      $('#create-file').on('submit', function(){
        $.ajax({
          method: "POST",
          url: "/api/group/create_memo",
          data: $('#create-file').serializeArray(),
          success: function(res){
            if(res.success){
              window.location.reload();
            }
          },
          dataType: 'json'
        });
        return false;
      });
    },
    view: function () {
      console.log('viewing');
    },
    unload: function () {
      console.log('Leaving');
    }
  }
});
