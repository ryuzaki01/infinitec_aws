'use strict';

define([], function () {
  return {
    init: function () {
      console.log('Initializing User Management');
      $('#template-select').on('change',function(){
        var optionSelected = $("option:selected", this);
        $('#template-preview').css('background-image', "url('"+optionSelected.attr('data-image')+"')");
      });

      $('#background-select').on('change',function(){
        var optionSelected = $("option:selected", this);
        $('#background-preview').css('background-image', "url('"+optionSelected.attr('data-image')+"')");
      });

      $('#upload-background').on('click', function(){
        $('#background-file-upload').trigger('click');
      });

      $('#background-file-upload').on('change',function(e){
        var i, len = this.files.length, file;
        for (i = 0 ; i < len; i++ ) {
          file = this.files[i];

          if (!file.type.match(/image.*/)) {
            alert('Error "'+file.type+'" : Allowed file type JPEG,JPG,BMP,GIF,PNG');
            return false;
          }
        }
        $('#background-upload').submit();
      });

      $('#background-upload').on('submit', function(){
        $('#loading').show();
        var formdata = new FormData();
        var files = $('#background-file-upload')[0].files;
        $.each(files, function(key, value)
        {
          formdata.append(key, value);
        });

        $.ajax({
          url: "/api/upload/background",
          type: "POST",
          data:  formdata,
          cache: false,
          processData: false,
          contentType: false,
          success: function (res) {
            $('#loading').hide();
            if(res.success){
              alert('Upload success');
              $('#background-select').append('<option value="'+res.background._id+'" data-image="'+res.background.url+'">'+res.background.name+'</option>');
            } else {
              alert('Upload failed : '+ res.error);
            }
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
            $('#loading').hide();
            alert('Upload failed : '+errorThrown);
            console.log('ERRORS: ' + textStatus);
          }
        });
        return false;
      });

      $('#group-select').on('change', function(){
        $.ajax({
          method: "POST",
          url: "/api/user/getall",
          data: {group: $("option:selected", this).val()},
          success: function(res){
            if(res.success){
              $('#group-member-select').html('');
              for(var i in res.users){
                $('#group-member-select').append('<option value="'+res.users[i]._id+'">'+ res.users[i].name.first +' '+res.users[i].name.last+'</option>');
              }
            }
          },
          dataType: 'json'
        });
      });

      $('#create-group').on('submit', function(){
        $.ajax({
          method: "POST",
          url: "/api/group/insert",
          data: $('#create-group').serializeArray(),
          success: function(res){
            if(res.success){
              $('#group-select').append('<option value="'+res.group._id+'">'+ res.group.name +'</option>');
              $('#create-group')[0].reset();
              alert('Group created');
            }
          },
          dataType: 'json'
        });
        return false;
      });

      $('#create-user').on('submit', function(){
        $.ajax({
          method: "POST",
          url: "/api/user/insert",
          data: $('#create-user').serialize(),
          success: function(res){
            if(res.success){
              console.log(res.user);
              switch(res.user.level){
                case 'teacher':
                  $('#teacher-select').append('<option value="'+res.user._id+'">'+ res.user.name.first+' '+ res.user.name.last +'</option>');
                break;
                case 'student':
                  $('#student-select').append('<option value="'+res.user._id+'">'+ res.user.name.first+' '+ res.user.name.last +'</option>');
                break;
              }
              $('#create-user')[0].reset();
              alert('User created');
            }
          },
          dataType: 'json'
        });
        return false;
      });
    }
  }
});