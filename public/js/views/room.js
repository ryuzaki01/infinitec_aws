'use strict';

define([], function () {
  return {
    init: function () {
      console.log('Room inited');
      $('.group-container .group-checkbox').each(function () {
        var groupCheckboxes = $(this);
        groupCheckboxes.click(function () {
          var isChecked = groupCheckboxes.is(':checked') ? true : false;
          var $userCheckboxes = groupCheckboxes.parent().find('.aws-users .user-checkbox');

          if (isChecked) {
            $userCheckboxes.attr('checked', 'checked');
          } else {
            $userCheckboxes.removeAttr('checked');
          }
        });
      });

      $('.user-checkbox').each(function () {
        var userCheckbox = $(this);

        userCheckbox.click(function () {
          var $this = $(this);
          var isChecked = $this.is(':checked') ? true : false;
          var $parentGroupCheckbox = $this.parent().parent().find('.group-checkbox');

          if (isChecked) {
            var allChecked = true;

            $parentGroupCheckbox.parent().find('.user-checkbox').each(function () {
              if (!$(this).is(':checked')) {
                allChecked = false;
              }
            });

            if(allChecked){
              $parentGroupCheckbox.attr('checked', 'checked');
            }
          } else {
            $parentGroupCheckbox.removeAttr('checked');
          }
        });
      });
    }
  };
});