'use strict';
define(["kendo", "board", "ro", "dt"], function (kendo, b) {
  return {
    init: function (e) {
      $('.button-collapse').sideNav({
          closeOnClick: false // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
      );
      Board = b;

      $(document).ready(function () {
        $('#toast-container').remove();

        $('.modal-trigger').leanModal();

        $(document).on( 'scroll', function(e){
          if($(document).scrollTop() > 30){
            $('.fixed-action-btn').css('top', '0px');
          } else {
            $('.fixed-action-btn').css('top', '60px');
          }
        });

        $(document).on('mouseenter', '.fixed-action-btn', function (e) {
          $(this).css({
            'height': '230px',
            'width': '100px'
          }).find('ul').css('left', '0px');
        });

        $(document).on('mouseleave', '.fixed-action-btn', function (e) {
          $(this).css({
            'height': '55px',
            'width': '55px'
          }).find('ul').css('left', '-300px');
        });

        $('#sticky-text').kendoEditor({
          tools:  [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "createLink",
            "unlink",
            "insertImage",
            "insertFile",
            "cleanFormatting",
            "fontSize",
            "foreColor",
            "backColor"
          ]
        });

        $('#edit-sticky-text').kendoEditor({
          tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "createLink",
            "unlink",
            "insertImage",
            "insertFile",
            "cleanFormatting",
            "fontSize",
            "foreColor",
            "backColor"
          ]
        });

        $('#sticky-create').on('submit', function(){
          var editor = $("#sticky-text").data("kendoEditor");
          Board.createSticky(editor.value(), $("input[name='style.fill']:checked").val(), $('#sticky-tag').val());
          editor.value('');
          $('#sticky-create')[0].reset();
          $('#sticky-post').closeModal();
          return false;
        });

        $('#update-sticky').on('click', function(){
          var editor = $("#edit-sticky-text").data("kendoEditor");
          Board.updateSticky($('#edit-id').val(), editor.value(), $("input[name='style.fill']:checked").val(), $('#edit-sticky-tag').val());
          editor.value('');
          $('#sticky-editor')[0].reset();
          $('#sticky-update').closeModal();
          return false;
        });

        var PHOTO_MAX_WIDTH = 512;
        var PHOTO_MAX_HEIGHT= 512;

        $('#photo-upload-input').on('change', function(){
          var reader = new FileReader();
          var canvas = document.getElementById('photo-preview');
          var ctx = canvas.getContext('2d');
          reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
              var ratio = 1.0;
              if(img.width > PHOTO_MAX_WIDTH)
                ratio = PHOTO_MAX_WIDTH / this.width;
              else if(this.height > PHOTO_MAX_HEIGHT)
                ratio = PHOTO_MAX_HEIGHT / this.height;

              //draw the image on the rotated context
              var resizedPhoto =  {
                width:  ratio * this.width,
                height: ratio * this.height
              };

              canvas.width = resizedPhoto.width;
              canvas.height = resizedPhoto.height;

              ctx.drawImage(img,0,0, resizedPhoto.width, resizedPhoto.height);
            };
            img.src = event.target.result;
          };
          reader.readAsDataURL($('#photo-upload-input')[0].files[0]);
        });

        $('#sticky-photo-create').on('submit', function(){
          $('#loading').show();
          var canvas = document.getElementById('photo-preview');
          var imageData = canvas.toDataURL("image/png");

          $.ajax({
            method: "POST",
            url: "/api/upload/element",
            data : {image: imageData},
            dataType: 'json',
            success: function(res){
              if(res.success){
                Board.createPhoto(res.data, canvas.width, canvas.height);
              } else {
                Board.createPhoto(imageData, canvas.width, canvas.height);
              }
              canvas.getContext('2d').clearRect(0,0, canvas.width, canvas.height);
              $('#image-post').closeModal();
              $('#loading').hide();
            },
            errro: function(){
              $('#loading').hide();
            }
          });
          return false;
        });

   /*     var canvasContainer = $('#canvasContainer');

        canvasContainer.on('mousedown touchstart', function(e){
          e.preventDefault();
          Board.panning = true;
          Board.startPanPoint = {
            y: Board.toCanvasY(e),
            x: Board.toCanvasX(e)
          }
        });

        canvasContainer.on('mousemove touchmove', function(e){
          if(Board.panning) {

            var offset = canvasContainer.offset();
            var scale = 1.0;
            //relative mouse x,y
            var mouseX = Board.toCanvasX(e) - Board.startPanPoint.x;
            var mouseY = Board.toCanvasY(e) - Board.startPanPoint.y;
            canvasContainer.css({
              '-webkit-transform': 'translate3d('+mouseX+'px,'+mouseY+'px, 0px)'
            });
            e.preventDefault();
            e.stopPropagation();
            return true;
          }
        });

        canvasContainer.on('mouseleave', function(e){
          canvasContainer.trigger('mouseup');
        });

        canvasContainer.on('mouseup touchend', function(e){
          Board.panning = false;
          Board.startPanPoint = {
            x: 0,
            y: 0
          }
        });*/

      });

      $("#panel-control").kendoPanelBar({});

      $('#chatmsg').keypress(function (ev) {
        if (ev.which === 13) {
          if ($(this).val() == '')
            return;

          ev.preventDefault();
          App.sendMsg();
        }
      });

      if (!$('#box-tab').data('kendoTabStrip')) {
        $("#box-tab").kendoTabStrip({
          animation: {
            open: {
              effects: ""
            }
          }
        });
      }

      var drawWindow = $("#draw-window"),
        drawWindowUndo = $("#draw-but").bind("click", function () {
          drawWindow.data("kendoWindow").center().open();
        });

      if (!drawWindow.data("kendoWindow")) {
        drawWindow.kendoWindow({
          title: "Drawing Toolbar",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }


      var optWindow = $("#opt-window"),
        optWindowUndo = $("#prop-but").bind("click", function () {
          optWindow.data("kendoWindow").center().open();
        });

      if (!optWindow.data("kendoWindow")) {
        optWindow.kendoWindow({
          title: "Option",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var textboxWindow = $("#textbox-window");

      if (!textboxWindow.data("kendoWindow")) {
        textboxWindow.kendoWindow({
          actions: ["Minimize", "Close"],
          title: "Text Toolbar",
          visible: false,
          resizable: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var saveWindow = $("#save-window"),
        saveWindowUndo = $("#save-but").bind("click", function () {
          saveWindow.data("kendoWindow").open();
        }),
        saveAsButton = $('#save-as-but').bind("click", Board.cloneFile);

      if (!saveWindow.data("kendoWindow")) {
        saveWindow.kendoWindow({
          actions: ["Close"],
          title: "Save As",
          visible: false,
          resizable: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var loadWindow = $("#load-window"),
        loadWindowUndo = $("#load-but").bind("click", function () {
          Board.loadSession();
          loadWindow.data("kendoWindow").open();
        });

      if (!loadWindow.data("kendoWindow")) {
        loadWindow.kendoWindow({
          actions: ["Close"],
          title: "Load File",
          visible: false,
          resizable: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var uploadImageWindow = $("#upload-image-window"),
        uploadImageUndo = $("#image-upload-but").bind("click", function () {
          chartWindow.data("kendoWindow").close();
          tabelWindow.data("kendoWindow").close();
          stampWindow.data("kendoWindow").close();
          uploadFileWindow.data("kendoWindow").close();
          uploadImageWindow.data("kendoWindow").center().open();
        });

      if (!uploadImageWindow.data("kendoWindow")) {
        uploadImageWindow.kendoWindow({
          title: "Image Toolbar",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var uploadFileWindow = $("#upload-file-window"),
        uploadFileUndo = $("#file-upload-but").bind("click", function () {
          uploadImageWindow.data("kendoWindow").close();
          chartWindow.data("kendoWindow").close();
          tabelWindow.data("kendoWindow").close();
          stampWindow.data("kendoWindow").close();
          uploadFileWindow.data("kendoWindow").center().open();
        });

      if (!uploadFileWindow.data("kendoWindow")) {
        uploadFileWindow.kendoWindow({
          title: "Attachment Toolbar",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var stampWindow = $("#stamp-window"),
        stampUndo = $("#stamp-but").bind("click", function () {
          uploadImageWindow.data("kendoWindow").close();
          uploadFileWindow.data("kendoWindow").close();
          chartWindow.data("kendoWindow").close();
          tabelWindow.data("kendoWindow").close();
          stampWindow.data("kendoWindow").center().open();
        });

      if (!stampWindow.data("kendoWindow")) {
        stampWindow.kendoWindow({
          title: "Stamp Toolbar",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var tabelWindow = $("#table-window"),
        tabelUndo = $("#table-but").bind("click", function () {
          uploadImageWindow.data("kendoWindow").close();
          uploadFileWindow.data("kendoWindow").close();
          stampWindow.data("kendoWindow").close();
          chartWindow.data("kendoWindow").close();
          tabelWindow.data("kendoWindow").center().open();
        });

      if (!tabelWindow.data("kendoWindow")) {
        tabelWindow.kendoWindow({
          title: "Table Toolbar",
          visible: false,
          animation: {
            open: {
              effects: "none"
            }
          }
        });
      }

      var chartWindow = $("#chart-window"),
        chartUndo = $("#chart-but").bind("click", function () {
          uploadImageWindow.data("kendoWindow").close();
          uploadFileWindow.data("kendoWindow").close();
          stampWindow.data("kendoWindow").close();
          chartWindow.data("kendoWindow").center().open();
        });

      if (!chartWindow.data("kendoWindow")) {
        chartWindow.kendoWindow({
          title: "Chart Toolbar",
          visible: false,
          resizable: false,
          animation: {
            open: {
              effects: "fade"
            }
          }
        });
      }

      if (!$('#upload-image-tab').data('kendoTabStrip')) {
        $("#upload-image-tab").kendoTabStrip({
          animation: {
            open: {
              effects: "expand:vertical"
            }
          }
        });
      }

      if (!$('#upload-file-tab').data('kendoTabStrip')) {
        $("#upload-file-tab").kendoTabStrip({
          animation: {
            open: {
              effects: "expand:vertical"
            }
          }
        });
      }

      if (!$('#stamp-tab').data('kendoTabStrip')) {
        $("#stamp-tab").kendoTabStrip({
          animation: {
            open: {
              effects: "expand:vertical"
            }
          }
        });
      }

      if (!$('#chart-tab').data('kendoTabStrip')) {
        $("#chart-tab").kendoTabStrip({
          animation: {
            open: {
              effects: "expand:vertical"
            }
          }
        });
      }

      $("#stamp-color-picker").kendoColorPalette({
        palette: "basic",
        change: function (e) {
          Board.setFill(e);
          $('#stamp-cursor').css('color', Board.fill);
        }
      });

      $('#preview').hide();

      $('#create-series-but').on('click', function () {
        if (!Board.tempChart.data) {
          Board.tempChart.data = [];
        }

        Board.tempChart.data.push({label: $('#chart-series-label').val(), value: $('#chart-series-value').val()});
        $('#chart-series-list').append('<tr><td>' + $('#chart-series-label').val() + '</td><td>' + $('#chart-series-value').val() + '</td>');
      });

      $('#delete-series-but').on('click', function () {
        if (!Board.tempChart.data) {
          alert('Chart is already empty');
          return;
        }

        Board.tempChart.data.pop();
        $('#chart-series-list:nth-child(' + (Board.tempChart.data.length + 1) + ')').remove();
      });

      /*$(document).tooltip({
        items: '#canvasContainer > div[class^="obj-"], .layerbox',
        position: {my: "center top", at: "center bottom"},
        content: function () {
          var element = $(this);
          if (element.is('#canvasContainer > div[class^="obj-"]')) {
            var author = 'Administrator';
            var date = new Date();
            var time = date.toString();
            return "<p>Author: " + author + "</p><p>Last Update :" + time + "</p>";
          }
          if (element.is('.layerbox')) {
            var author = 'Administrator';
            var date = $(this).attr('title');
            return "<p>Author: " + author + "</p><p>Last Update :" + time + "</p>";
          }
        }
      });
*/
      this.initContextMenu(true);

      console.log('Drawer inited..');

      Board.init();
    },

    initContextMenu: function(dev){
      $.contextMenu({
        selector: '#canvasContainer > div[class^="obj-"]',
        callback: function (key, options) {
          switch (key) {
            case 'edit':
              if ($(this).hasClass('obj-text') || $(this).hasClass('obj-comic') || $(this).hasClass('obj-table')) {
                console.log('editing.. !');
                Board.editTextArea($(this).attr('id'));
              }
              break;
            case 'copy':
              if (confirm('Copy selected object?')) {
                var newCtxId = (new Date).getTime();
                var cloneObject = $(this).clone();

                var cloneElement = Board.elementById[$(this).attr('id')];
                cloneElement.id = newCtxId;
                cloneElement.persistentState = 'new';

                Board.createObject(cloneElement);

                App.socket.emit('cm_new', cloneElement);

                Board.saveCanvas();
              } else {

              }
              break;
            case 'delete':
              if (confirm('Delete selected object?')) {
                Board.removeElement($(this).attr('id'));
                $(this).find('div').remove();
                $(this).remove();
                App.socket.emit('cm_delete', {
                  id: $(this).attr('id'),
                  groupid: Board.groupid,
                  page: Board.page
                });
                Board.saveCanvas();
              } else {

              }
              break;
            case 'btf':
              $(this).css('z-index', $(this).css('z-index') + 1);
              break;
            case 'stb':
              $(this).css('z-index', $(this).css('z-index') - 1);
              break;
            case 'bao':
              Board.layerOrder('bao');
              break;
            case 'suo':
              Board.layerOrder('suo');
              break;
            default:
              break;
          }
        },
        items: {
          "edit": {name: "Edit", icon: "edit", accesskey: "e"},
          "copy": {name: "Copy", icon: "copy", accesskey: "c"},
          "delete": {name: "Delete", accesskey: "d", icon: "delete"},
          "sep2": "---------",
          "order": {
            "name": "Ordering",
            "accesskey": "o",
            "items": {
              "btf": {"name": "Bring to Front", "accesskey": "f"},
              "stb": {"name": "Send to Back", "accesskey": "b"},
              "sep3": "---------",
              "bao": {"name": "Bring above object", "accesskey": "a"},
              "suo": {"name": "Send under object", "accesskey": "u"}
            }
          },
          "sep1": "---------",
          "cancel": {name: "cancel", icon: "quit", accesskey: "q"}
        }
      });

      if(!dev){
        $.contextMenu({
          selector: '*:not(#canvasContainer *)',
          callback: function (key, options) {
            alert('Digital Board | Copyright \u00a9 1998-2015 Infinitec Co., Ltd. All Rights Reserved.');
          },
          items: {
            "about": {name: "About", accesskey: "a"}
          }
        });
      }
    },

    fillpallete: $("#fillpallete").kendoColorPicker({
      value: "#000000",
      buttons: false,
      opacity: true,
      change: function (e) {
        Board.setFill(e);
      }
    }),

    strokepallete: $("#strokepallete").kendoColorPicker({
      value: "#000000",
      buttons: false,
      opacity: true,
      change: function (e) {
        Board.setStroke(e);
      }
    }),

    linethickslider: $("#thickslider").kendoSlider({
      increaseButtonTitle: "Decrease",
      decreaseButtonTitle: "Increase",
      min: 1,
      max: 10,
      smallStep: 1,
      largeStep: 1,
      change: function (e) {
        Board.setLineThick(e);
      }
    }).data("kendoSlider"),

    show: function (e) {

    },

    hide: function () {
      window.clearTimeout(Board.canvasLoadTimer);
    },

    saveSession: function (e) {
      Board.saveCanvas();
    }
  }
});