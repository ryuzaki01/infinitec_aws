var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var livereload = require('gulp-livereload');
var del = require('del');
var imageMin = require('gulp-imagemin');
var source = require('vinyl-source-stream');
var rename = require('gulp-rename');
var stylus = require('gulp-stylus');  // To compile Stylus CSS.
var bump = require('gulp-bump');
var amdOptimize = require('amd-optimize');
require('dotenv').load({path: '.env'});

/*
 * Create variables for our project paths so we can change in one place
 */
var paths = {
  'src': ['./models/**/*.js', './routes/**/*.js', 'keystone.js', 'package.json'],
  'stylWatch': ['./public/css/*.styl', './public/css/**/*.styl'],
  'jsWatch': ['./public/js/*.js', './public/js/**/*.js', './public/js/**/**/*.js'],
  'styl': ['./public/css/*.styl'],

  'cssOutput': './public/dist/css/',
  'cssOutputFileName': 'all.min.css',
  'images': [
    './public/img/**/*'
  ],
  'imageOutput': './public/dist/img/',
  'requireJsConfig': './public/js/config.js',
  'mainScript': 'main',
  'clientScriptOutput': './public/dist/js/',
  'clientScriptOutputFileName': 'main.js'
};

// You can use multiple globbing patterns as you would with `gulp.src`
gulp.task('cleanCss', function (cb) {
  del([
    paths.cssOutput + paths.cssOutputFilePattern
  ], cb);
});

// You can use multiple globbing patterns as you would with `gulp.src`
gulp.task('cleanClientScript', function (cb) {
  del([
    paths.clientScriptOutput
  ], cb);
});


gulp.task('cleanImages', function (cb) {
  del([
    paths.imageOutput
  ], cb);
});

gulp.task('stylus', ['cleanCss'], function(){
  gulp.src(paths.styl)
    .pipe(stylus({
      compress: true,
      sourcemap: {
        inline: true,
        sourceRoot: '.',
        basePath: paths.cssOutput
      }
    }))
    .pipe(autoprefixer())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(paths.cssOutput))
    .pipe(livereload());
});

gulp.task("clientScript", [ "cleanClientScript" ], function () {
  var task =  gulp.src("./public/js/**/*.js")
      .pipe(amdOptimize(paths.mainScript, {
        configFile : paths.requireJsConfig
      }))
      .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(uglify());
  if (!process.env.REQUIRE_BUNDLE || process.env.REQUIRE_BUNDLE === 'true') {
    task = task.pipe(concat(paths.clientScriptOutputFileName));
  }
  if (!process.env.REQUIRE_WRITEMAP || process.env.REQUIRE_WRITEMAP === 'true') {
    task = task.pipe(sourcemaps.write('./'));
  }

  task.pipe(gulp.dest(paths.clientScriptOutput));

});

// Copy all static images
gulp.task('images', ['cleanImages'], function () {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imageMin({
      optimizationLevel: 6,
      progressive: true
    }))
    .pipe(gulp.dest(paths.imageOutput));
});

// in the CI we want to run the client script and less
gulp.task('rebuild', ['clientScript','stylus']);

// Rerun the task when a file changes
gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(paths.jsWatch, ['clientScript']);
  gulp.watch(paths.stylWatch, ['stylus']);
  gulp.watch(paths.images, ['images']);
});

//inspired from:
//http://ponyfoo.com/articles/my-first-gulp-adventure

// we want to bump the version of the package.json
gulp.task('bump', function () {
  return gulp.src(['./package.json'])
    .pipe(bump())
    .pipe(gulp.dest('./'));
});
